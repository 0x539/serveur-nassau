# Arbre de technologie #

L’arbre de technologie permet de connaître les technologies et
bâtiments nécessaires à la construction d’autres bâtiments, navires,
défenses et à la recherche d’autres technologies.

## Bâtiments et installations ##

Les bâtiments sans prérequis sont la forêt, la mine de métal, la
fabrique de poudre à canon, le quartier de travailleurs, le camp de
formation de travailleurs, le laboratoire de recherche, le hangar de
bois, le hangar de métal et la poudrière.

Le chantier naval a pour prérequis le camp de formation de
travailleurs au second niveau.

La taverne a pour prérequis le camp de formation de travailleurs au
dixième niveau.

## Navires ##

Le bateau espion a pour prérequis le chantier naval au troisième
niveau, la recherche espionnage au second niveau.


La canonnière a pour prérequis le chantier naval au premier niveau,
les canons au premier niveau, la coque au premier niveau et les voiles
au premier niveau.


La corvette a pour prérequis le chantier naval au troisième niveau,
les canons au second niveau, la coque au second niveau, les voiles au
second niveau.


La frégate a pour prérequis le chantier naval au cinquième niveau, les
canons au cinquième niveau, les voiles au cinquième niveau, la coque
au quatrième niveau.


Le brick a pour prérequis le chantier naval au septième niveau, les
voiles aux septième niveau, les canons au septième niveau, la coque au
septième niveau.


Le galion a pour prérequis le chantier naval au neuvième niveau, les
voiles au neuvième niveau, la coque au neuvième niveau, les canons au
neuvième niveau.


Le navire de ligne a pour prérequis le chantier naval au douzième
niveau, les voiles au douzième niveau, les canons au quinzième niveau,
la coque au quinzième niveau.


Le bateau de transport léger a pour prérequis le chantier naval au
second niveau, les voiles au second niveau, la coque au second niveau.


Le bateau de transport lourd a pour prérequis le chantier naval au
quatrième niveau, les voiles au sixième niveau et la coque au sixième
niveau.

## Les défenses ##

Le mur a pour prérequis le chantier naval au premier niveau.


La mine a pour prérequis le chantier naval au troisième niveau.


Le canon a pour prérequis le chantier naval au cinquième niveau et les
canons au troisième niveau.


Le mortier a pour prérequis le chantier naval au huitième niveau et
les canons au sixième niveau.


Le fort a pour prérequis le chantier naval au douzième niveau et les
canons au dixième niveau.

## Les recherches ##

La technologie formation d’officiers nécessite le laboratoire de
recherche au premier niveau.


La technologie voile nécessite le laboratoire de recherche au second
niveau.


La technologie canon nécessite le laboratoire de recherche au second
niveau.


La technologie coque nécessite le laboratoire de recherche au second
niveau.


La technologie espionnage nécessite le laboratoire de recherche au
troisième niveau.


La technologie colonisation nécessite le laboratoire de recherche au
cinquième niveau.
