# Protocole
## Constructions
### Construction d’un bâtiment

Le client envoie : `son_pseudo son_mot_de_passe construire_bâtiment numéro_de_l_ile nom_bâtiment`.

#### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

#### Exemple :

##### Client :

`corentin coco construire_bâtiment 1 mine_de_métal`

##### Serveur :

`fait`

### Construction de navires

Le client envoie : `son_pseudo son_mot_de_passe construire_navires numéro_de_l_ile nom_navire nombre`.

Le serveur envoie : `fait`

#### Exemple :

##### Client :

`corentin coco construire_navires galion 10`

##### Serveur :

`fait`

### Construction de défenses

## Expéditions

### Lancer une attaque

Le client envoie : `son_pseudo son_mot_de_passe attaque numéro_de_l’ile_de_départ océan secteur emplacement nombre_bateau_espion nombre_bateau_transport_léger nombre_bateau_transport_lourd nombre_canonnière nombre_corvette nombre_frégate nombre_brick nombre_galion nombre_navire_de_ligne`

#### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

### Lancer un transport

Le client envoie : `son_pseudo son_mot_de_passe transporter numéro_de_l’ile_de_départ océan secteur emplacement nombre_bateau_espion nombre_bateau_transport_léger nombre_bateau_transport_lourd nombre_canonnière nombre_frégate nombre_corvette nombre_brick nombre_galion nombre_navire_de_ligne bois métal poudre_à_canon`

#### S’il n’y a pas d’erreur

Le serveur envoie : `fait`

### Lancer une colonisation

Le client envoie : `son_pseudo son_mot_de_passe colonisation numéro_de_l’ile_de_départ océan secteur emplacement nombre_de_bateau_de_transport_lourd nombre_de_bateau_de_transport_léger bois métal poudre_à_canon`

Attention, le nombre de bateau de transport lourd doit être supérieur
ou égale à un mais le nombre de bateau de transport léger peut être
égal à zéro.

#### S’il n’y a pas d’erreur

Le serveur envoie : `fait`

### Lancer un espionnage

Le client envoie : `son_pseudo son_mot_de_passe espionnage numéro_de_l’ile_de_départ océan secteur emplacement nombre_de_bateau_espion`

#### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

### Lancer un stationnement

Le client envoie : `son_pseudo son_mot_de_passe stationnement numéro_de_l’île_de_départ numéro_de_l’île_d’arrivée nombre_bateau_espion nombre_bateau_transport_léger nombre_bateau_transport_lourd nombre_canonnière nombre_frégate nombre_corvette nombre_brick nombre_galion nombre_navire_de_ligne bois métal poudre_à_canon`

#### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

## Lancer une recherche

Le client envoie : `son_pseudo son_mot_de_passe lancer_recherche nom_recherche numéro_île`

### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

## Abandonner une ile

Le client envoie : `son_pseudo son_mot_de_passe abandonner numéro_de_l’ile`

### S’il n’y a pas d’erreurs

Le serveur envoie : `fait`

## Consultations
### Ressources

Le client envoie : `son_pseudo son_mot_de_passe ressources numéro_de_l_ile`.

Le serveur envoie : `quantité_de_bois quantité_de_métal quantité_de_poudre_à_canon quantité_de_travailleurs`

#### Exemple :
##### Client :

`corentin coco ressources 1`

##### Serveur :

`25000 18000 17500 20`

### Niveau des bâtiments

Le client envoie : `son_pseudo son_mot_de_passe bâtiments numéro_de_l_ile`.

Le serveur envoie : `niveau_forêt niveau_mine_de_métal niveau_fabrique_poudre_à_canon niveau_hangar_bois niveau_hangar_métal niveau_poudrière niveau_quartier_travailleurs niveau_chantier_naval niveau_laboratoire_recherche niveau_camp_formation_travailleurs`.

#### Exemple :
##### Client :

`corentin coco bâtiments 1`

##### Serveur :

`15 13 11 5 5 5 17 3 2 3`

### Nombre de navires

Le client envoie : `son_pseudo son_mot_de_passe navires numéro_de_l_ile`

Le serveur envoie : `nombre_bateau_espion nombre_bateau_transport_léger nombre_bateau_transport_lourd nombre_canonnière nombre_frégate nombre_corvette nombre_brick nombre_galion nombre_navire_de_ligne`

#### Exemple :
##### Client :

`corentin coco navires 1`

##### Serveur :

`1 2 3 4 5 6 7 8 9`

### Nombre de défenses

Le client envoie : `son_pseudo son_mot_de_passe défenses numéro_de_l_ile`

Le serveur envoie : `nombre_murs nombre_canons nombre_mines nombre_mortiers nombre_forts`

#### Exemple :
##### Client :

`corentin coco défenses 1`

##### Serveur :

`1 2 3 4 5`

### Niveau des recherches
### Expéditions ordonnées par le joueur
### Expéditions contre le joueur
### Nombre de messages

Le client envoie : `son_pseudo son_mot_de_passe nombre_messages`

Le serveur envoie : `nombre_de_messages`

#### Exemple :
##### Client :

`corentin coco nombre_messages`

##### Serveur :

`8`

### Contenu d’un message

Le client envoie : `son_pseudo son_mot_de_passe consulter_message 3`

Le serveur envoie : `document_JSON`

#### Exemple :
##### Client :

`corentin coco consulter_message 3`

##### Serveur :

`{ "_id" : ObjectId("58bab65b01becd566b5d5b8e"), "destinataire" : "baptiste", "attaquant" : "corentin", "date" : ISODate("2017-03-04T12:43:07Z"), "type" : "défense", "issue du combat" : "victoire", "île attaquant" : { "océan" : 96, "secteur" : 404, "emplacement" : 8 }, "île cible" : { "océan" : 37, "secteur" : 564, "emplacement" : 9 }, "ressources perdues" : { "bois" : 0, "métal" : 0, "poudre à canon" : 0, "flotte attaquante" : { "bateau espion" : 1, "bateau transport léger" : 0, "bateau transport lourd" : 0, "canonnière" : 0, "corvette" : 0, "frégate" : 0, "brick" : 0, "galion" : 0, "navire de ligne" : 0 }, "navires détruits" : { "bateau espion" : 1, "bateau transport léger" : 0, "bateau transport lourd" : 0, "canonnière" : 0, "corvette" : 0, "frégate" : 0, "brick" : 0, "galion" : 0, "navire de ligne" : 0 }, "navires perdus" : { "bateau espion" : 0, "bateau transport léger" : 0, "bateau transport lourd" : 0, "canonnière" : 0, "corvette" : 0, "frégate" : 0, "brick" : 0, "galion" : 0, "navire de ligne" : 0 }, "défenses perdues" : { "murs" : 0, "mines" : 0, "canons" : 0, "forts" : 0 } } }`

### Contructions de bâtiments

Le client envoie : `son_pseudo son_mot_de_passe bâtiment_en_cours_de_construction numéro_de_l_ile`

#### Si un bâtiment est en construction

Le serveur envoie : `oui nom_du_bâtiment heure_de_fin_de_construction`

#### Si aucun bâtiment n’est en construction

Le serveur envoie : `non`

### Constructions de navires
### Constructions de défenses
### Informations fondamentales sur mes îles

Le client envoie : `son_pseudo son_mot_de_passe mes_îles`

Le serveur répond : `document_JSON`

#### Exemple

##### Client

`corentin coco mes_îles`

##### Serveur

`{ "nb_îles" : 2, "île1" : { "nom" : "La Réunion", "océan" : 1, "secteur" : 1, "emplacement" : 1 }, "île2" : { "nom" : "Mayotte", "océan" : 1, "secteur" : 2, "emplacement" : 1 } }`

### Informations sur les iles dans un secteur donné

Le client envoie : `son_pseudo son_mot_de_passe informations_iles numéro_océan numéro_secteur`

Le serveur envoie : `nom_ile_1;propriétaire_ile_1;…;nom_ile_15;propriétaire_ile_15`

#### Exemple :

Client : `corentin coco informations_iles 1 2`

Serveur : `LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;corentin;ile principale;LIBRE;LIBRE;LIBRE;LIBRE;baptiste;La Réunion;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE;LIBRE`


## Annulations

### Construction d’un bâtiment

### Construction de bateaux

### Construction de défenses

### Expédition

### Recherche

## Erreurs
