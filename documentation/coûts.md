# Formules pour le calcul des coûts

Ce document décrit les formules utilisées pour calculer les coûts des bâtiments, navires, défenses et recherches.

## Bâtiments

Ici, « n » désigne le niveau du bâtiment.

### Forêt

- bois : 60 × 1,5^(n−1)
- métal : 15 × 1,5^(n−1)
- poudre à canon : 0
- travailleurs : 10 × n × 1,1^n

### Mine de métal

- bois : 48 × 1,6^(n−1)
- métal : 24 × 1,6^(n−1)
- poudre à canon : 0
- travailleurs : 10 × n × 1,1^n

### Fabrique de poudre à canon

- bois : 225 × 1,5^(n−1)
- métal : 75 × 1,5^(n−1)
- poudre à canon : 0
- travailleurs : 20 × n × 1,1^n

### Quartier de travailleurs

- bois : 75 × 1,5^(n−1)
- métal : 30 × 1,5^(n−1)
- poudre à canon : 0
- travailleurs : −20 × n × 1,1^n

### Chantier naval

- bois : 400 × 2^(n−1)
- métal : 200 × 2^(n−1)
- poudre à canon : 100 × 2^(n−1)
- travailleurs : 0

### Laboratoire de recherche

- bois : 200 × 2^(n−1)
- métal : 400 × 2^(n−1)
- poudre à canon : 100 × 2^(n−1)
- travailleurs : 0

### Camp de formation de travailleurs

- bois : 400 × 2^(n−1)
- métal : 120 × 2^(n−1)
- poudre à canon : 200 × 2^(n−1)
- travailleurs : 0

## Navires

Voici le nombre de ressources nécessaires par navire.

### Bateau espion

- bois : 0
- métal : 1000
- poudre à canon : 0

### Bateau de transport léger

- bois : 2000
- métal : 2000
- poudre à canon : 0

### Bateau de transport lourd

- bois : 6000
- métal : 6000
- poudre à canon : 0

### Cannonière

- bois : 3000
- métal : 1000
- poudre à canon : 0

### Corvette

- bois : 6000
- métal : 4000
- poudre à canon : 0

### Frégate

- bois : 20000
- métal : 7000
- poudre à canon : 2000

### Brick

- bois : 45000
- métal : 15000
- poudre à canon : 0

### Galion

- bois : 60000
- métal : 50000
- poudre à canon : 15000

### Navire de ligne

- bois : 5000000
- métal : 4000000
- poudre à canon : 1000000

## Défenses

Voici le nombre de ressources nécessaires par défense.

### Mur

- bois : 7500
- métal : 5000
- poudre à canon : 0

### Canon

- bois : 10000
- métal : 7500
- poudre à canon : 3000

### Mine

- bois : 5000
- métal : 2000
- poudre à canon : 1000

### Mortier

- bois : 10000
- métal : 7500
- poudre à canon : 3000

### Fort

- bois : 200000
- métal : 135000
- poudre à canon : 100000
