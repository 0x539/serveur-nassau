# Production des bâtiments#

Ce fichier contient les formules utilisées pour calculer la produciton
d’un bâtiment.

« n » désigne le niveau du bâtiment.

## Forêt ##

60 × n × 1,1^n

## Mine de métal ##

40 × n × 1,1^n

## Fabrique de poudre à canon ##

20 × n × 1,1^n

## Quartier de travailleurs ##

20 × n × 1,1^n
