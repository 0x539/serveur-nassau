/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions relatives aux ressources.
*/

#include "ressources.h"

int
récupérer_ressources_nécessaires_bâtiment (const char* nom_objet,
					   int niveau,
					   struct ressources* ressources_nécessaires)
{
  if (!strcmp (nom_objet, "forêt"))
    {
      ressources_nécessaires->bois = 60 * pow (1.5, niveau - 1);
      ressources_nécessaires->métal = 15 * pow (1.5, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 0;
      ressources_nécessaires->travailleurs = 10 * niveau * pow (1.1, niveau);
    }
  else if (!strcmp (nom_objet, "mine_de_métal"))
    {
      ressources_nécessaires->bois = 48 * pow (1.6, niveau - 1);
      ressources_nécessaires->métal = 24 * pow (1.6, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 0;
      ressources_nécessaires->travailleurs = 10 * niveau * pow (1.1, niveau);
    }
  else if (!strcmp (nom_objet, "fabrique_poudre_canon"))
    {
      ressources_nécessaires->bois = 225 * pow (1.5, niveau - 1);
      ressources_nécessaires->métal = 75 * pow (1.5, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 0;
      ressources_nécessaires->travailleurs = 20 * niveau * pow (1.1, niveau);
    }
  /* else if (!strcmp (nom_objet, "hangar_bois")) */
  /*   { */

  /*   } */
  /* else if (!strcmp (nom_objet, "hangar_métal")) */
  /*   { */

  /*   } */
  /* else if (!strcmp (nom_objet, "poudrière")) */
  /*   { */

  /*   } */
  else if (!strcmp (nom_objet, "quartier_travailleurs"))
    {
      ressources_nécessaires->bois = 75 * pow (1.5, niveau - 1);
      ressources_nécessaires->métal = 30 * pow (1.5, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 0;
      ressources_nécessaires->travailleurs = 0 - 20 * niveau * pow (1.1, niveau);
    }
  else if (!strcmp (nom_objet, "chantier_naval"))
    {
      ressources_nécessaires->bois = 400 * pow (2, niveau - 1);
      ressources_nécessaires->métal = 200 * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 100 * pow (2, niveau - 1);
      ressources_nécessaires->travailleurs = 0;
    }
  else if (!strcmp (nom_objet, "laboratoire_recherche"))
    {
      ressources_nécessaires->bois = 200 * pow (2, niveau - 1);
      ressources_nécessaires->métal = 400 * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 200 * pow (2, niveau - 1);
      ressources_nécessaires->travailleurs = 0;
    }
  else if (!strcmp (nom_objet, "camp_formation_travailleurs"))
    {
      ressources_nécessaires->bois = 400 * pow (2, niveau - 1);
      ressources_nécessaires->métal = 120 * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 200 * pow (2, niveau - 1);
      ressources_nécessaires->travailleurs = 0;
    }
  else if (!strcmp (nom_objet, "taverne"))
    {
      ressources_nécessaires->bois = 2000000 * pow (2, niveau - 1);
      ressources_nécessaires->métal = 1000000 * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = 500000 * pow (2, niveau - 1);
      ressources_nécessaires->travailleurs = 0;
    }
  else
    return 0;

  return 1;
}

int
récupérer_ressources_nécessaires_navire (const char* nom_objet,
					 int nombre,
					 struct ressources* ressources_nécessaires)
{
  ressources_nécessaires->bois = 0;
  ressources_nécessaires->métal = 0;
  ressources_nécessaires->poudre_à_canon = 0;
  ressources_nécessaires->travailleurs = 0;

  if (!strcmp (nom_objet, "bateau_espion"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_BATEAU_ESPION * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_BATEAU_ESPION * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_BATEAU_ESPION * nombre;
    }
  else if (!strcmp (nom_objet, "bateau_transport_léger"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_BATEAU_TLÉ * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_BATEAU_TLÉ * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_BATEAU_TLÉ * nombre;
    }
  else if (!strcmp (nom_objet, "bateau_transport_lourd"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_BATEAU_TLO * nombre;
      ressources_nécessaires->métal = COÛT_BOIS_BATEAU_TLO * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_BATEAU_TLO * nombre;
    }
  else if (!strcmp (nom_objet, "canonnière"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_CANONNIÈRE * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_CANONNIÈRE * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_CANONNIÈRE * nombre;
    }
  else if (!strcmp (nom_objet, "frégate"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_FRÉGATE * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_FRÉGATE * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_FRÉGATE * nombre;
    }
  else if (!strcmp (nom_objet, "corvette"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_CORVETTE * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_CORVETTE * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_CORVETTE * nombre;
    }
  else if (!strcmp (nom_objet, "brick"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_BRICK * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_BRICK * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_BRICK * nombre;
    }
  else if (!strcmp (nom_objet, "galion"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_GALION * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_GALION * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_GALION * nombre;
    }
  else if (!strcmp (nom_objet, "navire_de_ligne"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_NDL * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_NDL * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_NDL * nombre;
    }
  else
    return 0;

  return 1;
}

int
récupérer_ressources_nécessaires_défenses (const char* nom_objet,
					   int nombre,
					   struct ressources* ressources_nécessaires)
{
  ressources_nécessaires->travailleurs = 0;

  if (!strcmp (nom_objet, "mur"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_MUR * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_MUR * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_MUR * nombre;
    }
  else if (!strcmp (nom_objet, "canon"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_CANON * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_CANON * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_CANON * nombre;
    }
  else if (!strcmp (nom_objet, "mine"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_MINE * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_MINE * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_MINE * nombre;
    }
  else if (!strcmp (nom_objet, "fort"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_FORT * nombre;
      ressources_nécessaires->métal = COÛT_MÉTAL_FORT * nombre;
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_FORT * nombre;
    }
  else
    return 0;

  return 1;
}

int
récupérer_ressources_nécessaires_recherche
(const char* nom_objet,
 int niveau,
 struct ressources* ressources_nécessaires)
{
  ressources_nécessaires->travailleurs = 0;

  if (!strcmp (nom_objet, "espionnage"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_ESPIONNAGE
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_ESPIONNAGE
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_ESPIONNAGE
        * pow (2, niveau - 1);
    }
  else if (!strcmp (nom_objet, "voilure"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_VOILURE
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_VOILURE
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_VOILURE
        * pow (2, niveau - 1);
    }
  else if (!strcmp (nom_objet, "coque"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_COQUE
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_COQUE
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_COQUE
        * pow (2, niveau - 1);
    }
  else if (!strcmp (nom_objet, "canons"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_CANONS
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_CANONS
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_CANONS
        * pow (2, niveau - 1);
    }
  else if (!strcmp (nom_objet, "colonisation"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_COLONISATION
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_COLONISATION
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_COLONISATION
        * pow (2, niveau - 1);
    }
  else if (!strcmp (nom_objet, "formation d’officiers"))
    {
      ressources_nécessaires->bois = COÛT_BOIS_FORMATION_OFFICIERS
        * pow (2, niveau - 1);
      ressources_nécessaires->métal = COÛT_MÉTAL_FORMATION_OFFICIERS
        * pow (2, niveau - 1);
      ressources_nécessaires->poudre_à_canon = COÛT_PÀC_FORMATION_OFFICIERS
        * pow (2, niveau - 1);
    }
  else
    return 0;

  return 1;
}

int
récupérer_temps_nécessaire_bâtiment (const struct ressources ressources_nécessaires,
                                     const int niveau_camp_travailleurs,
                                     const int niveau_taverne)
{
  double temps_nécessaire = (double) ressources_nécessaires.bois;
  temps_nécessaire += (double) ressources_nécessaires.métal;
  temps_nécessaire += (double) ressources_nécessaires.poudre_à_canon;
  temps_nécessaire /= (2500.0 * (double) (1 + niveau_camp_travailleurs));
  temps_nécessaire *= 3600.0;

  temps_nécessaire /= pow (2, niveau_taverne);

  return (int) temps_nécessaire;
}

int
récupérer_temps_nécessaire_objets_militaires (const struct ressources ressources_nécessaires,
					      const int niveau_chantier_naval,
                                              const int niveau_taverne)
{
  double temps_nécessaire = (double) ressources_nécessaires.bois;
  temps_nécessaire += (double) ressources_nécessaires.métal;
  temps_nécessaire += (double) ressources_nécessaires.poudre_à_canon;
  temps_nécessaire /= (5000.0 * (double) (1 + niveau_chantier_naval));
  temps_nécessaire *= 3600.0;

  temps_nécessaire /= pow (2, niveau_taverne);

  return (int) temps_nécessaire;
}

int
récupérer_temps_nécessaire_recherche
(const struct ressources ressources_nécessaires,
 const unsigned niveau_laboratoire_recherche)
{
  double temps_nécessaire = (double) ressources_nécessaires.bois;
  temps_nécessaire += (double) ressources_nécessaires.métal;
  temps_nécessaire += (double) ressources_nécessaires.poudre_à_canon;
  temps_nécessaire /= (1000.0 * (double) (1 + niveau_laboratoire_recherche));
  temps_nécessaire *= 3600.0;

  return (int) temps_nécessaire;
}
