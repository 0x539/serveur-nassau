/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants succèssifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions qui
permettront de manipuler la base de données.
*/

#include "bd.h"

int
supprimer_document (const bson_oid_t identifiant,
		    const char* nom_collection)
{
  int succès = 1;
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_error_t erreur;
  bson_t *doc;

  client = mongoc_client_new ("mongodb://localhost:27017/?appname=delete-example");
  collection = mongoc_client_get_collection (client, "nassau", nom_collection);

  doc = bson_new ();
  BSON_APPEND_OID (doc, "_id", &identifiant);

  if (!mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &erreur))
    {
      fprintf (stderr, "La suppression du document a échoué : %s\n",
	       erreur.message);
      succès = 0;
    }

  bson_destroy (doc);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
vérifier_existence_joueur (const char* nom_du_joueur)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;

  unsigned int existe = 0;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = bson_new ();
  BSON_APPEND_UTF8 (requête, "pseudo", nom_du_joueur);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    existe = 1;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return existe;
}

int
vérifier_existence_île (const struct coordonnées_île coordonnées)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;

  unsigned int existe = 0;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    existe = 1;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return existe;
}

static int
trouver_emplacement_libre (struct coordonnées_île *emplacement)
{
  for (unsigned i = 1; i <= OCÉAN_MAX; ++i)
    {
      for (unsigned j = 1; j <= SECTEUR_MAX; ++j)
        {
          for (unsigned k = 1; k <= 15; ++k)
            {
              emplacement->océan = i;
              emplacement->secteur = j;
              emplacement->emplacement = k;
              if (!vérifier_existence_île (*emplacement))
                return 1;
            }
        }
    }
  return 0;
}

int
créer_un_joueur (const char* nom_du_joueur, const char* mot_de_passe)
{
  int succès = 1;

  /* Le nom LIBRE est interdit car il est utilisé dans le protocole */
  if (!strcmp (nom_du_joueur, "LIBRE"))
    return 0;

  /*
     Il faut d’abord vérifier que le joueur n’existe pas puis,
     s’il existe, il faut renvoyer 0, s’il n’existe pas,
     il faut le créer avec les valeurs par défaut et renvoyer 1.
  */

  if (nom_du_joueur == NULL || mot_de_passe == NULL)
    return 0;

  if (vérifier_existence_joueur (nom_du_joueur))
    return 0;

  srand (time (NULL));

  struct coordonnées_île coordonnées;

  /* do */
  /*   { */
  /*     coordonnées.océan = rand () % OCÉAN_MAX + 1; */
  /*     coordonnées.secteur = rand () % SECTEUR_MAX + 1; */
  /*     coordonnées.emplacement = rand () % 15 + 1; */

  /*     if (i++ == 20) */
  /*       return 0; */
  /*   } while (vérifier_existence_île (coordonnées)); */

  if (!trouver_emplacement_libre (&coordonnées))
    return 0;

  /* créer île. */
  bson_t *île = NULL;

  île = BCON_NEW ("propriétaire", BCON_UTF8 (nom_du_joueur),
		  "dernière_màj", BCON_DATE_TIME (time (NULL) * 1000),

		  "nom", BCON_UTF8 ("île principale"),
		  "océan", BCON_INT32 (coordonnées.océan),
		  "secteur", BCON_INT32 (coordonnées.secteur),
		  "emplacement", BCON_INT32 (coordonnées.emplacement),

		  "ressources", "{",
		  "bois", BCON_INT32 (1000),
		  "métal", BCON_INT32 (1000),
		  "poudre_à_canon", BCON_INT32 (0),
		  "travailleurs", BCON_INT32 (0), "}",

		  "bâtiments", "{",
		  "forêt", BCON_INT32 (0),
		  "mine_de_métal", BCON_INT32 (0),
		  "fabrique_poudre_canon", BCON_INT32 (0),
		  "hangar_bois", BCON_INT32 (0),
		  "hangar_métal", BCON_INT32 (0),
		  "poudrière", BCON_INT32 (0),
		  "quartier_travailleurs", BCON_INT32 (0),
		  "chantier_naval", BCON_INT32 (0),
		  "laboratoire_recherche", BCON_INT32 (0),
		  "camp_formation_travailleurs", BCON_INT32 (0),
                  "taverne", BCON_INT32 (0), "}",

		  "défenses", "{",
		  "mur", BCON_INT32(0),
		  "canon", BCON_INT32 (0),
                  "mortier", BCON_INT32 (0),
		  "mine", BCON_INT32 (0),
		  "fort", BCON_INT32 (0), "}",

		  "navires", "{",
		  "bateau_espion", BCON_INT32 (0),
		  "bateau_transport_léger", BCON_INT32 (0),
		  "bateau_transport_lourd", BCON_INT32 (0),
		  "canonnière", BCON_INT32 (0),
		  "frégate", BCON_INT32 (0),
		  "corvette", BCON_INT32 (0),
		  "brick", BCON_INT32 (0),
		  "galion", BCON_INT32 (0),
		  "navire_de_ligne", BCON_INT32 (0), "}");

  /* Insérer l’île. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, île,
                                 NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_t *joueur = NULL;

  bson_destroy (île);
  mongoc_collection_destroy (collection);

  /* Créer joueur. */
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  joueur = BCON_NEW ("pseudo", BCON_UTF8 (nom_du_joueur),
		     "mot_de_passe", BCON_UTF8 (mot_de_passe),

		     "recherches", "{",
		     "espionnage", BCON_INT32 (0),
		     "voilure", BCON_INT32 (0),
		     "coque", BCON_INT32 (0),
		     "canons", BCON_INT32 (0),
                     "colonisation", BCON_INT32 (0),
		     "formation d’officiers", BCON_INT32 (0), "}",

		     "nb_îles", BCON_INT32 (1),
		     "île1", "{",
		     "océan", BCON_INT32 (coordonnées.océan),
		     "secteur", BCON_INT32 (coordonnées.secteur),
		     "emplacement", BCON_INT32 (coordonnées.emplacement), "}");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, joueur, NULL, &erreur))
    {
      succès = 0;
      fprintf (stderr, "%s\n", erreur.message);
    }

  bson_destroy (joueur);
  mongoc_client_destroy (client);

  return succès;
}

int
vérifier_mot_de_passe (const char* nom_du_joueur, const char* mot_de_passe)
{
  unsigned succès = 0;
  if (!vérifier_existence_joueur (nom_du_joueur))
    {
      return succès;
    }

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = bson_new ();
  BSON_APPEND_UTF8 (requête, "pseudo", nom_du_joueur);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  mongoc_cursor_next (curseur, &doc);
  if (bson_iter_init (&iter, doc))
    {
      while (bson_iter_next (&iter))
	{
	  if (strcmp (bson_iter_key (&iter), "mot_de_passe") == 0)
	    {
	      if (strcmp (mot_de_passe, bson_iter_utf8 (&iter, NULL)) == 0)
		{
		  succès = 1;
		  break;
		}
	    }
	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
modifier_ressources (const struct coordonnées_île coordonnées,
		     const struct ressources ressources_à_ajouter)
{
  int succès = 0;

  /* On récupère les ressources de l’île et on vérifie qu’elle existe bien. */
  struct ressources ressources_de_l_île;
  if (!récupérer_ressources_île (&ressources_de_l_île, coordonnées))
    return succès;

  ressources_de_l_île.bois += ressources_à_ajouter.bois;
  ressources_de_l_île.métal += ressources_à_ajouter.métal;
  ressources_de_l_île.poudre_à_canon += ressources_à_ajouter.poudre_à_canon;
  ressources_de_l_île.travailleurs += ressources_à_ajouter.travailleurs;

  time_t maintenant = time (NULL);
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_t *requête, *mise_à_jour;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  mise_à_jour = BCON_NEW ("$set", "{",
			  "ressources", "{",
			  "bois", BCON_INT32 (ressources_de_l_île.bois),
			  "métal", BCON_INT32 (ressources_de_l_île.métal),
			  "poudre_à_canon",
			  BCON_INT32 (ressources_de_l_île.poudre_à_canon),
			  "travailleurs",
			  BCON_INT32 (ressources_de_l_île.travailleurs),
			  "}",
			  "dernière_màj", BCON_DATE_TIME (maintenant * 1000),
			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
				requête, mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
soustraire_ressources (const struct coordonnées_île coordonnées,
		       const struct ressources ressources_à_soustraire)
{
  struct ressources ressources_à_ajouter =
    {
      .bois = ressources_à_soustraire.bois * -1,
      .métal = ressources_à_soustraire.métal * -1,
      .poudre_à_canon = ressources_à_soustraire.poudre_à_canon * -1,
      .travailleurs = ressources_à_soustraire.travailleurs * -1,
    };

  return modifier_ressources (coordonnées, ressources_à_ajouter);
}

int
modifier_nombre_navires (const struct coordonnées_île coordonnées,
			 const struct navires navires_à_ajouter)
{
  int succès = 0;

  /* On récupère le nombre de navires de l’île et on vérifie qu’elle existe bien. */
  struct navires navires_sur_l_île;
  if (!récupérer_nombre_navires (coordonnées, &navires_sur_l_île))
    return succès;

  /* Ajouter à la structure contenant les navires */
  /* de l’île celle passée en paramètre. */
  ajouter_structures_navires (&navires_sur_l_île, navires_à_ajouter);

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête, *mise_à_jour;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On suppose que chaque île n’est présente qu’une seule fois. */
  mongoc_cursor_next (curseur, &doc);

  mise_à_jour = BCON_NEW ("$set", "{",
			  "navires", "{",
			  "bateau_espion",
			  BCON_INT32 (navires_sur_l_île.bateau_espion),
			  "bateau_transport_léger",
			  BCON_INT32 (navires_sur_l_île.bateau_transport_léger),
			  "bateau_transport_lourd",
			  BCON_INT32 (navires_sur_l_île.bateau_transport_lourd),
			  "canonnière",
			  BCON_INT32 (navires_sur_l_île.canonnière),
			  "frégate",
			  BCON_INT32 (navires_sur_l_île.frégate),
			  "corvette",
			  BCON_INT32 (navires_sur_l_île.corvette),
			  "brick",
			  BCON_INT32 (navires_sur_l_île.brick),
			  "galion",
			  BCON_INT32 (navires_sur_l_île.galion),
			  "navire_de_ligne",
			  BCON_INT32 (navires_sur_l_île.navire_de_ligne),
			  "}",
  			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
  				requête, mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
soustraire_navires (const struct coordonnées_île coordonnées,
		    const struct navires navires_à_soustraire)
{
  int succès = 0;

  /* On récupère le nombre de navires de l’île et on vérifie qu’elle existe bien. */
  struct navires navires_sur_l_île;
  if (!récupérer_nombre_navires (coordonnées, &navires_sur_l_île))
    return succès;

  /* Soustraire à la structure contenant les navires */
  /* de l’île celle passée en paramètre. */
  if (!soustraire_structures_navires (&navires_sur_l_île, navires_à_soustraire))
    return succès;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête, *mise_à_jour;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On suppose que chaque île n’est présente qu’une seule fois. */
  mongoc_cursor_next (curseur, &doc);

  mise_à_jour = BCON_NEW ("$set", "{",
			  "navires", "{",
			  "bateau_espion",
			  BCON_INT32 (navires_sur_l_île.bateau_espion),
			  "bateau_transport_léger",
			  BCON_INT32 (navires_sur_l_île.bateau_transport_léger),
			  "bateau_transport_lourd",
			  BCON_INT32 (navires_sur_l_île.bateau_transport_lourd),
			  "canonnière",
			  BCON_INT32 (navires_sur_l_île.canonnière),
			  "frégate",
			  BCON_INT32 (navires_sur_l_île.frégate),
			  "corvette",
			  BCON_INT32 (navires_sur_l_île.corvette),
			  "brick",
			  BCON_INT32 (navires_sur_l_île.brick),
			  "galion",
			  BCON_INT32 (navires_sur_l_île.galion),
			  "navire_de_ligne",
			  BCON_INT32 (navires_sur_l_île.navire_de_ligne),
			  "}",
  			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
  				requête, mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
ajouter_défenses (const struct coordonnées_île coordonnées,
		  const struct défenses défenses_à_ajouter)
{
  int succès = 0;

  /* On récupère le nombre de navires de l’île et on vérifie qu’elle existe bien. */
  struct défenses défenses_sur_l_île;
  if (!récupérer_nombre_défenses (coordonnées, &défenses_sur_l_île))
    return succès;

  /* Ajouter à la structure contenant les défenses */
  /* de l’île celle passée en paramètre. */
  ajouter_structures_défenses (&défenses_sur_l_île, défenses_à_ajouter);

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête, *mise_à_jour;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On suppose que chaque île n’est présente qu’une seule fois. */
  mongoc_cursor_next (curseur, &doc);

  mise_à_jour = BCON_NEW ("$set", "{",
			  "défenses", "{",
			  "murs",
			  BCON_INT32 (défenses_sur_l_île.murs),
			  "mine",
			  BCON_INT32 (défenses_sur_l_île.mines),
			  "canon",
			  BCON_INT32 (défenses_sur_l_île.canons),
			  "mortier",
			  BCON_INT32 (défenses_sur_l_île.mortiers),
			  "fort",
			  BCON_INT32 (défenses_sur_l_île.forts), "}",
  			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
  				requête, mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
soustraire_défenses (const struct coordonnées_île coordonnées,
		     const struct défenses défenses_à_soustraire)
{
  int succès = 0;

  /* On récupère le nombre de navires de l’île et on vérifie qu’elle existe bien. */
  struct défenses défenses_sur_l_île;
  if (!récupérer_nombre_défenses (coordonnées, &défenses_sur_l_île))
    return succès;

  /* Soustraire à la structure contenant les défenses */
  /* de l’île celle passée en paramètre. */
  soustraire_structures_défenses (&défenses_sur_l_île, défenses_à_soustraire);

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête, *mise_à_jour;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On suppose que chaque île n’est présente qu’une seule fois. */
  mongoc_cursor_next (curseur, &doc);

  mise_à_jour = BCON_NEW ("$set", "{",
			  "défenses", "{",
			  "mur",
			  BCON_INT32 (défenses_sur_l_île.murs),
			  "mine",
			  BCON_INT32 (défenses_sur_l_île.mines),
			  "canon",
			  BCON_INT32 (défenses_sur_l_île.canons),
			  "mortier",
			  BCON_INT32 (défenses_sur_l_île.mortiers),
			  "fort",
			  BCON_INT32 (défenses_sur_l_île.forts), "}",
  			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
  				requête, mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
récupérer_ressources_île (struct ressources *ressources_de_l_île,
			  const struct coordonnées_île coordonnées)
{
  time_t date_derniere_maj;
  /* Il faut d’abord récupérer la date de dernière mise à jour,
     cette fonction vérifie aussi l’existence de l’île. */
  if (!récupérer_date_dernière_màj_île (&date_derniere_maj, coordonnées))
    return 0;

  /* Puis il faut récupérer les ressources et les enregistrer
     dans ressources_de_l_île. */

  if (!récupérer_valeur_objet_sur_île (coordonnées, "ressources",
				       "bois", &ressources_de_l_île->bois))
    return 0;

  récupérer_valeur_objet_sur_île (coordonnées, "ressources",
				  "métal", &ressources_de_l_île->métal);

  récupérer_valeur_objet_sur_île (coordonnées, "ressources",
				  "poudre_à_canon", &ressources_de_l_île->poudre_à_canon);

  récupérer_valeur_objet_sur_île (coordonnées, "ressources",
				  "travailleurs", &ressources_de_l_île->travailleurs);

  /* Il faut récupérer le niveau des mines pour pouvoir faire le calcul des
     ressources actuelles. */
  int niveau_forêt, niveau_mine_métal, niveau_fabrique_poudre_canon;

  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "forêt", &niveau_forêt);

  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "mine_de_métal", &niveau_mine_métal);

  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "fabrique_poudre_canon",
				  &niveau_fabrique_poudre_canon);

  /* Calculons la différence de temps entre la dernière mise à jour
     et maintenant */
  time_t maintenant = time (NULL);
  double difference_en_heure;
  difference_en_heure = (difftime (maintenant,
				   date_derniere_maj)
			 / 3600.0);

  float gain_bois_heure = 60 * niveau_forêt * pow (1.1, niveau_forêt);
  float gain_métal_heure = 40 * niveau_mine_métal * pow (1.1, niveau_mine_métal);
  float gain_pac_heure = 20 * niveau_fabrique_poudre_canon * pow (1.1, niveau_fabrique_poudre_canon);

  ressources_de_l_île->bois += (int) (gain_bois_heure * difference_en_heure);
  ressources_de_l_île->métal += (int) (gain_métal_heure * difference_en_heure);
  ressources_de_l_île->poudre_à_canon += (int) (gain_pac_heure * difference_en_heure);

  return 1;
}

int
récupérer_propriétaire_île (const struct coordonnées_île coordonnées,
			    char **destination)
{
  unsigned succès = 0;
  if (!vérifier_existence_île (coordonnées))
    {
      return succès;
    }

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return succès;

  if (bson_iter_init (&iter, doc)
      && bson_iter_find (&iter, "propriétaire")
      && BSON_ITER_HOLDS_UTF8 (&iter))
    {
      *destination = strdup (bson_iter_utf8 (&iter, NULL));
      succès = 1;
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
récupérer_date_dernière_màj_île (time_t* date,
				 const struct coordonnées_île coordonnées)
{
  unsigned succès = 0;
  if (!vérifier_existence_île (coordonnées))
    {
      return succès;
    }

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  mongoc_cursor_next (curseur, &doc);
  if (bson_iter_init (&iter, doc))
    {
      while (bson_iter_next (&iter))
	{
	  if (strcmp (bson_iter_key (&iter), "dernière_màj") == 0)
	    {
	      succès = 1;
	      *date = (time_t) (bson_iter_date_time (&iter) / 1000);
	      break;
	    }
	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
récupérer_valeur_objet_sur_île (const struct coordonnées_île coordonnées,
				const char* type_objet,
				const char* nom_objet,
				int* valeur)
{
  int succès = 0;
  if (!vérifier_existence_île (coordonnées))
    return succès;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, type;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On suppose que chaque île n’est présente qu’une seule fois. */
  mongoc_cursor_next (curseur, &doc);

  /* On calcule la longueur de la chaine. */
  int longueur_chaine = strlen (type_objet) + strlen (nom_objet) + 2;

  /* On crée la chaine en notation avec point. */
  char objet[longueur_chaine];

  snprintf (objet, longueur_chaine, "%s.%s", type_objet, nom_objet);

  if (bson_iter_init (&iter, doc)
      && bson_iter_find_descendant (&iter, objet, &type)
      && BSON_ITER_HOLDS_INT32 (&type))
    {
      *valeur = bson_iter_int32 (&type);
      succès = 1;
    }
  else
    *valeur = 0;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
modifier_valeur_objet_sur_île (const struct coordonnées_île coordonnées,
			       const char* type_objet,
			       const char* nom_objet,
			       const int valeur)
{
  int succès = 0;

  /* On vérifie que l’île existe. */
  if (!vérifier_existence_île (coordonnées))
    return succès;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête, *mise_à_jour;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  /* On calcule la longueur de la chaine. */
  int longueur_chaine = strlen (type_objet) + strlen (nom_objet) + 2;

  /* On crée la chaine en notation avec point. */
  char objet[longueur_chaine];
  snprintf (objet, longueur_chaine, "%s.%s", type_objet, nom_objet);

  mongoc_cursor_next (curseur, &doc);

  mise_à_jour = BCON_NEW ("$set", "{",
			  objet, BCON_INT32 (valeur),
			  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE, requête,
				mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
récupérer_niveau_recherche (const char *pseudo, const char* nom_recherche)
{
  if (!vérifier_existence_joueur (pseudo))
    return -1;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *document;
  bson_t *requête;


  client = mongoc_client_new ("mongodb://localhost:27017");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");
  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  mongoc_cursor_next (curseur, &document);

  /* On récupère le niveau et on l’enregistre dans la variable niveau. */
  int niveau = 0;
  bson_iter_t iter1, iter2;

  const size_t longueur_recherche = strlen ("recherches.") + strlen (nom_recherche);
  char recherche[longueur_recherche];
  snprintf (recherche, longueur_recherche, "recherches.%s", nom_recherche);

  if (bson_iter_init (&iter1, document)
      && bson_iter_find_descendant (&iter1, recherche, &iter2)
      && BSON_ITER_HOLDS_INT32 (&iter2))
    {
      niveau = bson_iter_int32 (&iter2);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return niveau;
}

int
modifier_niveau_recherche (const char *pseudo, const char* nom_recherche,
                           unsigned niveau)
{
  if (!vérifier_existence_joueur (pseudo))
    return 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  bson_t *requête;

  client = mongoc_client_new ("mongodb://localhost:27017");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");
  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  const size_t longueur_recherche = strlen ("recherches.");
  longueur_recherche += strlen (nom_recherche) + 1;
  char recherche[longueur_recherche];
  snprintf (recherche, longueur_recherche, "recherches.%s", nom_recherche);

  bson_t *mise_à_jour = BCON_NEW ("$set", "{",
                                  recherche, BCON_INT32 (niveau),
                                  "}");

  int succès = 0;
  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE, requête,
				mise_à_jour, NULL, NULL))
    succès = 1;

  if (succès)
    bson_destroy (mise_à_jour);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
trouver_coordonnées_île (const char* pseudo,
			 const unsigned int numéro_île,
			 struct coordonnées_île* coordonnées)
{
  if (numéro_île == 0)
    return 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, île;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = bson_new ();
  BSON_APPEND_UTF8 (requête, "pseudo", pseudo);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return 0;

  /* Il faut vérifier que le joueur a au moins numéro_île îles. */
  if (bson_iter_init (&iter, doc)
      && bson_iter_find (&iter, "nb_îles")
      && BSON_ITER_HOLDS_INT32 (&iter))
    {
      if (bson_iter_int32 (&iter) < numéro_île)
	return 0;
    }

  char chaine_île[32];
  snprintf (chaine_île, 32, "île%d.océan", numéro_île);

  if (bson_iter_init (&iter, doc)
      && bson_iter_find_descendant (&iter, chaine_île, &île)
      && BSON_ITER_HOLDS_INT32 (&île))
    {
      coordonnées->océan = bson_iter_int32 (&île);
    }

  snprintf (chaine_île, 32, "île%d.secteur", numéro_île);
  if (bson_iter_init (&iter, doc)
      && bson_iter_find_descendant (&iter, chaine_île, &île)
      && BSON_ITER_HOLDS_INT32 (&île))
    {
      coordonnées->secteur = bson_iter_int32 (&île);
    }

  snprintf (chaine_île, 32, "île%d.emplacement", numéro_île);
  if (bson_iter_init (&iter, doc)
      && bson_iter_find_descendant (&iter, chaine_île, &île)
      && BSON_ITER_HOLDS_INT32 (&île))
    {
      coordonnées->emplacement = bson_iter_int32 (&île);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
récupérer_niveau_bâtiments (const struct coordonnées_île coordonnées,
			    struct niveau_bâtiments* niveaux)
{
  int succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, bâtiment;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return succès;

  char *bâtiments[] = {
    "bâtiments.forêt",
    "bâtiments.mine_de_métal",
    "bâtiments.fabrique_poudre_canon",
    "bâtiments.hangar_bois",
    "bâtiments.hangar_métal",
    "bâtiments.poudrière",
    "bâtiments.quartier_travailleurs",
    "bâtiments.chantier_naval",
    "bâtiments.laboratoire_recherche",
    "bâtiments.camp_formation_travailleurs",
    "bâtiments.taverne"
  };

  int *bâtiments_struct[] = {
    &(niveaux->forêt),
    &(niveaux->mine_métal),
    &(niveaux->fabrique_poudre_canon),
    &(niveaux->hangar_bois),
    &(niveaux->hangar_métal),
    &(niveaux->poudrière),
    &(niveaux->quartier_travailleurs),
    &(niveaux->chantier_naval),
    &(niveaux->laboratoire_recherche),
    &(niveaux->camp_formation_travailleurs),
    &(niveaux->taverne)
  };

  for (int i = 0; i < sizeof (bâtiments) / sizeof (char**); ++i)
    {
      if (bson_iter_init (&iter, doc)
      	  && bson_iter_find_descendant (&iter, bâtiments[i], &bâtiment)
      	  && BSON_ITER_HOLDS_INT32 (&bâtiment))
        *(bâtiments_struct[i]) = bson_iter_int32 (&bâtiment);
      else
        *(bâtiments_struct[i]) = 0;
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
récupérer_niveau_recherches (const char *pseudo,
			     struct niveau_recherches *niveaux)
{
  int succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, iter2;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = bson_new ();
  BSON_APPEND_UTF8 (requête, "pseudo", pseudo);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return succès;

  char *recherches[] = {
    "recherches.espionnage",
    "recherches.voilure",
    "recherches.coque",
    "recherches.canons",
    "recherches.formation d’officiers",
    "recherches.colonisation"
  };

  int *recherches_struct[] = {
    &(niveaux->espionnage),
    &(niveaux->voilure),
    &(niveaux->coque),
    &(niveaux->canons),
    &(niveaux->formation_officiers),
    &(niveaux->colonisation)
  };

  for (int i = 0; i < sizeof (recherches) / sizeof (char**); ++i)
    {
      if (bson_iter_init (&iter, doc)
      	  && bson_iter_find_descendant (&iter, recherches[i], &iter2)
      	  && BSON_ITER_HOLDS_INT32 (&iter2))
      	{
      	  *(recherches_struct[i]) = bson_iter_int32 (&iter2);
      	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
récupérer_nombre_défenses (const struct coordonnées_île coordonnées,
			   struct défenses* nombre_défenses)
{
  int succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, défense;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return succès;

  char *défenses[] = {
    "défenses.mur",
    "défenses.canon",
    "défenses.mortier",
    "défenses.mine",
    "défenses.fort"
  };

  int *défenses_struct[] = {
    &(nombre_défenses->murs),
    &(nombre_défenses->canons),
    &(nombre_défenses->mortiers),
    &(nombre_défenses->mines),
    &(nombre_défenses->forts)
  };

  for (int i = 0; i < sizeof (défenses) / sizeof (char**); ++i)
    {
      if (bson_iter_init (&iter, doc)
      	  && bson_iter_find_descendant (&iter, défenses[i], &défense)
      	  && BSON_ITER_HOLDS_INT32 (&défense))
      	{
      	  *(défenses_struct[i]) = bson_iter_int32 (&défense);
      	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
récupérer_nombre_navires (const struct coordonnées_île coordonnées,
			  struct navires* nombre_navires)
{
  int succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter, navire;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return succès;

  char *navires[] = {
    "navires.bateau_espion",
    "navires.bateau_transport_léger",
    "navires.bateau_transport_lourd",
    "navires.canonnière",
    "navires.frégate",
    "navires.corvette",
    "navires.brick",
    "navires.galion",
    "navires.navire_de_ligne"
  };

  int *navires_struct[] = {
    &(nombre_navires->bateau_espion),
    &(nombre_navires->bateau_transport_léger),
    &(nombre_navires->bateau_transport_lourd),
    &(nombre_navires->canonnière),
    &(nombre_navires->frégate),
    &(nombre_navires->corvette),
    &(nombre_navires->brick),
    &(nombre_navires->galion),
    &(nombre_navires->navire_de_ligne),
  };

  for (int i = 0; i < sizeof (navires) / sizeof (char**); ++i)
    {
      if (bson_iter_init (&iter, doc)
      	  && bson_iter_find_descendant (&iter, navires[i], &navire)
      	  && BSON_ITER_HOLDS_INT32 (&navire))
      	{
      	  *(navires_struct[i]) = bson_iter_int32 (&navire);
      	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
construction_en_cours (const char *nom_collection,
                       const struct coordonnées_île île)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;

  int construction_en_cours = 0;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", nom_collection);

  requête = BCON_NEW ("océan", BCON_INT32 (île.océan),
                      "secteur", BCON_INT32 (île.secteur),
                      "emplacement", BCON_INT32 (île.emplacement));

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    construction_en_cours = 1;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return construction_en_cours;
}

int
recherche_en_cours (const char *pseudo)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;

  int recherche_en_cours = 0;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "recherches en cours");

  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    recherche_en_cours = 1;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return recherche_en_cours;
}

int
ajouter_construction_bâtiment (const struct coordonnées_île coordonnées,
			       const char* nom_bâtiment,
			       const int niveau_bâtiment,
			       time_t date_fin_construction)
{
  int succès = 1;

  if (!vérifier_existence_île (coordonnées))
    return 0;

  /* Création du document. */
  bson_t *construction;

  construction = BCON_NEW ("océan", BCON_INT32 (coordonnées.océan),
  			   "secteur", BCON_INT32 (coordonnées.secteur),
  			   "emplacement", BCON_INT32 (coordonnées.emplacement),
  			   "nom", BCON_UTF8 (nom_bâtiment),
			   "niveau", BCON_INT32 (niveau_bâtiment),
  			   "fin de construction",
  			   BCON_DATE_TIME (date_fin_construction * 1000));

  /* Insertion du document. */
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "bâtiments en construction");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, construction,
				 NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (construction);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
ajouter_construction_navires (const struct coordonnées_île coordonnées,
			      const char* nom_navire,
			      const int nombre_navires,
			      time_t date_fin_construction)
{
  int succès = 1;

  if (!vérifier_existence_île (coordonnées))
    return 0;

  /* Création du document. */
  bson_t *construction;

  construction = BCON_NEW ("océan", BCON_INT32 (coordonnées.océan),
  			   "secteur", BCON_INT32 (coordonnées.secteur),
  			   "emplacement", BCON_INT32 (coordonnées.emplacement),
  			   "nom", BCON_UTF8 (nom_navire),
			   "nombre", BCON_INT32 (nombre_navires),
  			   "fin de construction",
  			   BCON_DATE_TIME (date_fin_construction * 1000));

  /* Insertion du document. */
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "navires en construction");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, construction,
				 NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (construction);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
ajouter_construction_défenses (const struct coordonnées_île coordonnées,
			       const char* nom_défense,
			       const int nombre_défenses,
			       time_t date_fin_construction)
{
  int succès = 1;

  if (!vérifier_existence_île (coordonnées))
    return 0;

  /* Création du document. */
  bson_t *construction;

  construction = BCON_NEW ("océan", BCON_INT32 (coordonnées.océan),
  			   "secteur", BCON_INT32 (coordonnées.secteur),
  			   "emplacement", BCON_INT32 (coordonnées.emplacement),
  			   "nom", BCON_UTF8 (nom_défense),
			   "nombre", BCON_INT32 (nombre_défenses),
  			   "fin de construction",
  			   BCON_DATE_TIME (date_fin_construction * 1000));

  /* Insertion du document. */
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "défenses en construction");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, construction,
				 NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (construction);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
ajouter_recherche (const char *pseudo,
                   const char* nom_recherche,
                   const int niveau_recherche,
                   time_t date_fin)
{
  /* Création du document. */
  bson_t *recherche;

  recherche = BCON_NEW ("pseudo", BCON_UTF8 (pseudo),
                        "nom", BCON_UTF8 (nom_recherche),
                        "niveau", BCON_INT32 (niveau_recherche),
                        "fin de recherche",
                        BCON_DATE_TIME (date_fin * 1000));

  /* Insertion du document. */
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "recherches en cours");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, recherche,
				 NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (recherche);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

void
mettre_a_jour_liste_bâtiments ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "bâtiments en construction");

  requête = BCON_NEW ("fin de construction", "{", "$lt",
		      BCON_DATE_TIME (time (NULL) * 1000),
		      "}");

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  struct coordonnées_île coordonnées;
  char *nom_bâtiment = NULL;
  int niveau_bâtiment;

  while (mongoc_cursor_next (curseur, &doc))
    {
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "océan")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.océan = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "secteur")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.secteur = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "emplacement")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.emplacement = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  nom_bâtiment = strdup (bson_iter_utf8 (&iter, NULL));
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "niveau")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  niveau_bâtiment = bson_iter_int32 (&iter);
	}

      modifier_valeur_objet_sur_île (coordonnées, "bâtiments", nom_bâtiment,
				     niveau_bâtiment);
      free (nom_bâtiment);

      /* Supprimer le document. */
      mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, NULL);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
}

void
mettre_a_jour_liste_navires ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "navires en construction");

  requête = BCON_NEW ("fin de construction", "{", "$lt",
		      BCON_DATE_TIME (time (NULL) * 1000),
		      "}");

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  struct coordonnées_île coordonnées;
  char *nom_navire = NULL;
  int nombre;

  while (mongoc_cursor_next (curseur, &doc))
    {
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "océan")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.océan = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "secteur")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.secteur = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "emplacement")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.emplacement = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  nom_navire = strdup (bson_iter_utf8 (&iter, NULL));
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nombre")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  nombre = bson_iter_int32 (&iter);
	}
      int nombre_navires_actuel;
      récupérer_valeur_objet_sur_île (coordonnées, "navires",
                                      nom_navire, &nombre_navires_actuel);
      nombre += nombre_navires_actuel;
      modifier_valeur_objet_sur_île (coordonnées, "navires", nom_navire,
				     nombre);
      free (nom_navire);

      /* Supprimer le document. */
      mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, NULL);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
}

void
mettre_à_jour_liste_défenses ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "défenses en construction");

  requête = BCON_NEW ("fin de construction", "{", "$lt",
		      BCON_DATE_TIME (time (NULL) * 1000),
		      "}");

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  struct coordonnées_île coordonnées;
  char *nom_défense = NULL;
  int nombre;

  while (mongoc_cursor_next (curseur, &doc))
    {
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "océan")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.océan = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "secteur")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.secteur = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "emplacement")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  coordonnées.emplacement = bson_iter_int32 (&iter);
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  nom_défense = strdup (bson_iter_utf8 (&iter, NULL));
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nombre")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  nombre = bson_iter_int32 (&iter);
	}

      int nombre_défenses_actuel;
      récupérer_valeur_objet_sur_île (coordonnées, "défenses",
                                      nom_défense, &nombre_défenses_actuel);
      nombre += nombre_défenses_actuel;
      modifier_valeur_objet_sur_île (coordonnées, "défenses", nom_défense,
				     nombre);
      free (nom_défense);

      /* Supprimer le document. */
      mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, NULL);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
}

void
mettre_à_jour_liste_recherches ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "recherches en cours");

  requête = BCON_NEW ("fin de recherche", "{", "$lt",
		      BCON_DATE_TIME (time (NULL) * 1000),
		      "}");

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  char *nom_recherche, *pseudo;
  int niveau_recherche;

  while (mongoc_cursor_next (curseur, &doc))
    {
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "pseudo")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  pseudo = strdup (bson_iter_utf8 (&iter, NULL));
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  nom_recherche = strdup (bson_iter_utf8 (&iter, NULL));
	}

      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "niveau")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  niveau_recherche = bson_iter_int32 (&iter);
	}

      modifier_niveau_recherche (pseudo, nom_recherche,
                                 niveau_recherche);
      free (nom_recherche);
      free (pseudo);

      /* Supprimer le document. */
      mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, NULL);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
}

int
récupérer_construction_en_cours (const struct coordonnées_île coordonnées,
				 char **nom_bâtiment,
				 time_t *heure_de_fin)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  int construction_en_cours = 0;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "bâtiments en construction");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    construction_en_cours = 1;

  if (construction_en_cours)
    {
      /* Récupérer le nom du bâtiment. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  *nom_bâtiment = strdup (bson_iter_utf8 (&iter, NULL));
	}

      /* Récupérer l’heure de fin de construction. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "fin de construction")
	  && BSON_ITER_HOLDS_DATE_TIME (&iter))
	{
	  *heure_de_fin = (time_t) (bson_iter_date_time (&iter) / 1000);
	}
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return construction_en_cours;
}

int
obtenir_recherche_en_cours (const char *pseudo, char **nom_recherche,
                            time_t *heure_de_fin)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  int construction_en_cours = 0;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "recherches en cours");

  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    construction_en_cours = 1;

  if (construction_en_cours)
    {
      /* Récupérer le nom de la recherche. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
        *nom_recherche = bson_iter_dup_utf8 (&iter, NULL);

      /* Récupérer l’heure de fin de construction. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "fin de recherche")
	  && BSON_ITER_HOLDS_DATE_TIME (&iter))
        *heure_de_fin = (time_t) (bson_iter_date_time (&iter) / 1000);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return construction_en_cours;
}

int
récupérer_construction_navires_en_cours
(const struct coordonnées_île coordonnées,
 char **nom_navires, int *nombre, time_t* heure_de_fin)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  int construction_en_cours = 0;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "navires en construction");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    construction_en_cours = 1;

  if (construction_en_cours)
    {
      /* Récupérer le nom des navires. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  *nom_navires = strdup (bson_iter_utf8 (&iter, NULL));
	}

      /* Récupérons le nombre de navires */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nombre")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  *nombre = bson_iter_int32 (&iter);
	}

      /* Récupérer l’heure de fin de construction. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "fin de construction")
	  && BSON_ITER_HOLDS_DATE_TIME (&iter))
        *heure_de_fin = (time_t) (bson_iter_date_time (&iter) / 1000);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return construction_en_cours;
}

int
récupérer_construction_défenses_en_cours
(const struct coordonnées_île coordonnées,
 char **nom_défenses, int *nombre, time_t* heure_de_fin)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  int construction_en_cours = 0;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "défenses en construction");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (mongoc_cursor_next (curseur, &doc))
    construction_en_cours = 1;

  if (construction_en_cours)
    {
      /* Récupérer le nom des navires. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  *nom_défenses = strdup (bson_iter_utf8 (&iter, NULL));
	}

      /* Récupérons le nombre de navires */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nombre")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  *nombre = bson_iter_int32 (&iter);
	}

      /* Récupérer l’heure de fin de construction. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "fin de construction")
	  && BSON_ITER_HOLDS_DATE_TIME (&iter))
        *heure_de_fin = (time_t) (bson_iter_date_time (&iter) / 1000);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return construction_en_cours;
}

int
récupérer_informations_secteur (const int numéro_océan,
				const int numéro_secteur,
				struct informations_secteur *secteur)
{
  for (int i = 0; i < 15; i++)
    {
      secteur->nom_île[i] = NULL;
      secteur->propriétaire_île[i] = NULL;
    }

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", numéro_océan);
  BSON_APPEND_INT32 (requête, "secteur", numéro_secteur);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  int emplacement_courant = 0;

  while (mongoc_cursor_next (curseur, &doc))
    {
      /* On récupère l’emplacement de l’île. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "emplacement")
	  && BSON_ITER_HOLDS_INT32 (&iter))
	{
	  emplacement_courant = bson_iter_int32 (&iter);
	}

      /* On enregistre le nom de l’île. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "nom")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  secteur->nom_île[emplacement_courant-1] = strdup (bson_iter_utf8 (&iter,
									    NULL));
	}

      /* On enregistre le propriétaire de l’île. */
      if (bson_iter_init (&iter, doc)
	  && bson_iter_find (&iter, "propriétaire")
	  && BSON_ITER_HOLDS_UTF8 (&iter))
	{
	  secteur->propriétaire_île[emplacement_courant-1] =
	    strdup (bson_iter_utf8 (&iter, NULL));
	}
    }

  /* Quand le pointeur vers le nom ou le propriétaire vaut « NULL », */
  /* on remplit la chaine avec le mot « LIBRE ». */
  for (int i = 0; i < 15; ++i)
    {
      if (secteur->nom_île[i] == NULL)
	secteur->nom_île[i] = strdup ("LIBRE");

      if (secteur->propriétaire_île[i] == NULL)
	secteur->propriétaire_île[i] = strdup ("LIBRE");
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

void
libérer_informations_secteur (struct informations_secteur *secteur)
{
  for (int i = 0; i < 15; i++)
    {
      free (secteur->nom_île[i]);
      free (secteur->propriétaire_île[i]);
    }
}

int
inscrire_expédition (const char *type_mission,
		     const char *nom_commanditaire,
		     const struct coordonnées_île île_départ,
		     const struct coordonnées_île île_arrivée,
		     const struct ressources ressources,
		     time_t heure_fin, const struct navires flotte,
		     bson_t **document)
{
  bson_oid_t identifiant;
  bson_oid_init (&identifiant, NULL);

  /* On récupère le propriétaire de l’île cible. */
  char *nom_cible = NULL;
  if (!récupérer_propriétaire_île (île_arrivée, &nom_cible))
    return 0;

  *document = BCON_NEW ("_id", BCON_OID (&identifiant),
                        "mission", BCON_UTF8 (type_mission),
			"fin", BCON_DATE_TIME (heure_fin * 1000),

			"commanditaire", BCON_UTF8 (nom_commanditaire),
                        "cible", BCON_UTF8 (nom_cible),

			"île de départ", "{",
			"océan", BCON_INT32 (île_départ.océan),
			"secteur", BCON_INT32 (île_départ.secteur),
			"emplacement", BCON_INT32 (île_départ.emplacement), "}",

			"île d’arrivée", "{",
			"océan", BCON_INT32 (île_arrivée.océan),
			"secteur", BCON_INT32 (île_arrivée.secteur),
			"emplacement", BCON_INT32 (île_arrivée.emplacement), "}",

			"navires", "{",
			"bateau espion", BCON_INT32 (flotte.bateau_espion),
			"bateau transport léger", BCON_INT32 (flotte.bateau_transport_léger),
			"bateau transport lourd", BCON_INT32 (flotte.bateau_transport_lourd),
			"canonnière", BCON_INT32 (flotte.canonnière),
			"frégate", BCON_INT32 (flotte.frégate),
			"corvette", BCON_INT32 (flotte.corvette),
			"brick", BCON_INT32 (flotte.brick),
			"galion", BCON_INT32 (flotte.galion),
			"navire de ligne", BCON_INT32 (flotte.navire_de_ligne), "}",

			"ressources", "{",
			"bois", BCON_INT32 (ressources.bois),
			"métal", BCON_INT32 (ressources.métal),
			"poudre à canon", BCON_INT32 (ressources.poudre_à_canon), "}");
  free (nom_cible);

  /* Insérer l’expédition. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "expéditions");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, *document, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  mongoc_collection_destroy (collection);

  mongoc_client_destroy (client);

  return 1;
}

int
inscrire_rapport_espionnage (const char *destinataire,
			     const char *pseudo_cible,
			     const struct coordonnées_île île_cible,
			     const struct ressources ressources,
			     const struct navires flotte,
			     const struct défenses défenses,
			     const struct niveau_bâtiments bâtiments,
			     const struct niveau_recherches recherches)
{
  /* On crée le rapport d’espionnage. */
  bson_t *rapport_espionnage;
  rapport_espionnage =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "cible", BCON_UTF8 (pseudo_cible),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("espionnage"),

	      "île cible", "{",
	      "océan", BCON_INT32 (île_cible.océan),
	      "secteur", BCON_INT32 (île_cible.secteur),
	      "emplacement",
	      BCON_INT32 (île_cible.emplacement), "}",

	      "ressources", "{",
	      "bois", BCON_INT32 (ressources.bois),
	      "métal", BCON_INT32 (ressources.métal),
	      "poudre à canon",
	      BCON_INT32 (ressources.poudre_à_canon),
	      "travailleurs",
	      BCON_INT32 (ressources.travailleurs), "}",

	      "flotte", "{",
	      "bateau espion",
	      BCON_INT32 (flotte.bateau_espion),
	      "bateau transport léger",
	      BCON_INT32 (flotte.bateau_transport_léger),
	      "bateau transport lourd",
	      BCON_INT32 (flotte.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (flotte.canonnière),
	      "frégate", BCON_INT32 (flotte.frégate),
	      "corvette", BCON_INT32 (flotte.corvette),
	      "brick", BCON_INT32 (flotte.brick),
	      "galion", BCON_INT32 (flotte.galion),
	      "navire de ligne",
	      BCON_INT32 (flotte.navire_de_ligne), "}",

	      "défenses", "{",
	      "mur", BCON_INT32(défenses.murs),
	      "canon", BCON_INT32 (défenses.canons),
              "mortier", BCON_INT32 (défenses.mortiers),
	      "mine", BCON_INT32 (défenses.mines),
	      "fort", BCON_INT32 (défenses.forts), "}",

	      "bâtiments", "{",
	      "forêt", BCON_INT32 (bâtiments.forêt),
	      "mine de métal", BCON_INT32 (bâtiments.mine_métal),
	      "fabrique poudre canon", BCON_INT32 (bâtiments.fabrique_poudre_canon),
	      "hangar bois", BCON_INT32 (bâtiments.hangar_bois),
	      "hangar métal", BCON_INT32 (bâtiments.hangar_métal),
	      "poudrière", BCON_INT32 (bâtiments.poudrière),
	      "quartier travailleurs", BCON_INT32 (bâtiments.quartier_travailleurs),
	      "chantier naval", BCON_INT32 (bâtiments.chantier_naval),
	      "laboratoire recherche", BCON_INT32 (bâtiments.laboratoire_recherche),
	      "camp formation travailleurs", BCON_INT32 (bâtiments.camp_formation_travailleurs),
              "taverne", BCON_INT32 (bâtiments.taverne), "}",

	      "recherches", "{",
	      "espionnage", BCON_INT32 (recherches.espionnage),
	      "voilure", BCON_INT32 (recherches.voilure),
	      "coque", BCON_INT32 (recherches.coque),
	      "canons", BCON_INT32 (recherches.canons),
	      "formation d’officiers",
	      BCON_INT32 (recherches.formation_officiers), "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, rapport_espionnage, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (rapport_espionnage);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
inscrire_retour (const char *destinataire,
		 const struct coordonnées_île île_retour,
		 const struct coordonnées_île île_départ,
		 const struct ressources ressources)
{
  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("retour"),

	      "île départ", "{",
	      "océan", BCON_INT32 (île_départ.océan),
	      "secteur", BCON_INT32 (île_départ.secteur),
	      "emplacement",
	      BCON_INT32 (île_départ.emplacement), "}",

	      "île retour", "{",
	      "océan", BCON_INT32 (île_retour.océan),
	      "secteur", BCON_INT32 (île_retour.secteur),
	      "emplacement", BCON_INT32 (île_retour.emplacement), "}",

	      "ressources", "{",
	      "bois", BCON_INT32 (ressources.bois),
	      "métal", BCON_INT32 (ressources.métal),
	      "poudre à canon", BCON_INT32 (ressources.poudre_à_canon), "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int inscrire_rapport_transport_reçu (const struct coordonnées_île île_départ,
				     const struct coordonnées_île île_arrivée,
				     const struct ressources ressources_reçues)
{
  /* On trouve le destinataire. */
  char *destinataire = NULL;
  récupérer_propriétaire_île (île_arrivée, &destinataire);

  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("transport reçu"),

	      "île départ", "{",
	      "océan", BCON_INT32 (île_départ.océan),
	      "secteur", BCON_INT32 (île_départ.secteur),
	      "emplacement",
	      BCON_INT32 (île_départ.emplacement), "}",

	      "île arrivée", "{",
	      "océan", BCON_INT32 (île_arrivée.océan),
	      "secteur", BCON_INT32 (île_arrivée.secteur),
	      "emplacement", BCON_INT32 (île_arrivée.emplacement), "}",

	      "ressources", "{",
	      "bois", BCON_INT32 (ressources_reçues.bois),
	      "métal", BCON_INT32 (ressources_reçues.métal),
	      "poudre à canon", BCON_INT32 (ressources_reçues.poudre_à_canon),
	      "}");
  free (destinataire);

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
inscrire_rapport_transport_effectué (const struct coordonnées_île île_départ,
				     const struct coordonnées_île île_arrivée,
				     const struct ressources ressources_envoyées)
{
  /* On trouve le destinataire. */
  char *destinataire = NULL;
  récupérer_propriétaire_île (île_départ, &destinataire);

  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("transport effectué"),

	      "île départ", "{",
	      "océan", BCON_INT32 (île_départ.océan),
	      "secteur", BCON_INT32 (île_départ.secteur),
	      "emplacement",
	      BCON_INT32 (île_départ.emplacement), "}",

	      "île arrivée", "{",
	      "océan", BCON_INT32 (île_arrivée.océan),
	      "secteur", BCON_INT32 (île_arrivée.secteur),
	      "emplacement", BCON_INT32 (île_arrivée.emplacement), "}",

	      "ressources", "{",
	      "bois", BCON_INT32 (ressources_envoyées.bois),
	      "métal", BCON_INT32 (ressources_envoyées.métal),
	      "poudre à canon", BCON_INT32 (ressources_envoyées.poudre_à_canon),
	      "}");
  free (destinataire);

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int inscrire_rapport_stationnement_effectué
(const struct coordonnées_île île_départ,
 const struct coordonnées_île île_arrivée,
 const struct ressources ressources_envoyées,
 const struct navires flotte_envoyée)
{
  /* On trouve le destinataire. */
  char *destinataire = NULL;
  récupérer_propriétaire_île (île_départ, &destinataire);

  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("stationnement effectué"),

	      "île départ", "{",
	      "océan", BCON_INT32 (île_départ.océan),
	      "secteur", BCON_INT32 (île_départ.secteur),
	      "emplacement",
	      BCON_INT32 (île_départ.emplacement), "}",

	      "île arrivée", "{",
	      "océan", BCON_INT32 (île_arrivée.océan),
	      "secteur", BCON_INT32 (île_arrivée.secteur),
	      "emplacement", BCON_INT32 (île_arrivée.emplacement), "}",

	      "ressources", "{",
	      "bois", BCON_INT32 (ressources_envoyées.bois),
	      "métal", BCON_INT32 (ressources_envoyées.métal),
	      "poudre à canon", BCON_INT32 (ressources_envoyées.poudre_à_canon),
              "}",

	      "navires", "{",
	      "bateau espion", BCON_INT32 (flotte_envoyée.bateau_espion),
	      "bateau de transport léger", BCON_INT32 (flotte_envoyée.bateau_transport_léger),
	      "bateau de transport lourd", BCON_INT32 (flotte_envoyée.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (flotte_envoyée.canonnière),
	      "corvette", BCON_INT32 (flotte_envoyée.corvette),
	      "frégate", BCON_INT32 (flotte_envoyée.frégate),
	      "brick", BCON_INT32 (flotte_envoyée.brick),
	      "galion", BCON_INT32 (flotte_envoyée.galion),
	      "navire de ligne", BCON_INT32 (flotte_envoyée.navire_de_ligne),
              "}");
  free (destinataire);

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
inscrire_rapport_attaque (const char *destinataire,
			  const char *pseudo_cible,
			  const struct coordonnées_île île_cible,
			  const char *issue_du_combat,
			  const struct ressources ressources_récupérées,
			  const struct navires flotte_après_combat,
			  const struct navires navires_perdus,
			  const struct navires navires_détruits,
			  const struct défenses défenses_détruites)
{
  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "cible", BCON_UTF8 (pseudo_cible),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("attaque"),

	      "issue du combat", BCON_UTF8 (issue_du_combat),

	      "île cible", "{",
	      "océan", BCON_INT32 (île_cible.océan),
	      "secteur", BCON_INT32 (île_cible.secteur),
	      "emplacement",
	      BCON_INT32 (île_cible.emplacement), "}",

	      "ressources obtenues", "{",
	      "bois", BCON_INT32 (ressources_récupérées.bois),
	      "métal", BCON_INT32 (ressources_récupérées.métal),
	      "poudre à canon", BCON_INT32 (ressources_récupérées.poudre_à_canon),
              "}"

	      "navires perdus", "{",
	      "bateau espion", BCON_INT32 (navires_perdus.bateau_espion),
	      "bateau de transport léger", BCON_INT32 (navires_perdus.bateau_transport_léger),
	      "bateau de transport lourd", BCON_INT32 (navires_perdus.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (navires_perdus.canonnière),
	      "corvette", BCON_INT32 (navires_perdus.corvette),
	      "frégate", BCON_INT32 (navires_perdus.frégate),
	      "brick", BCON_INT32 (navires_perdus.brick),
	      "galion", BCON_INT32 (navires_perdus.galion),
	      "navire de ligne", BCON_INT32 (navires_perdus.navire_de_ligne), "}",

	      "navires détruits", "{",
	      "bateau espion", BCON_INT32 (navires_détruits.bateau_espion),
	      "bateau de transport léger", BCON_INT32 (navires_détruits.bateau_transport_léger),
	      "bateau de transport lourd", BCON_INT32 (navires_détruits.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (navires_détruits.canonnière),
	      "corvette", BCON_INT32 (navires_détruits.corvette),
	      "frégate", BCON_INT32 (navires_détruits.frégate),
	      "brick", BCON_INT32 (navires_détruits.brick),
	      "galion", BCON_INT32 (navires_détruits.galion),
	      "navire de ligne", BCON_INT32 (navires_détruits.navire_de_ligne), "}",

	      "défenses détruites", "{",
	      "mur", BCON_INT32 (défenses_détruites.murs),
	      "mine", BCON_INT32 (défenses_détruites.mines),
	      "canon", BCON_INT32 (défenses_détruites.canons),
              "mortier", BCON_INT32 (défenses_détruites.mortiers),
	      "fort", BCON_INT32 (défenses_détruites.forts), "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

/**
 * Permet d’inscrire un rapport de défense dans la collection de messages.
 */
int inscrire_rapport_défense (const char *destinataire,
			      const char *pseudo_attaquant,
			      const struct coordonnées_île île_attaquant,
			      const struct coordonnées_île île_cible,
			      const char *issue_du_combat,
			      const struct navires flotte_attaquante,
			      const struct navires navires_détruits,
			      const struct ressources ressources_perdues,
			      const struct navires navires_perdus,
			      const struct défenses défenses_perdues)
{
  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "attaquant", BCON_UTF8 (pseudo_attaquant),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("défense"),

	      "issue du combat", BCON_UTF8 (issue_du_combat),

	      "île attaquant", "{",
	      "océan", BCON_INT32 (île_attaquant.océan),
	      "secteur", BCON_INT32 (île_attaquant.secteur),
	      "emplacement",
	      BCON_INT32 (île_attaquant.emplacement), "}",

	      "île cible", "{",
	      "océan", BCON_INT32 (île_cible.océan),
	      "secteur", BCON_INT32 (île_cible.secteur),
	      "emplacement",
	      BCON_INT32 (île_cible.emplacement), "}",

	      "ressources perdues", "{",
	      "bois", BCON_INT32 (ressources_perdues.bois),
	      "métal", BCON_INT32 (ressources_perdues.métal),
	      "poudre à canon", BCON_INT32 (ressources_perdues.poudre_à_canon),

	      "flotte attaquante", "{",
	      "bateau espion", BCON_INT32 (flotte_attaquante.bateau_espion),
	      "bateau transport léger", BCON_INT32 (flotte_attaquante.bateau_transport_léger),
	      "bateau transport lourd", BCON_INT32 (flotte_attaquante.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (flotte_attaquante.canonnière),
	      "corvette", BCON_INT32 (flotte_attaquante.corvette),
	      "frégate", BCON_INT32 (flotte_attaquante.frégate),
	      "brick", BCON_INT32 (flotte_attaquante.brick),
	      "galion", BCON_INT32 (flotte_attaquante.galion),
	      "navire de ligne", BCON_INT32 (flotte_attaquante.navire_de_ligne), "}",

	      "navires détruits", "{",
	      "bateau espion", BCON_INT32 (navires_détruits.bateau_espion),
	      "bateau transport léger", BCON_INT32 (navires_détruits.bateau_transport_léger),
	      "bateau transport lourd", BCON_INT32 (navires_détruits.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (navires_détruits.canonnière),
	      "corvette", BCON_INT32 (navires_détruits.corvette),
	      "frégate", BCON_INT32 (navires_détruits.frégate),
	      "brick", BCON_INT32 (navires_détruits.brick),
	      "galion", BCON_INT32 (navires_détruits.galion),
	      "navire de ligne", BCON_INT32 (navires_détruits.navire_de_ligne), "}",

	      "navires perdus", "{",
	      "bateau espion", BCON_INT32 (navires_perdus.bateau_espion),
	      "bateau transport léger", BCON_INT32 (navires_perdus.bateau_transport_léger),
	      "bateau transport lourd", BCON_INT32 (navires_perdus.bateau_transport_lourd),
	      "canonnière", BCON_INT32 (navires_perdus.canonnière),
	      "corvette", BCON_INT32 (navires_perdus.corvette),
	      "frégate", BCON_INT32 (navires_perdus.frégate),
	      "brick", BCON_INT32 (navires_perdus.brick),
	      "galion", BCON_INT32 (navires_perdus.galion),
	      "navire de ligne", BCON_INT32 (navires_perdus.navire_de_ligne), "}",

	      "défenses perdues", "{",
	      "mur", BCON_INT32 (défenses_perdues.murs),
	      "mine", BCON_INT32 (défenses_perdues.mines),
	      "canon", BCON_INT32 (défenses_perdues.canons),
              "mortier", BCON_INT32 (défenses_perdues.mortiers),
	      "fort", BCON_INT32 (défenses_perdues.forts), "}", "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
mettre_à_jour_ressources (const struct coordonnées_île coordonnées)
{
  struct ressources res = {0, 0, 0, 0};
  return modifier_ressources (coordonnées, res);
}

int
récupérer_nombre_messages (const char *destinataire)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  int64_t nombre_messages;


  bson_t *message;
  message = BCON_NEW ("destinataire", BCON_UTF8 (destinataire));

  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");
  nombre_messages = mongoc_collection_count (collection, MONGOC_QUERY_NONE, message, 0, 0, NULL, NULL);

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  if (nombre_messages < 0)
    nombre_messages = 0;

  return nombre_messages;
}

int
récupérer_message (const char *destinataire, unsigned numéro, char **message)
{
  int succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;

  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  bson_t *filtre;
  bson_t *options;
  mongoc_cursor_t *curseur;
  const bson_t *document;

  filtre = BCON_NEW ("destinataire", BCON_UTF8 (destinataire));

  options = BCON_NEW ("sort", "{", "date", BCON_INT32 (1), "}",
		      "projection", "{", "_id", BCON_BOOL (false), "}");

  curseur = mongoc_collection_find_with_opts (collection, filtre, options, NULL);

  unsigned document_courant = 1;
  while (mongoc_cursor_next (curseur, &document))
    {
      if (document_courant == numéro)
	{
	  *message = bson_as_json (document, NULL);
	  succès = 1;
	  break;
	}
      else
	++document_courant;
    }

  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
  mongoc_cursor_destroy (curseur);
  bson_destroy (filtre);
  bson_destroy (options);

  return succès;
}

void
supprimer_anciens_messages ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *document;
  bson_t *filtre;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "messages");

  /* On supprime les messages plus vieux d’une semaine. */
  filtre = BCON_NEW ("date", "{", "$lt",
		     BCON_DATE_TIME ((time (NULL) - 604800) * 1000),
		     "}");

  curseur = mongoc_collection_find_with_opts (collection, filtre, NULL, NULL);

  while (mongoc_cursor_next (curseur, &document))
    {
      /* Supprimer le document. */
      mongoc_collection_remove (collection, MONGOC_REMOVE_SINGLE_REMOVE, document, NULL, NULL);
    }

  bson_destroy (filtre);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
}

char*
obtenir_informations_îles_joueur (const char *pseudo)
{
  char *chaine_informations = NULL;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *document;
  bson_t *filtre;
  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau",
					     "joueurs");
  filtre = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));
  curseur = mongoc_collection_find_with_opts (collection, filtre, NULL, NULL);

  mongoc_cursor_next (curseur, &document);
  if (!document)
    goto erreur;

  bson_iter_t itérateur;
  int nombre_d_îles = 0;
  if (bson_iter_init (&itérateur, document)
      && bson_iter_find (&itérateur, "nb_îles")
      && BSON_ITER_HOLDS_INT32 (&itérateur))
    {
      nombre_d_îles = bson_iter_int32 (&itérateur);
    }
  if (nombre_d_îles == 0)
    goto erreur;

  bson_t informations;
  bson_init (&informations);
  bson_append_int32 (&informations, "nombre d’îles", -1, nombre_d_îles);

  bson_t île;
  for (unsigned i = 1; i <= nombre_d_îles; ++i)
    {
      struct coordonnées_île coordonnées;
      if (!trouver_coordonnées_île (pseudo, i, &coordonnées))
	goto erreur2;
      char *clef = NULL;
      unsigned longueur_clef = snprintf (NULL, 0, "île%d", i) + 2;
      clef = malloc (sizeof (char) * longueur_clef);
      snprintf (clef, longueur_clef, "île%d", i);

      bson_append_document_begin (&informations, clef, -1, &île);
      BSON_APPEND_INT32 (&île, "océan", coordonnées.océan);
      BSON_APPEND_INT32 (&île, "secteur", coordonnées.secteur);
      BSON_APPEND_INT32 (&île, "emplacement", coordonnées.emplacement);
      char *nom_île = obtenir_nom_île (coordonnées);
      BSON_APPEND_UTF8 (&île, "nom", nom_île);
      bson_append_document_end (&informations, &île);

      free (nom_île);
      free (clef);
    }

  chaine_informations = bson_as_json (&informations, NULL);

 erreur2:
  bson_destroy (&informations);
 erreur:
  bson_destroy (filtre);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return chaine_informations;
}

char*
obtenir_nom_île (const struct coordonnées_île coordonnées)
{
  char *nom_île = NULL;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *document;
  bson_t *requête;


  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  requête = bson_new ();
  BSON_APPEND_INT32 (requête, "océan", coordonnées.océan);
  BSON_APPEND_INT32 (requête, "secteur", coordonnées.secteur);
  BSON_APPEND_INT32 (requête, "emplacement", coordonnées.emplacement);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &document))
    goto erreur;

  bson_iter_t itérateur;
  if (bson_iter_init (&itérateur, document)
      && bson_iter_find (&itérateur, "nom")
      && BSON_ITER_HOLDS_UTF8 (&itérateur))
    nom_île = strdup (bson_iter_utf8 (&itérateur, NULL));

 erreur:
  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return nom_île;
}

int
inscrire_rapport_colonisation_infructueuse (const struct coordonnées_île île,
					    const char *destinataire)
{
  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("colonisation infructueuse"),

	      "île", "{",
	      "océan", BCON_INT32 (île.océan),
	      "secteur", BCON_INT32 (île.secteur),
	      "emplacement", BCON_INT32 (île.emplacement), "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int inscrire_rapport_colonisation_réussie (const struct coordonnées_île île,
					   const char *destinataire)
{
  /* On crée le message. */
  bson_t *message;
  message =
    BCON_NEW ("destinataire", BCON_UTF8 (destinataire),
	      "date", BCON_DATE_TIME (time (NULL) * 1000),
	      "type", BCON_UTF8 ("colonisation réussie"),

	      "île", "{",
	      "océan", BCON_INT32 (île.océan),
	      "secteur", BCON_INT32 (île.secteur),
	      "emplacement", BCON_INT32 (île.emplacement), "}");

  /* On doit ensuite insérer ce rapport dans la base de donnée. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;
  bson_error_t erreur;


  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "messages");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, message, NULL, &erreur))
    {
      fprintf (stderr, "%s\n", erreur.message);
      return 0;
    }

  bson_destroy (message);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
ajouter_île (const struct coordonnées_île île, const char *colon,
	     const struct ressources ressources,
	     const struct navires flotte)
{
  if (!vérifier_existence_joueur (colon))
    return 0;
  if (vérifier_existence_île (île))
      return 0;

  /* Créer île. */
  bson_t *document_île = NULL;
  document_île = BCON_NEW ("propriétaire", BCON_UTF8 (colon),
			   "dernière_màj", BCON_DATE_TIME (time (NULL) * 1000),

			   "nom", BCON_UTF8 ("île secondaire"),
			   "océan", BCON_INT32 (île.océan),
			   "secteur", BCON_INT32 (île.secteur),
			   "emplacement", BCON_INT32 (île.emplacement),

			   "ressources", "{",
			   "bois", BCON_INT32 (1000+ressources.bois),
			   "métal", BCON_INT32 (1000+ressources.métal),
			   "poudre_à_canon",
			   BCON_INT32 (ressources.poudre_à_canon),
			   "travailleurs", BCON_INT32 (0), "}",

			   "bâtiments", "{",
			   "forêt", BCON_INT32 (0),
			   "mine_de_métal", BCON_INT32 (0),
			   "fabrique_poudre_canon", BCON_INT32 (0),
			   "hangar_bois", BCON_INT32 (0),
			   "hangar_métal", BCON_INT32 (0),
			   "poudrière", BCON_INT32 (0),
			   "quartier_travailleurs", BCON_INT32 (0),
			   "chantier_naval", BCON_INT32 (0),
			   "laboratoire_recherche", BCON_INT32 (0),
                           "camp_formation_travailleurs", BCON_INT32 (0),
			   "taverne", BCON_INT32 (0), "}",

			   "défenses", "{",
			   "mur", BCON_INT32(0),
			   "canon", BCON_INT32 (0),
                           "mortier", BCON_INT32 (0),
			   "mine", BCON_INT32 (0),
			   "fort", BCON_INT32 (0), "}",

			   "navires", "{",
			   "bateau_espion", BCON_INT32 (0),
			   "bateau_transport_léger", BCON_INT32 (0),
			   "bateau_transport_lourd", BCON_INT32 (0),
			   "canonnière", BCON_INT32 (0),
			   "frégate", BCON_INT32 (0),
			   "corvette", BCON_INT32 (0),
			   "brick", BCON_INT32 (0),
			   "galion", BCON_INT32 (0),
			   "navire_de_ligne", BCON_INT32 (0), "}");

  /* Insérer l’île. */
  mongoc_client_t *client = NULL;
  mongoc_collection_t *collection = NULL;

  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");

  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, document_île, NULL,
                                 NULL))
    {
      if (document_île)
        bson_destroy (document_île);
      mongoc_collection_destroy (collection);
      mongoc_client_destroy (client);
      return 0;
    }
  bson_destroy (document_île);
  mongoc_collection_destroy (collection);

  int nombre_îles = obtenir_nombre_îles (colon);
  if (nombre_îles == 0)
    return 0;
  ++nombre_îles;

  unsigned int longueur_clef = snprintf (NULL, 0, "île%d", nombre_îles) + 2;
  char *clef = malloc (sizeof (char) * longueur_clef);
  snprintf (clef, longueur_clef, "île%d", nombre_îles);
  bson_t *nouvelle_île = BCON_NEW ("$set", "{",
                                   clef, "{",
                                   "océan", BCON_INT32 (île.océan),
                                   "secteur", BCON_INT32 (île.secteur),
                                   "emplacement", BCON_INT32 (île.emplacement),
                                   "}", "}");
  bson_t *requête = BCON_NEW ("pseudo", colon);
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");
  mongoc_collection_update (collection, MONGOC_UPDATE_NONE,
                            requête, nouvelle_île, NULL, NULL);
  modifier_nombre_îles (colon, nombre_îles);


  if (nouvelle_île)
    bson_destroy (nouvelle_île);
  if (requête)
    bson_destroy (requête);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return 1;
}

int
modifier_nombre_îles (const char *pseudo, unsigned nombre_d_îles)
{
  mongoc_collection_t *collection;
  mongoc_client_t *client;
  bson_t *document = NULL;
  bson_t *requête = NULL;

  int succès = 0;

  mongoc_init ();

  client = mongoc_client_new ("mongodb://localhost:27017");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));
  document = BCON_NEW ("$set", "{", "nb_îles", BCON_INT32 (nombre_d_îles), "}");

  bson_error_t erreur;
  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE, requête,
                                document, NULL, &erreur))
    succès = 1;
  else
    fprintf (stderr, "Erreur : %s.\n", erreur.message);

  if (requête)
    bson_destroy (requête);
  if (document)
    bson_destroy (document);

  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
obtenir_nombre_îles (const char *pseudo)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *document;
  bson_t *requête;

  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  requête = bson_new ();
  BSON_APPEND_UTF8 (requête, "pseudo", pseudo);

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);
  mongoc_cursor_next (curseur, &document);
  if (!document)
    {
      if (requête)
        bson_destroy (requête);
      mongoc_cursor_destroy (curseur);
      mongoc_collection_destroy (collection);
      mongoc_client_destroy (client);
      return 0;
    }

  bson_iter_t itérateur;
  int nombre_îles = 0;
  if (bson_iter_init (&itérateur, document)
      && bson_iter_find (&itérateur, "nb_îles")
      && BSON_ITER_HOLDS_INT32 (&itérateur))
    nombre_îles = bson_iter_int32 (&itérateur);

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return nombre_îles;
}
