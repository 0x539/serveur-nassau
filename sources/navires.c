/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions relatives aux navires.
*/

#include <stdio.h>

#include "navires.h"

int
nombre_total_navires (const struct navires navires)
{
  int nombre_total = navires.bateau_espion;
  nombre_total += navires.bateau_transport_léger;
  nombre_total += navires.bateau_transport_lourd;
  nombre_total += navires.canonnière;
  nombre_total += navires.frégate;
  nombre_total += navires.corvette;
  nombre_total += navires.brick;
  nombre_total += navires.galion;
  nombre_total += navires.navire_de_ligne;

  return nombre_total;
}

int
ajouter_structures_navires (struct navires *destination,
			    const struct navires source)
{
  struct navires copie_destination = *destination;

  if (destination == NULL)
    return 0;

  destination->bateau_espion += source.bateau_espion;
  if (destination->bateau_espion < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->bateau_transport_léger += source.bateau_transport_léger;
  if (destination->bateau_transport_léger < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->bateau_transport_lourd += source.bateau_transport_lourd;
  if (destination->bateau_transport_lourd < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->canonnière += source.canonnière;
  if (destination->canonnière < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->frégate += source.frégate;
  if (destination->frégate < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->corvette += source.corvette;
  if (destination->corvette < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->brick += source.brick;
  if (destination->brick < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->galion += source.galion;
  if (destination->galion < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->navire_de_ligne += source.navire_de_ligne;
  if (destination->navire_de_ligne < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  return 1;
}

int
soustraire_structures_navires (struct navires *destination,
			       const struct navires source)
{
  struct navires opposé_source = source;
  opposé_source.bateau_espion *= -1;
  opposé_source.bateau_transport_léger *= -1;
  opposé_source.bateau_transport_lourd *= -1;
  opposé_source.canonnière *= -1;
  opposé_source.corvette *= -1;
  opposé_source.frégate *= -1;
  opposé_source.brick *= -1;
  opposé_source.galion *= -1;
  opposé_source.navire_de_ligne *= -1;

  return ajouter_structures_navires (destination, opposé_source);
}

int
nom_navire_correct (const char* nom_navire)
{
  int correct = 0;

  if (!strcmp (nom_navire, "bateau_espion"))
    correct = 1;
  else if (!strcmp (nom_navire, "bateau_transport_léger"))
    correct = 1;
  else if (!strcmp (nom_navire, "bateau_transport_lourd"))
    correct = 1;
  else if (!strcmp (nom_navire, "canonnière"))
    correct = 1;
  else if (!strcmp (nom_navire, "frégate"))
    correct = 1;
  else if (!strcmp (nom_navire, "corvette"))
    correct = 1;
  else if (!strcmp (nom_navire, "brick"))
    correct = 1;
  else if (!strcmp (nom_navire, "galion"))
    correct = 1;
  else if (!strcmp (nom_navire, "navire_de_ligne"))
    correct = 1;

  return correct;
}

long
obtenir_capacité_flotte (const struct navires flotte)
{
  long capacité = 0;

  capacité += (long) flotte.bateau_espion * CAPACITÉ_BATEAU_ESPION;
  capacité += (long) flotte.bateau_transport_léger * CAPACITÉ_BATEAU_TLÉ;
  capacité += (long) flotte.bateau_transport_lourd * CAPACITÉ_BATEAU_TLO;
  capacité += (long) flotte.canonnière * CAPACITÉ_CANONNIÈRE;
  capacité += (long) flotte.corvette * CAPACITÉ_CORVETTE;
  capacité += (long) flotte.frégate * CAPACITÉ_FRÉGATE;
  capacité += (long) flotte.brick * CAPACITÉ_BRICK;
  capacité += (long) flotte.galion * CAPACITÉ_GALION;
  capacité += (long) flotte.navire_de_ligne * CAPACITÉ_NDL;

  return capacité;
}

int
obtenir_vitesse_la_plus_longue (const struct navires flotte)
{
  const int membres[] =
    {
      flotte.bateau_espion,
      flotte.bateau_transport_léger,
      flotte.bateau_transport_lourd,
      flotte.canonnière,
      flotte.corvette,
      flotte.frégate,
      flotte.brick,
      flotte.galion,
      flotte.navire_de_ligne
    };

  int vitesses[] =
    {
      VITESSE_BATEAU_ESPION,
      VITESSE_BATEAU_TLÉ,
      VITESSE_BATEAU_TLO,
      VITESSE_CANONNIÈRE,
      VITESSE_CORVETTE,
      VITESSE_FRÉGATE,
      VITESSE_BRICK,
      VITESSE_GALION,
      VITESSE_NDL
    };

  unsigned vitesse = VITESSE_MAX;
  for (unsigned i = 0; i < NOMBRE_NAVIRES; ++i)
    {
      if (membres[i] > 0 && vitesses[i] < vitesse)
        vitesse = vitesses[i];
    }

  return vitesse;
}

void
afficher_navires (const struct navires flotte)
{
  printf ("%d bateau(x) espion(s).\n", flotte.bateau_espion);
  printf ("%d bateau(x) de transport léger.\n", flotte.bateau_transport_léger);
  printf ("%d bateau(x) de transport lourd.\n", flotte.bateau_transport_lourd);
  printf ("%d canonnière(s).\n", flotte.canonnière);
  printf ("%d corvette(s).\n", flotte.corvette);
  printf ("%d frégate(s).\n", flotte.frégate);
  printf ("%d brick(s).\n", flotte.brick);
  printf ("%d galion(s).\n", flotte.galion);
  printf ("%d navire(s) de ligne.\n", flotte.navire_de_ligne);
}
