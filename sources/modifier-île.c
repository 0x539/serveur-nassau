/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants succèssifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Le programme permettant de faciliter la correction d’erreurs.
*/

#include <stdio.h>
#include <stdlib.h>

#include "bd.h"

int
main (int argc, char *argv[])
{
  if (argc < 7)
    {
      fprintf (stderr, "Utilisation : %s océan secteur "
               "emplacement type_objet nom_objet valeur.\n",
               argv[0]);
      return 1;
    }

  int valeur = atoi (argv[6]);
  struct coordonnées_île coordonnées =
    {
      .océan = atoi (argv[1]),
      .secteur = atoi (argv[2]),
      .emplacement = atoi (argv[3])
    };

  if (!strcmp (argv[4], "navires")
      || !strcmp (argv[4], "bâtiments")
      || !strcmp (argv[4], "défenses")
      || !strcmp (argv[4], "ressources"))
    modifier_valeur_objet_sur_île (coordonnées, argv[4], argv[5], valeur);
  else
    return 2;

  return 0;
}
