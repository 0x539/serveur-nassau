/*
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016-2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants succèssifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Permet la création de compte depuis le client.
*/

#include "création-compte.h"
#include "bd.h"

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>

void
libérer_enfants ()
{
  int statut;

  while (waitpid (-1, &statut, WNOHANG) > 0);
}

void
créer_processus_création_compte ()
{
  /* Création de la fourche. */
  int identifiant_processus = fork ();
  if (identifiant_processus == 0)
    {
      /* Enfant */
      signal (SIGCHLD, libérer_enfants);
      création_compte ("4285");
      exit (0);
    }
  else if (identifiant_processus < (pid_t) 0)
    {
      /* Échec. */
      fprintf (stderr, "Échec lors de la création de la fourche"
               " pour la création de comptes.");
      return;
    }
}

int
obtenir_interface_serveur_tcp (const char *port)
{
  int interface;
  struct addrinfo informations, *résultat, *rés;

  memset (&informations, 0, sizeof (struct addrinfo));
  informations.ai_family = AF_INET6; /* Permet d’utiliser l’IPv4 et l’IPv6. */
  informations.ai_socktype = SOCK_STREAM; /* Utilisation de datagrammes. */
  informations.ai_flags = AI_PASSIVE;
  informations.ai_protocol = 0;
  informations.ai_canonname = NULL;
  informations.ai_addr = NULL;
  informations.ai_next = NULL;

  int retour = getaddrinfo (NULL, port, &informations, &résultat);
  if (retour != 0)
    {
      fprintf (stderr, "Erreur lors du lancement "
               "du serveur de création de compte (getaddrinfo).");
      return -1;
    }

  for (rés = résultat; rés != NULL; rés = résultat->ai_next)
    {
      interface = socket (rés->ai_family, rés->ai_socktype, rés->ai_protocol);
      if (interface == -1)
        continue;

      if (bind (interface, rés->ai_addr, rés->ai_addrlen) != -1)
        break;                  /* Succès */

      close (interface);
    }

  if (rés == NULL)
    {
      fprintf (stderr, "Erreur lors du lancement du "
               "serveur de création de compte (bind).");
      return -1;
    }

  freeaddrinfo (résultat);

  return interface;
}

void
création_compte (const char *port)
{
  int interface = obtenir_interface_serveur_tcp (port);
  if (interface == -1)
    return;

  if (listen (interface, 10))
    return;

  int client;

  while (1)
    {
      client = accept (interface, NULL, NULL);

      int f = fork ();
      if (f == 0)
        {
          protocole_création_compte (client);
          exit (0);
        }
      else if (f == -1)
        {
          fprintf (stderr, "Il s’est produit une erreur lors du traitement d’"
                   "une création de compte.");
          exit (1);
        }

close (client);
    }
}

static int
extraire_informations (const char *chaîne,
                       char **pseudo, char **mot_de_passe)
{
  if (pseudo == NULL || mot_de_passe == NULL)
    return -1;

  char *copie = strdup (chaîne);
  char *sauvegarde;
  char *élément = strtok_r (copie, " ", &sauvegarde);
  if (élément == NULL || strcmp (élément, "créer_compte"))
    return -1;

  élément = strtok_r (NULL, " ", &sauvegarde);
  if (élément == NULL)
    return -1;
  *pseudo = strdup (élément);

  élément = strtok_r (NULL, " ", &sauvegarde);
  if (élément == NULL)
    return -1;
  *mot_de_passe = strdup (élément);

  élément = strtok_r (NULL, " ", &sauvegarde);
  if (élément != NULL)
    {
      free (*pseudo);
      free (*mot_de_passe);
      return -1;
    }

  free (copie);
  return 1;
}

void
protocole_création_compte (int interface)
{
  char tampon[65536];
  unsigned long n = read (interface, &tampon, 65535);
  tampon[n] = 0;

  char *pseudo, *mot_de_passe;
  if (extraire_informations (tampon, &pseudo, &mot_de_passe) == -1)
    {
      const char *erreur = "Message malformé.";
      write (interface, erreur, strlen (erreur));
      close (interface);
      return;
    }

  if (vérifier_existence_joueur (pseudo))
    {
      const char *erreur = "Le joueur existe déjà.";
      write (interface, erreur, strlen (erreur));
      free (pseudo);
      free (mot_de_passe);
      close (interface);
      return;
    }

  const char *début =
    "python3 -c \"import sys\n"
    "from captcha.image import ImageCaptcha\n"
    "image = ImageCaptcha()\n"
    "image.write('";
  const char *milieu = "', '";
  const char *fin = "')\"";

  const char *caractères_contrôle =
    "abcdefghijklmnopqrstuvwxyz0123456789";

  int taille_nom_fichier = snprintf (NULL, 0,
                                     "/tmp/nassau-jeu-imitation-%d.png",
                                     getpid ()) + 1;
  char nom_fichier[taille_nom_fichier];
  snprintf (nom_fichier, taille_nom_fichier, "/tmp/nassau-jeu-imitation-%d.png", getpid ());

  srand (time (NULL));
  char code[7];
  code[6] = 0;
  for (unsigned i = 0; i < 6; ++i)
    {
      int position = rand () % (sizeof (caractères_contrôle) / sizeof (char));
      code[i] = caractères_contrôle[position];
    }

  const int taille_commande = snprintf (NULL, 0, "%s%s%s%s%s", début, code,
                                        milieu, nom_fichier, fin) + 1;
  char commande[taille_commande];
  snprintf (commande, taille_commande, "%s%s%s%s%s", début, code,
            milieu, nom_fichier, fin);
  system (commande);

  /* Ouverture du jeu d’imitation. */
  FILE *image = fopen (nom_fichier, "r");
  if (!image)
    fprintf (stderr, "Erreur lors de l’ouverture du fichier %s.\n", nom_fichier);

  /* On récupère la taille de l’image. */
  if (fseek (image, 0, SEEK_END) == -1)
    {
      fprintf (stderr, "Erreur lors de la lecture du fichier : %s.\n", nom_fichier);
      return;
    }
  long taille_fichier = ftell (image);
  rewind (image);
  /* On crée la chaîne contenant la taille du fichier. */
  char chaîne_taille_fichier[7];
  /* On la remplit de zéros. */
  for (unsigned i = 0; i < 7; ++i)
    chaîne_taille_fichier[i] = 0;
  /* On lui affecte la taille du fichier. */
  snprintf (chaîne_taille_fichier, 7, "%ld", taille_fichier);
  /* On l’envoi. */
  size_t total_envoyé = 0;
  while (total_envoyé < 7)
    {
      n = write (interface, chaîne_taille_fichier, 7);
      if (n == -1)
        return;
      total_envoyé += n;
    }

  n = fread (&tampon, 1, 65535, image);
  fclose (image);
  write (interface, &tampon, n);

  n = read (interface, &tampon, 65535);
  if (strncmp (tampon, code, 6))
    {
      const char *erreur = "Code de vérification incorrect.";
      write (interface, erreur, strlen (erreur));
      free (pseudo);
      free (mot_de_passe);
      close (interface);
      return;
    }

  /* Création du compte. */
  if (!créer_un_joueur (pseudo, mot_de_passe))
    {
      const char *erreur = "Erreur inconnue.";
      write (interface, erreur, strlen (erreur));
    }
  else
    {
      const char *réponse = "Le compte a bien été créé.";
      write (interface, réponse, strlen (réponse));
    }

  free (pseudo);
  free (mot_de_passe);
  close (interface);
}
