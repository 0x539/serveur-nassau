/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions relatives aux défenses.
*/

#include "défenses.h"

int
ajouter_structures_défenses (struct défenses *destination,
			     const struct défenses source)
{
  struct défenses copie_destination = *destination;

  if (destination == NULL)
    return 0;

  destination->murs += source.murs;
  if (destination->murs < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->mines += source.mines;
  if (destination->mines < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->canons += source.canons;
  if (destination->canons < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->mortiers += source.mortiers;
  if (destination->mortiers < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  destination->forts += source.forts;
  if (destination->forts < 0)
    {
      *destination = copie_destination;
      return 0;
    }

  return 1;
}

int
soustraire_structures_défenses (struct défenses *destination,
				const struct défenses source)
{
  struct défenses opposé_source = source;
  opposé_source.murs *= -1;
  opposé_source.mines *= -1;
  opposé_source.canons *= -1;
  opposé_source.mortiers *= -1;
  opposé_source.forts *= -1;

  return ajouter_structures_défenses (destination, opposé_source);
}
