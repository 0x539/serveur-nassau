/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions relatives aux expéditions.
*/

#include "expéditions.h"

/* Déclarations des fonctions statiques. */
static int
y_a_t_il_feu_rapide (struct objet_militaire *objet_attaquant,
                     struct objet_militaire *objet_attaqué);

static void
choisir_objet_pour_combat (int tour_attaquant, int feu_rapide,
                           int numéro_objet_attaquant,
                           struct objet_militaire **objet_attaquant,
                           struct objet_militaire **objet_attaqué,
                           struct objet_militaire *attaquant,
                           struct objet_militaire *défenseur,
                           unsigned nombre_attaquants,
                           unsigned nombre_défenseurs);

static void
exécuter_combat (struct objet_militaire *objet_attaquant,
                 struct objet_militaire *objet_attaqué);

static void
libération_objet_militaire_détruit (int tour_attaquant,
                                    struct objet_militaire *objet_attaqué,
                                    struct objet_militaire **attaquant,
                                    struct objet_militaire **défenseur,
                                    unsigned *nombre_attaquants,
                                    unsigned *nombre_défenseurs);

static void
exécuter_tour (int *tour_attaquant, unsigned *nombre_attaquants,
               unsigned *nombre_défenseurs, unsigned *numéro_objet_attaquant,
               int *feu_rapide, struct objet_militaire **attaquant,
               struct objet_militaire **défenseur,
               int *num_tour);

void
créer_processus_expéditions ()
{
  /* Création des tubes de communication. */
  int tubes[2];
  if (pipe (tubes))
    {
      fprintf (stderr,
	       "Erreur lors de la création des tubes de communication "
	       "avec le processus se chargeant des expéditions.");

      return;
    }

  /* Création de la fourche. */
  identifiant_processus_expédition = fork ();
  if (identifiant_processus_expédition == (pid_t) 0)
    {
      /* Enfant */
      close (tubes[1]);
      tube_expéditions = tubes[0];

      /* À faire : mettre un drapeau à vrai et le faire ailleurs. */
      signal (SIGUSR1, lire_données_tube_expéditions);
      attendre_expéditions ();

      exit (EXIT_SUCCESS);
    }
  else if (identifiant_processus_expédition < (pid_t) 0)
    {
      /* Échec. */
      fprintf (stderr, "Échec lors de la création de la fourche.");
      return;
    }
  else
    {
      /* Parent. */
      close (tubes[0]);
      tube_expéditions = tubes[1];
    }
}

void
attendre_expéditions ()
{
  expéditions = NULL;
  remplir_liste_expéditions ();

  unsigned int temps_attente_restant = 0;
  double temps_à_attendre;

  while (1)
    {
      if (expéditions == NULL)
	{
	  /* Attendre le premier signal utilisateur. */
	  sleep (600);
	  continue;
	}

      temps_à_attendre = difftime (expéditions->heure_de_fin, time (NULL));
      if (temps_à_attendre > 0)
	temps_attente_restant = sleep ((unsigned int) temps_à_attendre);
      else
	temps_attente_restant = 0;

      if (temps_attente_restant == 0)
        exécuter_expédition ();
    }
}

void
remplir_liste_expéditions ()
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *document;
  bson_t *requête;

  mongoc_init ();

  client = mongoc_client_new ("mongodb://localhost:27017");
  collection = mongoc_client_get_collection (client, "nassau", "expéditions");
  requête = bson_new ();

  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  while (mongoc_cursor_next (curseur, &document))
    {
      mettre_document_dans_liste_expéditions (document);
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
  mongoc_cleanup ();
}

void
mettre_document_dans_liste_expéditions (const bson_t *document)
{
  struct expédition *expédition;
  expédition = (struct expédition*) malloc (sizeof (struct expédition));
  expédition->suivant = NULL;
  expédition->ressources.travailleurs = 0;

  bson_iter_t iter, iter2;

  /* On enregistre l’identifiant. */
  if (bson_iter_init (&iter, document)
      && bson_iter_find (&iter, "_id")
      && BSON_ITER_HOLDS_OID (&iter))
    {
      bson_oid_copy (bson_iter_oid (&iter), &expédition->identifiant);
    }

  /* On enregistre le type de mission. */
  if (bson_iter_init (&iter, document)
      && bson_iter_find (&iter, "mission")
      && BSON_ITER_HOLDS_UTF8 (&iter))
    {
      expédition->type = strdup (bson_iter_utf8 (&iter, NULL));
    }

  /* On enregistre le nom du commanditaire. */
  if (bson_iter_init (&iter, document)
      && bson_iter_find (&iter, "commanditaire")
      && BSON_ITER_HOLDS_UTF8 (&iter))
    {
      expédition->commanditaire = strdup (bson_iter_utf8 (&iter, NULL));
    }

  /* On enregistre l’heure de fin. */
  if (bson_iter_init (&iter, document)
      && bson_iter_find (&iter, "fin")
      && BSON_ITER_HOLDS_DATE_TIME (&iter))
    {
      expédition->heure_de_fin = (time_t) (bson_iter_date_time (&iter) / 1000);
    }

  char *informations2[] = {
    "île de départ.océan",
    "île de départ.secteur",
    "île de départ.emplacement",

    "île d’arrivée.océan",
    "île d’arrivée.secteur",
    "île d’arrivée.emplacement",

    "navires.bateau espion",
    "navires.bateau transport léger",
    "navires.bateau transport lourd",
    "navires.canonnière",
    "navires.frégate",
    "navires.corvette",
    "navires.brick",
    "navires.galion",
    "navires.navire de ligne",

    "ressources.bois",
    "ressources.métal",
    "ressources.poudre à canon",
  };

  int *informations2_struct[] = {
    &(expédition->île_départ.océan),
    &(expédition->île_départ.secteur),
    &(expédition->île_départ.emplacement),
    &(expédition->île_arrivée.océan),
    &(expédition->île_arrivée.secteur),
    &(expédition->île_arrivée.emplacement),
    &(expédition->flotte.bateau_espion),
    &(expédition->flotte.bateau_transport_léger),
    &(expédition->flotte.bateau_transport_lourd),
    &(expédition->flotte.canonnière),
    &(expédition->flotte.frégate),
    &(expédition->flotte.corvette),
    &(expédition->flotte.brick),
    &(expédition->flotte.galion),
    &(expédition->flotte.navire_de_ligne),
    &(expédition->ressources.bois),
    &(expédition->ressources.métal),
    &(expédition->ressources.poudre_à_canon)
  };

  for (int i = 0; i < sizeof (informations2) / sizeof (char**); ++i)
    {
      if (bson_iter_init (&iter, document)
      	  && bson_iter_find_descendant (&iter, informations2[i], &iter2)
      	  && BSON_ITER_HOLDS_INT32 (&iter2))
      	{
      	  *(informations2_struct[i]) = bson_iter_int32 (&iter2);
      	}
    }

  /* Maintenant que la structure est remplie, il faut l’ajouter à la liste. */
  struct expédition *expédition_courante = expéditions;
  struct expédition *expédition_précédente = NULL;

  if (expédition_courante == NULL)
    {
      expéditions = expédition;
      return;
    }

  do
    {
      if (expédition->heure_de_fin
	  < expédition_courante->heure_de_fin)
	{
	  expédition->suivant = expédition_courante;

	  if (expédition_précédente != NULL)
	    expédition_précédente->suivant = expédition;
	  else
	    expéditions = expédition;

	  break;
	}
      else if (expédition_courante->suivant == NULL)
	{
	  expédition->suivant = NULL;
	  expédition_courante->suivant = expédition;
	  break;
	}

      expédition_précédente = expédition_courante;
      expédition_courante = expédition_courante->suivant;
    } while (expédition_courante != NULL);

}

void
exécuter_expédition ()
{
  /* On détermine le type d’expédition */
  /* et on appelle la fonction s’occupant de ce type d’expédition. */
  if (!strcmp (expéditions->type, "attaque"))
    exécuter_attaque ();
  else if (!strcmp (expéditions->type, "transport"))
    exécuter_transport ();
  else if (!strcmp (expéditions->type, "colonisation"))
    exécuter_colonisation ();
  else if (!strcmp (expéditions->type, "espionnage"))
    exécuter_espionnage ();
  else if (!strcmp (expéditions->type, "stationnement"))
    exécuter_stationnement ();
  else if (!strcmp (expéditions->type, "retour"))
    exécuter_retour ();
}

void
exécuter_attaque ()
{
  struct navires flotte_attaquante = expéditions->flotte;

  struct navires flotte_défenseur = {0};
  if (!récupérer_nombre_navires (expéditions->île_arrivée,
				 &flotte_défenseur))
    return;

  struct défenses défenses = {0};
  if (!récupérer_nombre_défenses (expéditions->île_arrivée,
				  &défenses))
    return;

  struct objet_militaire *attaquant = NULL;
  struct objet_militaire *défenseur = NULL;

  /* Il faut remplir les listes d’objets militaires. */
  remplir_objets_militaires (&attaquant, &flotte_attaquante, NULL);
  remplir_objets_militaires (&défenseur, &flotte_défenseur, &défenses);

  unsigned nombre_attaquants = nombre_total_navires (flotte_attaquante);
  unsigned nombre_défenseurs = défenses.murs + défenses.canons
    + défenses.mortiers + défenses.forts + défenses.mines;
  nombre_défenseurs += nombre_total_navires (flotte_défenseur);

  int tour_attaquant = 1;
  int num_tour = 0;
  int feu_rapide = 0;
  unsigned numéro_objet_attaquant = 0;
  unsigned combat_est_nul = 0;

  /* Tant qu’il y a des objets militaires dans les deux camps. */
  while (attaquant != NULL && défenseur != NULL)
    {
      exécuter_tour (&tour_attaquant, &nombre_attaquants, &nombre_défenseurs,
                     &numéro_objet_attaquant, &feu_rapide, &attaquant,
                     &défenseur, &num_tour);

      if (num_tour == 10 && attaquant != NULL && défenseur != NULL)
	{
	  combat_est_nul = 1;
	  break;
	}
    }

  /* Les rapports de combat. */
  if (combat_est_nul)
    combat_nul (attaquant, défenseur, flotte_défenseur, défenses);
  else if (nombre_attaquants == 0)
    victoire_défenseur (défenseur, flotte_défenseur, défenses);
  else if (nombre_défenseurs == 0)
    victoire_attaquant (attaquant, défenseur, flotte_défenseur, défenses);
}

void
exécuter_tour (int *tour_attaquant, unsigned *nombre_attaquants,
               unsigned *nombre_défenseurs, unsigned *numéro_objet_attaquant,
               int *feu_rapide, struct objet_militaire **attaquant,
               struct objet_militaire **défenseur,
               int *num_tour)
{
  struct objet_militaire *objet_attaquant = NULL;
  struct objet_militaire *objet_attaqué = NULL;

  /* On choisit les objets faisant le combat. */
  choisir_objet_pour_combat (*tour_attaquant, *feu_rapide,
                             *numéro_objet_attaquant,
                             &objet_attaquant, &objet_attaqué,
                             *attaquant, *défenseur,
                             *nombre_attaquants, *nombre_défenseurs);

  /* On exécute le combat. */
  exécuter_combat (objet_attaquant, objet_attaqué);

  /* L’objet attaquant peut-il attaquer à nouveau ? */
  *feu_rapide = y_a_t_il_feu_rapide (objet_attaquant, objet_attaqué);

  /* Libération de l’objet militaire s’il est détruit. */
  libération_objet_militaire_détruit (*tour_attaquant, objet_attaqué,
                                      attaquant, défenseur, nombre_attaquants,
                                      nombre_défenseurs);

  /* Si l’objet ne fait pas à nouveau feu, on passe au suivant. */
  if (!(*feu_rapide))
    ++(*numéro_objet_attaquant);

  if (*tour_attaquant && *numéro_objet_attaquant >= *nombre_attaquants)
    {
      *tour_attaquant = 0;
      *numéro_objet_attaquant = 0;
    }
  else if (!(*tour_attaquant) && *numéro_objet_attaquant >= *nombre_défenseurs)
    {
      *tour_attaquant = 1;
      *numéro_objet_attaquant = 0;
      ++(*num_tour);
    }
}

static void
choisir_objet_pour_combat (int tour_attaquant, int feu_rapide,
                           int numéro_objet_attaquant,
                           struct objet_militaire **objet_attaquant,
                           struct objet_militaire **objet_attaqué,
                           struct objet_militaire *attaquant,
                           struct objet_militaire *défenseur,
                           unsigned nombre_attaquants,
                           unsigned nombre_défenseurs)
{
  srand (time (NULL));
  unsigned numéro_objet_attaqué = 0;

  if (tour_attaquant)		/* Si c’est au tour de l’attaquant. */
    {
      if (!feu_rapide)	/* Si l’objet actuel n’attaque pas de nouveau. */
        {
          *objet_attaquant = attaquant;
          for (unsigned i = 0; i < numéro_objet_attaquant; ++i)
            *objet_attaquant = (*objet_attaquant)->suivant;
        }

      numéro_objet_attaqué = rand () % nombre_défenseurs;
      *objet_attaqué = défenseur;
      for (unsigned i = 0; i < numéro_objet_attaqué; ++i)
        {
          *objet_attaqué = (*objet_attaqué)->suivant;
        }
    }
  else		/* Si c’est au tour du défenseur d’attaquer. */
    {
      if (!feu_rapide)	/* Si l’objet militaire actuel n’attaque pas de nouveau. */
        {
          *objet_attaquant = défenseur;
          for (unsigned i = 0; i < numéro_objet_attaquant; ++i)
            *objet_attaquant = (*objet_attaquant)->suivant;
        }

      numéro_objet_attaqué = rand () % nombre_attaquants;
      *objet_attaqué = attaquant;
      for (unsigned i = 0; i < numéro_objet_attaqué; ++i)
        *objet_attaqué = (*objet_attaqué)->suivant;
    }
}

static int
y_a_t_il_feu_rapide (struct objet_militaire *objet_attaquant,
                     struct objet_militaire *objet_attaqué)
{
  int probabilité_tir_à_nouveau =
    obtenir_probabilité_tir_à_nouveau (objet_attaqué->type,
                                       objet_attaquant->feu_rapide_navires,
                                       objet_attaquant->feu_rapide_défenses);

  return rand () % 101 < probabilité_tir_à_nouveau;
}

static void
exécuter_combat (struct objet_militaire *objet_attaquant,
                 struct objet_militaire *objet_attaqué)
{
  if (objet_attaquant->attaque < objet_attaqué->structure)
    objet_attaqué->structure -= objet_attaquant->attaque;
  else
    objet_attaqué->structure = 0;
}

static void
libération_objet_militaire_détruit (int tour_attaquant,
                                    struct objet_militaire *objet_attaqué,
                                    struct objet_militaire **attaquant,
                                    struct objet_militaire **défenseur,
                                    unsigned *nombre_attaquants,
                                    unsigned *nombre_défenseurs)
{
  /* Si l’objet attaqué n’est pas détruit. */
  if (objet_attaqué->structure > 0)
    return;

  /* Si c’est le premier élément de la liste. */
  if (objet_attaqué->précédent == NULL)
    {
      if (!tour_attaquant)
        *attaquant = (*attaquant)->suivant;
      else
        *défenseur = (*défenseur)->suivant;

      if (objet_attaqué->suivant != NULL)
        objet_attaqué->suivant->précédent = NULL;

      free (objet_attaqué);
    }

  /* Si c’est le dernier élément de la liste. */
  else if (objet_attaqué->suivant == NULL)
    {
      objet_attaqué->précédent->suivant = NULL;
      free (objet_attaqué);
    }

  /* S’il est entre deux éléments. */
  else
    {
      objet_attaqué->précédent->suivant = objet_attaqué->suivant;
      objet_attaqué->suivant->précédent = objet_attaqué->précédent;
      free (objet_attaqué);
    }

  if (tour_attaquant)
    --(*nombre_défenseurs);
  else
    --(*nombre_attaquants);
}

void
exécuter_transport ()
{
  struct ressources ressources_transportées = expéditions->ressources;
  struct coordonnées_île île_arrivée = expéditions->île_arrivée;

  modifier_ressources (île_arrivée, ressources_transportées);

  /* Inscription du rapport de transport dans les messages */
  /* de la personne ayant envoyée les ressources. */
  inscrire_rapport_transport_effectué (expéditions->île_départ, île_arrivée,
				       ressources_transportées);

  /* Inscription du rapport de transport dans les messages */
  /* de la personne ayant reçu les ressources. */
  inscrire_rapport_transport_reçu (expéditions->île_départ, île_arrivée,
				   ressources_transportées);

  /* On inscrit le retour dans la base de données. */
  bson_t *document;
  int vitesse = obtenir_vitesse_la_plus_longue (expéditions->flotte);
  time_t heure_fin = récupérer_temps_expédition (expéditions->île_départ,
						 île_arrivée,
						 vitesse);
  struct ressources ressources = {0, 0, 0, 0};
  inscrire_expédition ("retour", expéditions->commanditaire, île_arrivée,
		       expéditions->île_départ, ressources,
		       heure_fin,
		       expéditions->flotte, &document);

  supprimer_expédition_courante ();
  mettre_document_dans_liste_expéditions (document);
  bson_destroy (document);
}

void
exécuter_colonisation ()
{
  if (!ajouter_île (expéditions->île_arrivée, expéditions->commanditaire,
		    expéditions->ressources, expéditions->flotte))
    {
      /* Envoyer un rapport de colonisation infructueuse. */
      inscrire_rapport_colonisation_infructueuse (expéditions->île_arrivée,
                                                  expéditions->commanditaire);
      /* Lancer le retour de la flotte. */
      /* On inscrit le retour dans la base de données. */
      bson_t *document;
      int vitesse = obtenir_vitesse_la_plus_longue (expéditions->flotte);
      time_t heure_fin = récupérer_temps_expédition (expéditions->île_départ,
						     expéditions->île_arrivée,
						     vitesse);
      inscrire_expédition ("retour", expéditions->commanditaire,
                           expéditions->île_arrivée, expéditions->île_départ,
                           expéditions->ressources,
			   heure_fin,
			   expéditions->flotte, &document);

      supprimer_expédition_courante ();
      mettre_document_dans_liste_expéditions (document);
      bson_destroy (document);
      return;
    }

  /* Envoyer un rapport de colonisation fructueuse. */
  inscrire_rapport_colonisation_réussie (expéditions->île_arrivée,
                                         expéditions->commanditaire);
  supprimer_expédition_courante ();
}

void
exécuter_espionnage ()
{
  /* À faire : */
  /* Lancer le retour ici. */

  /* On récupère le nom du propriétaire de l’île cible. */
  char *propriétaire_île_cible = NULL;
  if (!récupérer_propriétaire_île (expéditions->île_arrivée,
				   &propriétaire_île_cible))
    {
      fprintf (stderr, "Erreur lors de l’exécution d’un espionnage, impossible"
	       " de récupérer le propriétaire de l’île cible.\n");
      return;
    }

  /* On récupère les niveaux d’espionnage des joueurs. */
  unsigned int niveau_espionnage_cible;
  unsigned int niveau_espionnage_commanditaire;

  niveau_espionnage_cible = récupérer_niveau_recherche (propriétaire_île_cible,
							"espionnage");
  niveau_espionnage_commanditaire = récupérer_niveau_recherche
    (expéditions->commanditaire,
     "espionnage");

  if (niveau_espionnage_cible == -1 || niveau_espionnage_commanditaire == -1)
    {
      fprintf (stderr, "Erreur lors de l’exécution d’un espionnage, impossible"
	       "de récupérer le niveau d’espionnage des joueurs.\n");
      return;
    }

  /* On calcule la différence de niveau d’espionnage. */
  int différence_niveau_espionnage = niveau_espionnage_commanditaire;
  différence_niveau_espionnage -= niveau_espionnage_cible;

  /* On calcule le nombre de sondes nécessaires par niveau. */
  int sonde_nécessaire_par_niveau[3] = {1, 1, 1};
  if (différence_niveau_espionnage > 0)
    {
      sonde_nécessaire_par_niveau[2] = 4 - pow (2,
						différence_niveau_espionnage);
      if (sonde_nécessaire_par_niveau[2] < 1)
	sonde_nécessaire_par_niveau[2] = 1;
    }
  else
    {
      différence_niveau_espionnage *= -1;
      sonde_nécessaire_par_niveau[0] = 1 + pow (2,
						différence_niveau_espionnage);
      sonde_nécessaire_par_niveau[1] = 2 + pow (2,
						différence_niveau_espionnage);
      sonde_nécessaire_par_niveau[2] = 4 + pow (2,
						différence_niveau_espionnage);
    }

  /* On détermine le niveau de détail de l’espionnage. */
  int niveau_de_détail = 0;
  int nombre_bateaux_espions = expéditions->flotte.bateau_espion;
  for (unsigned int i = 2; i != 0; --i)
    {
      if (nombre_bateaux_espions >= sonde_nécessaire_par_niveau[i])
	{
	  niveau_de_détail = i+1;
	  break;
	}
    }

  /* Initialisation des données du rapport. */
  struct ressources ressources = {-1, -1, -1, -1};
  struct navires flotte = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
  struct défenses défenses = {-1, -1, -1, -1};
  struct niveau_bâtiments bâtiments = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
  struct niveau_recherches recherches = {-1, -1, -1, -1, -1};

  if (niveau_de_détail > 0)
    {
      récupérer_ressources_île (&ressources, expéditions->île_arrivée);
    }

  if (niveau_de_détail > 1)
    {
      récupérer_nombre_navires (expéditions->île_arrivée, &flotte);
      récupérer_nombre_défenses (expéditions->île_arrivée, &défenses);
    }

  if (niveau_de_détail == 3)
    {
      récupérer_niveau_bâtiments (expéditions->île_arrivée, &bâtiments);
      récupérer_niveau_recherches (propriétaire_île_cible, &recherches);
    }

  /* Inscrire le compte rendu d’espionnage dans les messages. */
  if (!inscrire_rapport_espionnage (expéditions->commanditaire,
				    propriétaire_île_cible,
				    expéditions->île_arrivée,
				    ressources, flotte, défenses, bâtiments,
				    recherches))
    {
      fprintf (stderr, "Erreur lors de l’inscription du rapport d’espionnage "
	       "dans la liste des messages.\n");
    }
  free (propriétaire_île_cible);
  supprimer_expédition_courante ();
}

void
exécuter_stationnement ()
{
  struct ressources ressources = expéditions->ressources;

  if (!vérifier_existence_île (expéditions->île_arrivée))
    {
      /* On inscrit le retour dans la base de données. */
      bson_t *document;
      int vitesse = obtenir_vitesse_la_plus_longue (expéditions->flotte);
      time_t heure_fin = récupérer_temps_expédition (expéditions->île_départ,
                                                     expéditions->île_arrivée,
                                                     vitesse);
      inscrire_expédition ("retour", expéditions->commanditaire,
                           expéditions->île_arrivée,
                           expéditions->île_départ, ressources,
                           heure_fin,
                           expéditions->flotte, &document);

      supprimer_expédition_courante ();
      mettre_document_dans_liste_expéditions (document);
      bson_destroy (document);
      return;
    }

  struct coordonnées_île île_départ = expéditions->île_départ;
  struct coordonnées_île île_arrivée = expéditions->île_arrivée;

  struct navires flotte = expéditions->flotte;
  modifier_nombre_navires (île_arrivée, flotte);
  modifier_ressources (île_arrivée, ressources);

  /* Inscription du rapport de transport dans les messages */
  /* de la personne ayant envoyée les ressources. */
  inscrire_rapport_stationnement_effectué (île_départ, île_arrivée,
                                           ressources, flotte);

  supprimer_expédition_courante ();
}

void
exécuter_retour ()
{
  /* Récupérer le nombre de ressources. */
  struct ressources ressources_expédition = expéditions->ressources;

  /* Récupérer le nombre de navires. */
  struct navires flotte = expéditions->flotte;

  /* Récupérer l’île d’arrivée. */
  struct coordonnées_île île_retour = expéditions->île_arrivée;

  /* Ajouter sur l’île d’arrivée les navires de l’expédition. */
  modifier_nombre_navires (île_retour, flotte);

  /* Ajouter sur l’île d’arrivée les ressources de l’expédition. */
  modifier_ressources (île_retour, ressources_expédition);


  /* Ajouter un message de retour effectué dans la base de donnée. */
  inscrire_retour (expéditions->commanditaire, île_retour,
		   expéditions->île_départ, expéditions->ressources);

  supprimer_expédition_courante ();
}

void
libérer_expédition (struct expédition *expédition)
{
  free (expédition->type);
  free (expédition->commanditaire);
  free (expédition);
}

void
lire_données_tube_expéditions ()
{
  char *tampon = NULL;
  size_t taille_tampon = 0;

  FILE *tube = fdopen (tube_expéditions, "r");

  int lecture_terminée = 0;
  while (!lecture_terminée)
    {
      taille_tampon += 4096;
      tampon = (char*) realloc (tampon, sizeof (char) * taille_tampon);

      for (unsigned long i = taille_tampon - 4096; i < taille_tampon; ++i)
	{
	  tampon[i] = fgetc (tube);
	  if (tampon[i] == '\0')
	    {
	      lecture_terminée = 1;
	      tampon = (char*) realloc (tampon, i+1);
	      break;
	    }
	}
    }

  bson_t *document = bson_new_from_json ((const uint8_t*) tampon, -1, NULL);
  free (tampon);

  if (!document)
    {
      fprintf (stderr, "Erreur lors de la lecture du "
	       "document reçu dans le tube des expéditions");
      return;
    }

  mettre_document_dans_liste_expéditions (document);
  bson_destroy (document);
}

void
envoyer_document_dans_tube (const bson_t document, int tube)
{
  size_t longueur_chaîne_document;
  char *chaîne_document = bson_as_json (&document, &longueur_chaîne_document);

  FILE *fichier = fdopen (tube, "a");

  fprintf (fichier, "%s", chaîne_document);
  fputc (0, fichier);
  bson_free (chaîne_document);
  fflush (fichier);
}

time_t
récupérer_temps_expédition (const struct coordonnées_île île_départ,
			    const struct coordonnées_île île_arrivée,
			    int vitesse)
{
  time_t temps = time (NULL);
  int éloignement_océan, éloignement_secteur, éloignement_emplacement;

  if (île_départ.océan == île_arrivée.océan)
    éloignement_océan = 0;
  else
    {
      éloignement_océan = île_départ.océan - île_arrivée.océan;
      if (éloignement_océan < 0)
	éloignement_océan *= -1;
    }

  if (île_départ.secteur == île_arrivée.secteur)
    éloignement_secteur = 0;
  else
    {
      éloignement_secteur = île_départ.secteur - île_arrivée.secteur;
      if (éloignement_secteur < 0)
	éloignement_secteur *= -1;
    }

  if (éloignement_océan == 0 && éloignement_secteur == 0)
    {
      éloignement_emplacement = île_départ.emplacement - île_arrivée.emplacement;
      if (éloignement_emplacement < 0)
	éloignement_emplacement *= -1;
    }
  else
    {
      éloignement_emplacement = île_arrivée.emplacement;
    }

  /* Deux heures par océan. */
  time_t secondes = éloignement_océan * (int) (3600 * 2);
  /* Cinq minutes par secteur. */
  secondes += éloignement_secteur * (int) (300);
  /* Soixante secondes par emplacement. */
  secondes += éloignement_emplacement * (int) (60);
  double diviseur = (double)vitesse / 1000.0;
  secondes /= diviseur;

  temps += secondes;

  return temps;
}

void
afficher_expéditions ()
{
  struct expédition *expédition_courante = expéditions;
  char date[32];

  while (expédition_courante != NULL)
    {
      printf ("Type : %s\n", expédition_courante->type);
      printf ("Commanditaire : %s\n", expédition_courante->commanditaire);

      printf ("Coordonnées de l’île de départ : %d;%d;%d\n",
	      expédition_courante->île_départ.océan,
	      expédition_courante->île_départ.secteur,
	      expédition_courante->île_départ.emplacement);
      printf ("Coordonnées de l’île d’arrivée : %d;%d;%d\n",
	      expédition_courante->île_arrivée.océan,
	      expédition_courante->île_arrivée.secteur,
	      expédition_courante->île_arrivée.emplacement);

      strftime (date, 32, "%c", localtime (&(expédition_courante->heure_de_fin)));
      printf ("Heure de fin : %s\n", date);

      expédition_courante = expédition_courante->suivant;
    }
}

void
supprimer_expédition_courante ()
{
  /* Supprimer l’expédition de la base de donnée. */
  supprimer_document (expéditions->identifiant, "expéditions");

  /* Supprimer l’expédition de la liste d’expéditions. */
  struct expédition *expédition_suivante = NULL;
  expédition_suivante = expéditions->suivant;
  libérer_expédition (expéditions);
  expéditions = expédition_suivante;
}

int
remplir_objets_militaires (struct objet_militaire **objets,
			   const struct navires *navires,
			   const struct défenses *défenses)
{
  int nombre_navires = 0;
  if (navires != NULL)
    nombre_navires = nombre_total_navires (*navires);

  int nombre_défenses = 0;
  if (défenses != NULL)
    nombre_défenses = défenses->murs + défenses->canons + défenses->mortiers
      + défenses->forts + défenses->mines;

  int nombre_objets_militaires = nombre_navires + nombre_défenses;
  if (nombre_objets_militaires == 0)
    return 0;

  struct objet_militaire *om[nombre_objets_militaires];
  for (int i = 0; i < nombre_objets_militaires; ++i)
    om[i] = malloc (sizeof (struct objet_militaire));

  int case_actuelle = 0;
  /* S’il y a des navires. */
  if (navires != NULL)
    {
      for (unsigned i = 0; i < navires->bateau_espion; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_BATEAU_ESPION + COÛT_MÉTAL_BATEAU_ESPION;
	  om[case_actuelle]->attaque = ATTAQUE_BATEAU_ESPION;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_BATEAU_ESPION;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->bateau_transport_léger; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_BATEAU_TLÉ + COÛT_MÉTAL_BATEAU_TLÉ;
	  om[case_actuelle]->attaque = ATTAQUE_BATEAU_TLÉ;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_BATEAU_TLÉ;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->bateau_transport_lourd; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_BATEAU_TLO + COÛT_MÉTAL_BATEAU_TLO;
	  om[case_actuelle]->attaque = ATTAQUE_BATEAU_TLO;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_BATEAU_TLO;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->canonnière; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_CANONNIÈRE + COÛT_MÉTAL_CANONNIÈRE;
	  om[case_actuelle]->attaque = ATTAQUE_CANONNIÈRE;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_CANONNIÈRE;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->corvette; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_CORVETTE + COÛT_MÉTAL_CORVETTE;
	  om[case_actuelle]->attaque = ATTAQUE_CORVETTE;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_CORVETTE;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->frégate; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_FRÉGATE + COÛT_MÉTAL_FRÉGATE;
	  om[case_actuelle]->attaque = ATTAQUE_FRÉGATE;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_FRÉGATE;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->brick; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_BRICK + COÛT_MÉTAL_BRICK;
	  om[case_actuelle]->attaque = ATTAQUE_BRICK;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_BRICK;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->galion; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_GALION + COÛT_MÉTAL_GALION;
	  om[case_actuelle]->attaque = ATTAQUE_GALION;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_GALION;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < navires->navire_de_ligne; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_NDL + COÛT_MÉTAL_NDL;
	  om[case_actuelle]->attaque = ATTAQUE_NDL;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = TYPE_NDL;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}
    }

  /* S’il y a des défenses. */
  if (défenses != NULL)
    {
      for (unsigned i = 0; i < défenses->murs; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_MUR + COÛT_MÉTAL_MUR;
	  om[case_actuelle]->attaque = ATTAQUE_MUR;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = NOMBRE_NAVIRES + TYPE_MUR;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < défenses->canons; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_CANON + COÛT_MÉTAL_CANON;
	  om[case_actuelle]->attaque = ATTAQUE_CANON;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = NOMBRE_NAVIRES + TYPE_CANON;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < défenses->mortiers; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_MORTIER + COÛT_MÉTAL_MORTIER;
	  om[case_actuelle]->attaque = ATTAQUE_MORTIER;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = NOMBRE_NAVIRES + TYPE_MORTIER;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < défenses->mines; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_MINE + COÛT_MÉTAL_MINE;
	  om[case_actuelle]->attaque = ATTAQUE_MINE;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = NOMBRE_NAVIRES + TYPE_MINE;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}

      for (unsigned i = 0; i < défenses->forts; ++i)
	{
	  om[case_actuelle]->structure = COÛT_BOIS_FORT + COÛT_MÉTAL_FORT;
	  om[case_actuelle]->attaque = ATTAQUE_FORT;

	  om[case_actuelle]->feu_rapide_navires.bateau_espion = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_léger = 0;
	  om[case_actuelle]->feu_rapide_navires.bateau_transport_lourd = 0;
	  om[case_actuelle]->feu_rapide_navires.canonnière = 0;
	  om[case_actuelle]->feu_rapide_navires.corvette = 0;
	  om[case_actuelle]->feu_rapide_navires.frégate = 0;
	  om[case_actuelle]->feu_rapide_navires.brick = 0;
	  om[case_actuelle]->feu_rapide_navires.galion = 0;
	  om[case_actuelle]->feu_rapide_navires.navire_de_ligne = 0;

	  om[case_actuelle]->feu_rapide_défenses.murs = 0;
	  om[case_actuelle]->feu_rapide_défenses.mines = 0;
	  om[case_actuelle]->feu_rapide_défenses.canons = 0;
          om[case_actuelle]->feu_rapide_défenses.mortiers = 0;
	  om[case_actuelle]->feu_rapide_défenses.forts = 0;

	  om[case_actuelle]->type = NOMBRE_NAVIRES + TYPE_FORT;
	  om[case_actuelle]->précédent = NULL;
	  om[case_actuelle]->suivant = NULL;
	  ++case_actuelle;
	}
    }

  /* On lie dans le sens actuel -> suivant. */
  for (unsigned i = 0; i < nombre_objets_militaires-1; ++i)
    om[i]->suivant = om[i+1];

  /* On lie dans le sens actuel <- suivant. */
  for (unsigned i = 1; i < nombre_objets_militaires; ++i)
    om[i]->précédent = om[i-1];

  *objets = om[0];
  return 1;
}

void
libérer_objets_militaires (struct objet_militaire *objets)
{
  struct objet_militaire *suivant = NULL;

  while (objets != NULL)
    {
      suivant = objets->suivant;
      free (objets);
      objets = suivant;
    }
}


int
obtenir_probabilité_tir_à_nouveau (const unsigned type_objet,
				   const struct navires feu_rapide_navires,
				   const struct défenses feu_rapide_défenses)
{
  float probabilité_tir_à_nouveau = 0.0;

  switch (type_objet)
    {
    case TYPE_BATEAU_ESPION:
      if (feu_rapide_navires.bateau_espion > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.bateau_espion);
      break;

    case TYPE_BATEAU_TLÉ:
      if (feu_rapide_navires.bateau_transport_léger > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.bateau_transport_léger);
      break;

    case TYPE_BATEAU_TLO:
      if (feu_rapide_navires.bateau_transport_lourd > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.bateau_transport_lourd);
      break;

    case TYPE_CANONNIÈRE:
      if (feu_rapide_navires.canonnière > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.canonnière);
      break;

    case TYPE_CORVETTE:
      if (feu_rapide_navires.corvette > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.corvette);
      break;

    case TYPE_FRÉGATE:
      if (feu_rapide_navires.frégate > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.frégate);
      break;

    case TYPE_BRICK:
      if (feu_rapide_navires.brick > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.brick);
      break;

    case TYPE_GALION:
      if (feu_rapide_navires.galion > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.galion);
      break;

    case TYPE_NDL:
      if (feu_rapide_navires.navire_de_ligne > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_navires.navire_de_ligne);
      break;

    case TYPE_MUR + NOMBRE_NAVIRES:
      if (feu_rapide_défenses.murs > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_défenses.murs);
      break;

    case TYPE_CANON + NOMBRE_NAVIRES:
      if (feu_rapide_défenses.canons > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_défenses.canons);
      break;

    case TYPE_MORTIER + NOMBRE_NAVIRES:
      if (feu_rapide_défenses.mortiers > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_défenses.mortiers);
      break;

    case TYPE_MINE + NOMBRE_NAVIRES:
      if (feu_rapide_défenses.mines > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_défenses.mines);
      break;

    case TYPE_FORT + NOMBRE_NAVIRES:
      if (feu_rapide_défenses.forts > 0)
	probabilité_tir_à_nouveau = 1.0
	  -(1.0/(float)feu_rapide_défenses.forts);
      break;
    }

  return (int)100.0 * probabilité_tir_à_nouveau;
}

void
convertir_objets_militaires (struct objet_militaire *objets_militaires,
			     struct navires *flotte,
			     struct défenses *défenses)
{
  struct objet_militaire *objet_actuel = objets_militaires;
  if (flotte != NULL)
    {
      flotte->bateau_espion = 0;
      flotte->bateau_transport_léger = 0;
      flotte->bateau_transport_lourd = 0;
      flotte->canonnière = 0;
      flotte->corvette = 0;
      flotte->frégate = 0;
      flotte->brick = 0;
      flotte->galion = 0;
      flotte->navire_de_ligne = 0;
    }

  if (défenses != NULL)
    {
      défenses->murs = 0;
      défenses->mines = 0;
      défenses->canons = 0;
      défenses->mortiers = 0;
      défenses->forts = 0;
    }

  while (objet_actuel != NULL)
    {
      if (flotte != NULL)
	{
	  switch (objet_actuel->type)
	    {
	    case TYPE_BATEAU_ESPION:
	      ++(flotte->bateau_espion);
	      break;

	    case TYPE_BATEAU_TLÉ:
	      ++(flotte->bateau_transport_léger);
	      break;

	    case TYPE_BATEAU_TLO:
	      ++(flotte->bateau_transport_lourd);
	      break;

	    case TYPE_CANONNIÈRE:
	      ++(flotte->canonnière);
	      break;

	    case TYPE_CORVETTE:
	      ++(flotte->corvette);
	      break;

	    case TYPE_FRÉGATE:
	      ++(flotte->frégate);
	      break;

	    case TYPE_BRICK:
	      ++(flotte->brick);
	      break;

	    case TYPE_GALION:
	      ++(flotte->galion);
	      break;

	    case TYPE_NDL:
	      ++(flotte->navire_de_ligne);
	      break;
	    }
	}

      if (défenses != NULL)
	{
	  switch (objet_actuel->type)
	    {
	    case TYPE_MUR + NOMBRE_NAVIRES:
	      ++(défenses->murs);
	      break;

	    case TYPE_MINE + NOMBRE_NAVIRES:
	      ++(défenses->mines);
	      break;

	    case TYPE_CANON + NOMBRE_NAVIRES:
	      ++(défenses->canons);
	      break;

	    case TYPE_MORTIER + NOMBRE_NAVIRES:
	      ++(défenses->mortiers);
	      break;

	    case TYPE_FORT + NOMBRE_NAVIRES:
	      ++(défenses->forts);
	      break;
	    }
	}

      objet_actuel = objet_actuel->suivant;
    }
}

void
combat_nul (struct objet_militaire *attaquant_après_combat,
	    struct objet_militaire *défenseur_après_combat,
	    const struct navires flotte_défenseur_avant_combat,
	    const struct défenses défenses_avant_combat)
{
  struct navires flotte_attaquant_après_combat, flotte_défenseur_après_combat;
  struct défenses défenses_après_combat;

  /* Recalculer la flotte de l’attaquant. */
  convertir_objets_militaires (attaquant_après_combat,
			       &flotte_attaquant_après_combat, NULL);

  /* Renvoyer la flotte de l’attaquant. */
  struct ressources ressources = {.bois = 0, .métal = 0,
				  .poudre_à_canon = 0, .travailleurs = 0};

  int vitesse = obtenir_vitesse_la_plus_longue (expéditions->flotte);
  time_t heure_fin = récupérer_temps_expédition (expéditions->île_arrivée,
						 expéditions->île_départ, vitesse);
  bson_t *document;
  inscrire_expédition ("retour", expéditions->commanditaire, expéditions->île_arrivée,
		       expéditions->île_départ, ressources,
		       heure_fin,
		       flotte_attaquant_après_combat, &document);

  struct navires flotte_attaquant_avant_combat = expéditions->flotte;
  char *nom_attaquant = strdup (expéditions->commanditaire);
  struct coordonnées_île île_attaquant = expéditions->île_départ;
  struct coordonnées_île île_défenseur = expéditions->île_arrivée;
  char *nom_défenseur;
  récupérer_propriétaire_île (île_défenseur, &nom_défenseur);

  supprimer_expédition_courante ();
  mettre_document_dans_liste_expéditions (document);
  bson_destroy (document);

  /* Prévenir l’attaquant du combat nul et de ce qu’il a perdu. */
  struct navires navires_attaquants_perdus = flotte_attaquant_avant_combat;
  soustraire_structures_navires (&navires_attaquants_perdus, flotte_attaquant_après_combat);

  /* Recalculer la flotte du défenseur. */
  /* Recalculer les défenses. */
  convertir_objets_militaires (défenseur_après_combat,
			       &flotte_défenseur_après_combat,
			       &défenses_après_combat);


  struct ressources ressources_récupérées = {.bois = 0, .métal = 0,
					     .poudre_à_canon = 0,
					     .travailleurs = 0};

  struct navires navires_défenseurs_perdus = flotte_défenseur_avant_combat;
  soustraire_structures_navires (&navires_défenseurs_perdus, flotte_défenseur_après_combat);

  struct défenses défenses_perdues = défenses_avant_combat;
  soustraire_structures_défenses (&défenses_perdues, défenses_après_combat);

  inscrire_rapport_attaque (nom_attaquant, nom_défenseur, île_défenseur,
			    "combat nul", ressources_récupérées,
			    flotte_attaquant_après_combat,
			    navires_attaquants_perdus,
			    navires_défenseurs_perdus, défenses_perdues);

  /* Modifier en conséquence la base de données. */
  soustraire_navires (île_défenseur, navires_défenseurs_perdus);
  soustraire_défenses (île_défenseur, défenses_perdues);

  /* Prévenir le défenseur du combat nul et de ce qu’il a perdu. */
  inscrire_rapport_défense (nom_défenseur, nom_attaquant, île_attaquant,
			    île_défenseur, "combat nul",
			    flotte_attaquant_avant_combat,
			    navires_attaquants_perdus, ressources_récupérées,
			    navires_défenseurs_perdus, défenses_perdues);

  free (nom_attaquant);
  free (nom_défenseur);
}

void
victoire_défenseur (struct objet_militaire *défenseur_après_combat,
		    const struct navires flotte_défenseur_avant_combat,
		    const struct défenses défenses_avant_combat)
{
  struct navires flotte_attaquante_avant_combat = expéditions->flotte;
  char *nom_attaquant = strdup (expéditions->commanditaire);
  struct coordonnées_île île_attaquant = expéditions->île_départ;
  struct coordonnées_île île_défenseur = expéditions->île_arrivée;
  char *nom_défenseur;
  récupérer_propriétaire_île (île_défenseur, &nom_défenseur);

  /* On supprime l’expédition. */
  supprimer_expédition_courante ();

  /* Recalculer la flotte du défenseur. */
  /* Recalculer les défenses. */
  struct navires flotte_défenseur_après_combat;
  struct défenses défenses_après_combat;

  convertir_objets_militaires (défenseur_après_combat,
			       &flotte_défenseur_après_combat,
			       &défenses_après_combat);

  struct ressources ressources_récupérées =
    {
      .bois = 0,
      .métal = 0,
      .poudre_à_canon = 0,
      .travailleurs = 0
    };

  struct navires navires_défenseurs_perdus = flotte_défenseur_avant_combat;

  soustraire_structures_navires (&navires_défenseurs_perdus, flotte_défenseur_après_combat);

  struct défenses défenses_perdues = défenses_avant_combat;
  soustraire_structures_défenses (&défenses_perdues, défenses_après_combat);

  struct navires flotte_attaquante_après_combat = {0};

  /* Prévenir l’attaquant de sa défaite. */
  inscrire_rapport_attaque (nom_attaquant, nom_défenseur, île_défenseur,
			    "défaite", ressources_récupérées,
			    flotte_attaquante_après_combat,
			    flotte_attaquante_avant_combat,
			    navires_défenseurs_perdus, défenses_perdues);

  /* Modifier en conséquence la base de données. */
  soustraire_navires (île_défenseur, navires_défenseurs_perdus);
  soustraire_défenses (île_défenseur, défenses_perdues);

  /* Prévenir le défenseur de sa victoire et de ce qu’il a perdu. */
  inscrire_rapport_défense (nom_défenseur, nom_attaquant, île_attaquant,
			    île_défenseur, "victoire",
			    flotte_attaquante_avant_combat,
			    flotte_attaquante_avant_combat, ressources_récupérées,
			    navires_défenseurs_perdus, défenses_perdues);

  free (nom_attaquant);
  free (nom_défenseur);
}

void
victoire_attaquant (struct objet_militaire *attaquant_après_combat,
		    struct objet_militaire *défenseur_après_combat,
		    const struct navires flotte_défenseur_avant_combat,
		    const struct défenses défenses_avant_combat)
{
  /* Recalculer la flotte de l’attaquant. */
  struct navires flotte_attaquante_après_combat;
  convertir_objets_militaires (attaquant_après_combat,
			       &flotte_attaquante_après_combat, NULL);

  /* Calculer sa capacité totale de transport. */
  long capacité_transport =
    obtenir_capacité_flotte (flotte_attaquante_après_combat);

  /* Récupérer le nombre de ressources qu’il y a sur l’île. */
  struct ressources ressources_île;
  récupérer_ressources_île (&ressources_île, expéditions->île_arrivée);

  /* On récupère le nombre de ressources à prendre. */
  struct ressources ressources_prises =
    calculer_montant_ressources_à_prendre (capacité_transport, ressources_île);

  /* Lancer l’expédition de retour. */
  int vitesse = obtenir_vitesse_la_plus_longue (expéditions->flotte);
  time_t heure_fin = récupérer_temps_expédition (expéditions->île_arrivée,
						 expéditions->île_départ,
						 vitesse);
  bson_t *document;
  inscrire_expédition ("retour", expéditions->commanditaire,
		       expéditions->île_arrivée,
		       expéditions->île_départ, ressources_prises,
		       heure_fin,
		       flotte_attaquante_après_combat, &document);

  struct navires flotte_attaquante_avant_combat = expéditions->flotte;
  char *nom_attaquant = strdup (expéditions->commanditaire);
  struct coordonnées_île île_attaquant = expéditions->île_départ;
  struct coordonnées_île île_défenseur = expéditions->île_arrivée;
  char *nom_défenseur;
  récupérer_propriétaire_île (île_défenseur, &nom_défenseur);

  supprimer_expédition_courante ();
  mettre_document_dans_liste_expéditions (document);
  bson_destroy (document);

  /* Calcul des pertes de l’attaquant. */
  struct navires navires_attaquants_perdus = flotte_attaquante_avant_combat;
  soustraire_structures_navires (&navires_attaquants_perdus, flotte_attaquante_après_combat);

  /* Prévenir l’attaquant de sa victoire. */
  inscrire_rapport_attaque (nom_attaquant, nom_défenseur, île_défenseur,
			    "victoire", ressources_prises,
			    flotte_attaquante_après_combat,
			    navires_attaquants_perdus, flotte_défenseur_avant_combat,
			    défenses_avant_combat);

  /* Soustraire les ressources au défenseur. */
  soustraire_ressources (île_défenseur, ressources_prises);

  /* Supprimer sa flotte et ses défenses. */
  soustraire_navires (île_défenseur, flotte_défenseur_avant_combat);
  soustraire_défenses (île_défenseur, défenses_avant_combat);

  /* Le prévenir de sa défaite. */
  inscrire_rapport_défense (nom_défenseur, nom_attaquant, île_attaquant,
			    île_défenseur, "défaite",
			    flotte_attaquante_avant_combat,
			    navires_attaquants_perdus, ressources_prises,
			    flotte_défenseur_avant_combat,
			    défenses_avant_combat);

  free (nom_attaquant);
  free (nom_défenseur);
}

struct ressources
calculer_montant_ressources_à_prendre (long capacité_flotte,
				       const struct ressources ressources_île)
{
  /* À refaire avec un algorithme récursif. */

  struct ressources ressources_à_prendre = {0};

  /* Le nombre de ressources qui dépasse la capacité */
  /* de la flotte divisée par trois. */
  unsigned short nombre_ressource_dépassant_capacité = 0;

  long tiers_capacité = capacité_flotte / 3;
  if (ressources_île.bois > tiers_capacité)
    ++nombre_ressource_dépassant_capacité;
  if (ressources_île.métal > tiers_capacité)
    ++nombre_ressource_dépassant_capacité;
  if (ressources_île.poudre_à_canon > tiers_capacité)
    ++nombre_ressource_dépassant_capacité;

  long capacité_utilisée = 0;

  switch (nombre_ressource_dépassant_capacité)
    {
    case 0:
      ressources_à_prendre.bois = ressources_île.bois;
      ressources_à_prendre.métal = ressources_île.métal;
      ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
      break;

    case 1:
      if (ressources_île.bois > tiers_capacité)
	{
	  ressources_à_prendre.métal = ressources_île.métal;
	  capacité_utilisée += ressources_île.métal;
	  ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	  capacité_utilisée += ressources_île.poudre_à_canon;

	  if (ressources_île.bois < capacité_flotte - capacité_utilisée)
	    ressources_à_prendre.bois = ressources_île.bois;
	  else
	    ressources_à_prendre.bois = capacité_flotte - capacité_utilisée;
	}
      else if (ressources_île.métal > tiers_capacité)
	{
	  ressources_à_prendre.bois = ressources_île.bois;
	  capacité_utilisée += ressources_île.bois;
	  ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	  capacité_utilisée += ressources_île.poudre_à_canon;

	  if (ressources_île.métal < capacité_flotte - capacité_utilisée)
	    ressources_à_prendre.métal = ressources_île.métal;
	  else
	    ressources_à_prendre.métal = capacité_flotte - capacité_utilisée;
	}
      else
	{
	  ressources_à_prendre.bois = ressources_île.bois;
	  capacité_utilisée += ressources_île.bois;
	  ressources_à_prendre.métal = ressources_île.métal;
	  capacité_utilisée += ressources_île.métal;

	  if (ressources_île.poudre_à_canon < capacité_flotte - capacité_utilisée)
	    ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	  else
	    ressources_à_prendre.poudre_à_canon = capacité_flotte - capacité_utilisée;
	}
      break;

    case 2:
      if (ressources_île.bois < tiers_capacité)
	{
	  ressources_à_prendre.bois = ressources_île.bois;
	  capacité_utilisée += ressources_île.bois;

	  long moitié_capacité = capacité_flotte / 2;
	  if (ressources_île.métal > moitié_capacité
	      && ressources_île.poudre_à_canon > moitié_capacité)
	    {
	      ressources_à_prendre.métal = moitié_capacité;
	      ressources_à_prendre.poudre_à_canon = moitié_capacité;
	    }
	  else if (ressources_île.métal < moitié_capacité
		   && ressources_île.poudre_à_canon > moitié_capacité)
	    {
	      ressources_à_prendre.métal = ressources_île.métal;
	      capacité_utilisée += ressources_île.métal;
	      if (ressources_à_prendre.poudre_à_canon < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	      else
		ressources_à_prendre.poudre_à_canon = capacité_flotte - capacité_utilisée;
	    }
	  else if (ressources_île.métal > moitié_capacité
		   && ressources_île.poudre_à_canon < moitié_capacité)
	    {
	      ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	      capacité_utilisée += ressources_île.poudre_à_canon;
	      if (ressources_à_prendre.métal < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.métal = ressources_île.métal;
	      else
		ressources_à_prendre.métal = capacité_flotte - capacité_utilisée;
	    }
	  else
	    {
	      ressources_à_prendre.métal = ressources_île.métal;
	      ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	    }
	}
      else if (ressources_île.métal < tiers_capacité)
	{
	  ressources_à_prendre.métal = ressources_île.métal;
	  capacité_utilisée += ressources_île.métal;

	  long moitié_capacité = capacité_flotte / 2;
	  if (ressources_île.bois > moitié_capacité
	      && ressources_île.poudre_à_canon > moitié_capacité)
	    {
	      ressources_à_prendre.bois = moitié_capacité;
	      ressources_à_prendre.poudre_à_canon = moitié_capacité;
	    }
	  else if (ressources_île.bois < moitié_capacité
		   && ressources_île.poudre_à_canon > moitié_capacité)
	    {
	      ressources_à_prendre.bois = ressources_île.bois;
	      capacité_utilisée += ressources_île.bois;
	      if (ressources_à_prendre.poudre_à_canon < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	      else
		ressources_à_prendre.poudre_à_canon = capacité_flotte - capacité_utilisée;
	    }
	  else if (ressources_île.bois > moitié_capacité
		   && ressources_île.poudre_à_canon < moitié_capacité)
	    {
	      ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	      capacité_utilisée += ressources_île.poudre_à_canon;
	      if (ressources_à_prendre.bois < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.bois = ressources_île.bois;
	      else
		ressources_à_prendre.bois = capacité_flotte - capacité_utilisée;
	    }
	  else
	    {
	      ressources_à_prendre.bois = ressources_île.bois;
	      ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	    }
	}
      else
	{
	  ressources_à_prendre.poudre_à_canon = ressources_île.poudre_à_canon;
	  capacité_utilisée += ressources_île.poudre_à_canon;

	  long moitié_capacité = capacité_flotte / 2;
	  if (ressources_île.bois > moitié_capacité
	      && ressources_île.métal > moitié_capacité)
	    {
	      ressources_à_prendre.bois = moitié_capacité;
	      ressources_à_prendre.métal = moitié_capacité;
	    }
	  else if (ressources_île.bois < moitié_capacité
		   && ressources_île.métal > moitié_capacité)
	    {
	      ressources_à_prendre.bois = ressources_île.bois;
	      capacité_utilisée += ressources_île.bois;
	      if (ressources_à_prendre.métal < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.métal = ressources_île.métal;
	      else
		ressources_à_prendre.métal = capacité_flotte - capacité_utilisée;
	    }
	  else if (ressources_île.bois > moitié_capacité
		   && ressources_île.métal < moitié_capacité)
	    {
	      ressources_à_prendre.métal = ressources_île.métal;
	      capacité_utilisée += ressources_île.métal;
	      if (ressources_à_prendre.bois < capacité_flotte - capacité_utilisée)
		ressources_à_prendre.bois = ressources_île.bois;
	      else
		ressources_à_prendre.bois = capacité_flotte - capacité_utilisée;
	    }
	  else
	    {
	      ressources_à_prendre.bois = ressources_île.bois;
	      ressources_à_prendre.métal = ressources_île.métal;
	    }
	}
      break;

    case 3:
      ressources_à_prendre.bois = tiers_capacité;
      ressources_à_prendre.métal = tiers_capacité;
      ressources_à_prendre.poudre_à_canon = tiers_capacité;
      break;
    }

  return ressources_à_prendre;
}

void
afficher_objets_militaires (struct objet_militaire *objets)
{
  struct objet_militaire *objet_actuel = objets;

  while (objet_actuel != NULL)
    {
      printf ("Actuel : %p\n", objet_actuel);
      printf ("Précédent : %p\n", objet_actuel->précédent);
      printf ("Suivant : %p\n", objet_actuel->suivant);
      printf ("Structure : %u.\n", objet_actuel->structure);
      printf ("Attaque : %u.\n", objet_actuel->attaque);
      printf ("Type : %u.\n", objet_actuel->type);
      objet_actuel = objet_actuel->suivant;
    }
}

int
obtenir_expéditions_générique (const char *clef,
                               const char *valeur,
                               char **expéditions)
{
  unsigned succès = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;

  const bson_t *doc;
  bson_t *requête;
  bson_t *options;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "expéditions");

  if (!strcmp (clef, "cible"))
    requête = BCON_NEW (clef, BCON_UTF8 (valeur),
                        "mission", "{", "$ne", BCON_UTF8 ("retour"), "}");
  else
    requête = BCON_NEW (clef, BCON_UTF8 (valeur));
  options = BCON_NEW ("projection", "{", "_id", BCON_BOOL (0), "}");


  curseur = mongoc_collection_find_with_opts (collection, requête, options, NULL);

  *expéditions = NULL;
  unsigned long taille = 1;
  *expéditions = malloc (taille);
  *expéditions[0] = 0;

  unsigned long taille_chaîne_document_courant;
  char *chaîne_document_courant;

  while (mongoc_cursor_next (curseur, &doc))
    {
      chaîne_document_courant = bson_as_json
        (doc, &taille_chaîne_document_courant);

      taille += taille_chaîne_document_courant + 1;
      *expéditions = realloc (*expéditions, taille);

      strncat (*expéditions, chaîne_document_courant,
               taille_chaîne_document_courant);
      strcat (*expéditions, "\n");

      free (chaîne_document_courant);
      succès = 1;
    }

  if (!succès)
    free (*expéditions);

  bson_destroy (requête);
  bson_destroy (options);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}

int
obtenir_expéditions (const char *joueur,
                     char **expéditions)
{
  int succès = 0;
  char *expéditions_ordonnées, *expéditions_étrangères;

  *expéditions = NULL;
  unsigned long taille = 0;

  if (!obtenir_expéditions_générique ("commanditaire", joueur,
                                      &expéditions_ordonnées))
    expéditions_ordonnées = NULL;
  else
    {
      taille = strlen (expéditions_ordonnées) + 1;
      *expéditions = malloc (taille);
      strcpy (*expéditions, expéditions_ordonnées);
      succès = 1;
    }

  if (!obtenir_expéditions_générique ("cible", joueur,
                                      &expéditions_étrangères))
    expéditions_étrangères = NULL;
  else
    {
      if (taille != 0)
        taille += strlen (expéditions_étrangères);
      else
        taille = strlen (expéditions_étrangères) + 1;

      *expéditions = realloc (*expéditions, taille);
      if (succès)
        strcat (*expéditions, expéditions_étrangères);
      else
        strcpy (*expéditions, expéditions_étrangères);

      succès = 1;
    }

  if (expéditions_ordonnées)
    free (expéditions_ordonnées);
  if (expéditions_étrangères)
    free (expéditions_étrangères);

  /* Suppression du retour à la ligne superflu. */
  if (*expéditions != NULL)
    {
      --taille;
      if (taille < 1)
        {
          free (*expéditions);
          *expéditions = NULL;
        }
      else
        {
          *expéditions = realloc (*expéditions, taille);
          (*expéditions)[taille - 1] = 0;
        }
    }

  return succès;
}
