/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Les alliances.
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mongoc.h>
#include <bson.h>
#include <bcon.h>

#include "alliance.h"

int
créer_une_alliance (const char *fondateur, const char *nom_alliance)
{
  int succès = 0;

  /* On vérifie si le nom de l’alliance et du fondateur sont correctes. */
  if (!nom_alliance || !fondateur || !strcmp (nom_alliance, "")
      || !strcmp (fondateur, ""))
    return succès;

  /* Il faut vérifier que le fondateur ne fait partie d’aucune alliance. */
  if (obtenir_nom_alliance (fondateur, NULL))
    return succès;

  /* Il faut vérifier l’unicité du nom de l’alliance. */
  if (nom_alliance_existant (nom_alliance))
    return succès;

  bson_oid_t identifiant;
  bson_oid_init (&identifiant, NULL);
  bson_t *alliance = BCON_NEW ("_id", BCON_OID (identifiant),
                               "nom", BCON_UTF8 (nom_alliance),
                               "fondateur", BCON_UTF8 (fondateur),
                               "membres", "[", BCON_UTF8 (fondateur), "]",
                               "description externe", BCON_UTF8 (""),
                               "description interne", BCON_UTF8 (""));

  client = mongoc_client_new ("mongodb://localhost:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "îles");
  if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE,
                                 alliance, NULL, NULL))
    {
      fprintf (stderr, "Erreur lors de la création d’une alliance.\n");
      bson_destroy (alliance);
      mongoc_collection_destroy (collection);
      mongoc_client_destroy (client);
      return succès;
    }

  bson_destroy (alliance);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  /* Il faut définir l’alliance du fondateur. */
  if (définir_alliance_joueur_par_identifiant (fondateur, identifiant))
    succès = 1;
}

int
obtenir_nom_alliance (const char *pseudo, const char **alliance)
{
  int a_une_alliance = 0;

  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");
  requête = BCON_NEW ("pseudo", BCON_UTF8 (pseudo));
  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  if (!mongoc_cursor_next (curseur, &doc))
    return a_une_alliance;

  const bson_oid_t *identifiant;
  if (bson_iter_init (&iter, doc)
      && bson_iter_find (&iter, "alliance")
      && BSON_ITER_HOLDS_OID (&iter))
    {
      identifiant = bson_iter_oid (&iter);
      a_une_alliance = 1;
    }

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);

  if (alliance)
    {
      requête = BCON_NEW ("_id", BCON_OID (identifiant));
      collection = mongoc_client_get_collection (client, "nassau", "alliances");
      curseur = mongoc_collection_find_with_opts (collection, requête,
                                                  NULL, NULL);
      if (bson_iter_init (&iter, doc)
          && bson_iter_find (&iter, "nom")
          && BSON_ITER_HOLDS_UTF8 (&iter))
        *alliance = bson_iter_dup_utf8 (&iter, NULL);

      bson_destroy (requête);
      mongoc_collection_destroy (collection);
      mongoc_cursor_destroy (curseur);
    }

  mongoc_client_destroy (client);
  return a_une_alliance;
}

int
nom_alliance_existant (const char *nom)
{
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *curseur;
  const bson_t *doc;
  bson_t *requête;
  bson_iter_t iter;

  client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  collection = mongoc_client_get_collection (client, "nassau", "alliances");
  requête = BCON_NEW ("nom", BCON_UTF8 (nom));
  curseur = mongoc_collection_find_with_opts (collection, requête, NULL, NULL);

  int existe = 0;
  if (mongoc_cursor_next (curseur, &doc))
    existe = 1;

  bson_destroy (requête);
  mongoc_cursor_destroy (curseur);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return existe;
}

int
définir_alliance_joueur_par_identifiant (const char *joueur,
                                         const bson_oid_t identifiant)
{
  int succès = 0;

  /* On vérifie que le joueur n’a pas d’alliance. */
  if (obtenir_nom_alliance (joueur, NULL))
    return succès;

  mongoc_client_t *client = mongoc_client_new ("mongodb://127.0.0.1:27017/");
  mongoc_collection_t *collection;
  collection = mongoc_client_get_collection (client, "nassau", "joueurs");

  bson_t *requête = BCON_NEW ("pseudo", BCON_UTF8 (joueur));
  bson_t *mise_à_jour = BCON_NEW ("$set", "{",
                                  "alliance", BCON_OID (identifiant),
                                  "}");

  if (mongoc_collection_update (collection, MONGOC_UPDATE_NONE, requête,
                                mise_à_jour, NULL, NULL))
    succès = 1;

  bson_destroy (requête);
  bson_destroy (mise_à_jour);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);

  return succès;
}
