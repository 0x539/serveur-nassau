/*
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016-2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants succèssifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions qui
permettront de dialoguer en réseau.
*/

#include "réseau.h"
#include "technologie.h"
#include "navires.h"

int
mettre_trame_dans_structure (const char* chaine_trame,
			     struct trame** structure_trame,
			     size_t taille_message)
{
  int succès = 0;

  if (chaine_trame == NULL)
    {
      return succès;
    }

  *structure_trame = (struct trame*) malloc (sizeof (struct trame));
  if (*structure_trame == NULL)
    {
      fprintf (stderr, "Impossible d’allouer de la mémoire.\n");
      return succès;
    }

  (*structure_trame)->entete = NULL;
  (*structure_trame)->commande = NULL;
  (*structure_trame)->arguments = NULL;

  struct entete_trame *entete;
  entete = (struct entete_trame*) malloc (sizeof (struct entete_trame));
  if (entete == NULL)
    {
      fprintf (stderr, "Impossible d’allouer de la mémoire.\n");
      return succès;
    }

  (*structure_trame)->entete = entete;

  char *chaine = strdup (chaine_trame);
  if (chaine[taille_message] != '\0')
    {
      chaine = (char*) realloc (chaine, taille_message + 1);
      chaine[taille_message] = '\0';
    }
  char *chaine1 = chaine;

  char *element;

  element = strsep (&chaine, " \n"); /* Pseudo */
  if (element == NULL)
    return succès;
  entete->pseudo = strdup (element);

  element = strsep (&chaine, " \n"); /* Mot de passe */
  if (element == NULL)
    return succès;
  entete->mot_de_passe = strdup (element);

  element = strsep (&chaine, " \n"); /* Commande */
  if (element == NULL)
    return succès;
  (*structure_trame)->commande = strdup (element);

  element = strsep (&chaine, " \n"); /* Premier argument */
  succès = 1;
  if (element != NULL)
    {
      struct argument *arg;
      arg = (struct argument*) malloc (sizeof (struct argument));
      arg->valeur = strdup (element);
      arg->suivant = NULL;
      (*structure_trame)->arguments = arg;

      /* On enregistre tous les autres arguments */
      while ((element = strsep (&chaine, " \n")) != NULL)
	{
	  arg->suivant = (struct argument*) malloc (sizeof (struct argument));
	  arg = arg->suivant;
	  arg->suivant = NULL;
	  arg->valeur = strdup (element);
	}
    }

  free (chaine1);

  return succès;
}

void libérer_trame (struct trame *la_trame)
{
  if (!la_trame)
    return;

  /* On libère l’en-tête. */
  if (la_trame->entete != NULL)
    {
      if (la_trame->entete->pseudo != NULL)
	free (la_trame->entete->pseudo);

      if (la_trame->entete->mot_de_passe != NULL)
	free (la_trame->entete->mot_de_passe);

      free (la_trame->entete);
    }

  /* On libère la commande. */
  if (la_trame->commande != NULL)
    free (la_trame->commande);

  /* On libère la liste chaînée. */
  struct argument *arg = la_trame->arguments;
  struct argument *argtmp = NULL;
  if (arg != NULL)
    {
      do
	{
	  if (arg->valeur != NULL)
	    free (arg->valeur);

	  argtmp = arg;
	  arg = arg->suivant;
	  free (argtmp);

	  argtmp = NULL;
	} while (arg != NULL);
    }

  free (la_trame);
}

int
vérifier_identifiants (const struct trame structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  const char* pseudo = structure_trame.entete->pseudo;
  const char* mot_de_passe = structure_trame.entete->mot_de_passe;
  if (vérifier_mot_de_passe (pseudo, mot_de_passe))
    succès = 1;

  return succès;
}

int
nom_défense_correct (const char* nom_défense)
{
  int correct = 0;

  if (!strcmp (nom_défense, "mur"))
    correct = 1;
  else if (!strcmp (nom_défense, "canon"))
    correct = 1;
  else if (!strcmp (nom_défense, "mortier"))
    correct = 1;
  else if (!strcmp (nom_défense, "mine"))
    correct = 1;
  else if (!strcmp (nom_défense, "fort"))
    correct = 1;

  return correct;
}

int
nom_bâtiment_correct (const char* nom_bâtiment)
{
  int correct = 0;

  if (!strcmp (nom_bâtiment, "forêt"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "mine_de_métal"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "fabrique_poudre_canon"))
    correct = 1;
  /* else if (!strcmp (nom_bâtiment, "hangar_bois")) */
  /*   correct = 1; */
  /* else if (!strcmp (nom_bâtiment, "hangar_métal")) */
  /*   correct = 1; */
  /* else if (!strcmp (nom_bâtiment, "poudrière")) */
  /*   correct = 1; */
  else if (!strcmp (nom_bâtiment, "quartier_travailleurs"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "chantier_naval"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "laboratoire_recherche"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "camp_formation_travailleurs"))
    correct = 1;
  else if (!strcmp (nom_bâtiment, "taverne"))
    correct = 1;

  return correct;
}

int
décoder_commande (const char* commande)
{
  int retour = 0;

  if (strcmp (commande, "construire_bâtiment") == 0)
    retour = 1;
  else if (strcmp (commande, "construire_navires") == 0)
    retour = 2;
  else if (strcmp (commande, "construire_défenses") == 0)
    retour = 3;
  else if (strcmp (commande, "attaque") == 0)
    retour = 4;
  else if (strcmp (commande, "transporter") == 0)
    retour = 5;
  else if (strcmp (commande, "colonisation") == 0)
    retour = 6;
  else if (strcmp (commande, "espionnage") == 0)
    retour = 7;
  else if (strcmp (commande, "stationnement") == 0)
    retour = 8;
  else if (strcmp (commande, "lancer_recherche") == 0)
    retour = 9;
  else if (strcmp (commande, "abandon_île") == 0)
    retour = 10;
  else if (strcmp (commande, "ressources") == 0)
    retour = 11;
  else if (strcmp (commande, "bâtiments") == 0)
    retour = 12;
  else if (strcmp (commande, "navires") == 0)
    retour = 13;
  else if (strcmp (commande, "défenses") == 0)
    retour = 14;
  else if (strcmp (commande, "consulter_recherches") == 0)
    retour = 15;
  else if (strcmp (commande, "bâtiment_en_cours_de_construction") == 0)
    retour = 16;
  else if (strcmp (commande, "navires_en_cours_de_construction") == 0)
    retour = 17;
  else if (strcmp (commande, "défenses_en_cours_de_construction") == 0)
    retour = 18;
  else if (strcmp (commande, "expéditions") == 0)
    retour = 19;
  else if (strcmp (commande, "informations_îles") == 0)
    retour = 20;
  else if (strcmp (commande, "nombre_messages") == 0)
    retour = 21;
  else if (strcmp (commande, "consulter_message") == 0)
    retour = 22;
  else if (strcmp (commande, "mes_îles") == 0)
    retour = 23;
  else if (strcmp (commande, "recherches_en_cours") == 0)
    retour = 24;
  else if (strcmp (commande, "annulation_bâtiment") == 0)
    retour = 25;
  else if (strcmp (commande, "annulation_navire") == 0)
    retour = 26;
  else if (strcmp (commande, "annulation_défense") == 0)
    retour = 27;
  else if (strcmp (commande, "annulation_expédition") == 0)
    retour = 28;
  else if (strcmp (commande, "annulation_recherche") == 0)
    retour = 29;

  return retour;
}

int
récupérer_interface_connexion (unsigned short port)
{
  int interface_connexion, s;
  struct addrinfo informations, *resultat, *rp;

  memset (&informations, 0, sizeof (struct addrinfo));
  informations.ai_family = AF_INET6; /* Permet d’utiliser l’IPv4 et l’IPv6. */
  informations.ai_socktype = SOCK_DGRAM; /* Utilisation de datagrammes. */
  informations.ai_flags = AI_PASSIVE;
  informations.ai_protocol = 0;
  informations.ai_canonname = NULL;
  informations.ai_addr = NULL;
  informations.ai_next = NULL;

  char port_str[6];
  snprintf (port_str, 6, "%d", port);

  s = getaddrinfo (NULL, port_str, &informations, &resultat);
  if (s != 0)
    {
      exit (EXIT_FAILURE);
    }

  for (rp = resultat; rp != NULL; rp = rp->ai_next)
    {
      interface_connexion = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);
      if(interface_connexion == -1)
	continue;

      if (bind (interface_connexion, rp->ai_addr, rp->ai_addrlen) != -1)
	break; // Succès

      close (interface_connexion);
    }

  if (rp == NULL)
    {
      exit (EXIT_FAILURE);
    }

  freeaddrinfo (resultat);

  /* Interface de connexion non bloquante. */
  /* int flags = fcntl (interface_connexion, F_GETFL, 0); */
  /* fcntl (interface_connexion, F_SETFL, flags | O_NONBLOCK); */

  return interface_connexion;
}

int
lire_interface_connexion (struct informations_réseau* infos_réseau,
			  char* tampon, size_t taille_tampon)
{
  infos_réseau->longueur_adresse_pair = sizeof (struct sockaddr_storage);
  int nb_octets_lus = 0;

  nb_octets_lus = recvfrom (infos_réseau->interface_connexion, tampon,
			    taille_tampon, 0,
			    (struct sockaddr*) &infos_réseau->adresse_pair,
			    &infos_réseau->longueur_adresse_pair);

  return nb_octets_lus;
}

void
répondre (const struct informations_réseau infos_réseau, const char* message)
{
  sendto (infos_réseau.interface_connexion, message, strlen (message), 0,
	  (struct sockaddr *) &infos_réseau.adresse_pair,
	  infos_réseau.longueur_adresse_pair);
}

void
attendre_commande (unsigned short port)
{
  int interface_connexion = récupérer_interface_connexion (port);
  int nb_octets_lus = 0;

  const int TAILLE_TAMPON = 8192;
  char tampon[TAILLE_TAMPON];

  struct informations_réseau infos_réseau;
  infos_réseau.interface_connexion = interface_connexion;

  while (1)
    {
      nb_octets_lus = lire_interface_connexion (&infos_réseau, tampon, TAILLE_TAMPON);
      if (nb_octets_lus > 0)
	{
	  traiter_message (infos_réseau, tampon, nb_octets_lus);
	}
    }
}

void
traiter_message (const struct informations_réseau infos_réseau,
		 void* message, size_t taille_message)
{
  struct trame *structure_trame = NULL;

  /* Si la trame est incorrecte. */
  if (mettre_trame_dans_structure (message, &structure_trame, taille_message) == 0)
    {
      répondre (infos_réseau, "incorrecte");
      if (structure_trame)
        libérer_trame (structure_trame);
      return;
    }

  int (*fonctions[29]) (struct informations_réseau,
			struct trame *structure_trame) = {
    construire_bâtiment,
    construire_navires,
    construire_défenses,
    attaque,
    transporter,
    colonisation,
    lancement_espionnage,
    stationnement,
    lancer_recherche,
    nom1,
    consulter_ressources,
    consulter_niveau_bâtiments,
    consulter_nombre_navires,
    consulter_nombre_défenses,
    consulter_recherches,
    consulter_bâtiment_construction,
    consulter_navires_construction,
    consulter_défenses_construction,
    consulter_expéditions,
    consulter_informations_secteur,
    consultation_nombre_messages,
    consulter_message,
    consultation_de_mes_îles,
    consultation_recherche_en_cours,
    nom1,
    nom1,
    nom1,
    nom1,
    nom1
  };

  int retour;
  retour = décoder_commande (structure_trame->commande);
  if (retour == 0)
    {
      répondre (infos_réseau, "incorrecte");
      libérer_trame (structure_trame);
      return;
    }

  /************************************************/
  /** Il faut faire cela dans un autre processus **/
  /**   sur le modèle de celui des expéditions.  **/
  /************************************************/
  /**                                            **/
  /**   On met à jour la liste des bâtiments.    **/
  /**/   mettre_a_jour_liste_bâtiments ();      /**/
  /**   On met à jour la liste des navires.      **/
  /**/   mettre_a_jour_liste_navires ();        /**/
  /**   On met à jour la liste des navires.      **/
  /**/   mettre_à_jour_liste_défenses ();       /**/
  /**   On met à jour la liste des recherches.   **/
  /**/   mettre_à_jour_liste_recherches ();     /**/
  /**                                            **/
  /************************************************/
  /************************************************/

  /* On exécute la commande. */
  retour = (*fonctions[retour - 1]) (infos_réseau, structure_trame);

  /* S’il y a eu une erreur. */
  if (retour == 0)
    répondre (infos_réseau, "incorrecte");

  libérer_trame (structure_trame);
}

int
construire_bâtiment (const struct informations_réseau infos_réseau,
		     struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;
  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  argument_courant = structure_trame->arguments->suivant;
  if (argument_courant == NULL)
    return succès;

  /* Vérifier que le nom du bâtiment est correct. */
  if (!nom_bâtiment_correct (argument_courant->valeur))
    return succès;


  /* Vérifier si une construction n’est pas déjà en cours sur cette île. */
  if (construction_en_cours ("bâtiments en construction", coordonnées))
    return succès;

  /* Trouver le niveau actuel du bâtiment. */
  int niveau_bâtiment = 0;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
                                  argument_courant->valeur, &niveau_bâtiment);

  /* Calculer les ressources nécessaires. */
  struct ressources ressources_nécessaires;
  récupérer_ressources_nécessaires_bâtiment (argument_courant->valeur,
					     niveau_bâtiment + 1,
					     &ressources_nécessaires);

  /* Récupérer les ressources de l’île. */
  struct ressources ressources_de_l_île;
  récupérer_ressources_île (&ressources_de_l_île, coordonnées);

  /* Vérifier qu’il y a assez de ressources. */
  if (ressources_nécessaires.bois > ressources_de_l_île.bois
      || ressources_nécessaires.métal > ressources_de_l_île.métal
      || ressources_nécessaires.poudre_à_canon > ressources_de_l_île.poudre_à_canon
      || ressources_nécessaires.travailleurs > ressources_de_l_île.travailleurs)
    {
      /* Pas assez de ressources */
      return succès;
    }
  succès = 1;

  /* Récupérer le niveau du camp de travailleur. */
  int niveau_camp_travailleurs = 0;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "camp_formation_travailleurs",
				  &niveau_camp_travailleurs);

  /* Récupérer le niveau de la taverne. */
  int niveau_taverne = 0;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "taverne",
				  &niveau_taverne);

  /* Calculer le temps nécessaire. */
  unsigned int temps_nécessaire;
  temps_nécessaire = récupérer_temps_nécessaire_bâtiment
    (ressources_nécessaires, niveau_camp_travailleurs, niveau_taverne);

  /* Calculer la date à laquelle la construction sera terminée. */
  time_t date_fin_construction = time (NULL) + (time_t) temps_nécessaire;

  /* Retrancher les ressources. */
  ressources_nécessaires.bois *= -1;
  ressources_nécessaires.métal *= -1;
  ressources_nécessaires.poudre_à_canon *= -1;
  ressources_nécessaires.travailleurs *= -1;
  modifier_ressources (coordonnées, ressources_nécessaires);

  /* Appeler la fonction de la base de donnée qui enregistre
     une construction de bâtiment. */
  ajouter_construction_bâtiment (coordonnées, argument_courant->valeur,
				 niveau_bâtiment + 1,
				 date_fin_construction);

  répondre (infos_réseau, "fait");

  return succès;
}

int
construire_navires (const struct informations_réseau infos_réseau,
		    struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char *pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  argument_courant = structure_trame->arguments->suivant;
  if (argument_courant == NULL)
    return succès;

  const char *nom_navire = argument_courant->valeur;
  /* Vérifier que le nom du navire est correct. */
  if (!nom_navire_correct (nom_navire))
    return succès;

  if (construction_en_cours ("navires en construction", coordonnées))
    return succès;

  /* Récupération du nombre de navires à construire. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;

  const int nombre_navires = atoi (argument_courant->valeur);
  if (nombre_navires == 0)
    return succès;

  /* Calculer les ressources nécessaires. */
  struct ressources ressources_nécessaires;
  récupérer_ressources_nécessaires_navire (nom_navire, nombre_navires,
					   &ressources_nécessaires);

  /* Récupérer les ressources de l’île. */
  struct ressources ressources_de_l_île;
  récupérer_ressources_île (&ressources_de_l_île, coordonnées);

  /* Vérifier qu’il y a assez de ressources. */
  if (ressources_nécessaires.bois > ressources_de_l_île.bois
      || ressources_nécessaires.métal > ressources_de_l_île.métal
      || ressources_nécessaires.poudre_à_canon > ressources_de_l_île.poudre_à_canon
      || ressources_nécessaires.travailleurs > ressources_de_l_île.travailleurs)
    {
      /* Pas assez de ressources */
      return succès;
    }
  succès = 1;

  /* Récupérer le niveau du camp de travailleurs. */
  int niveau_chantier_naval = 0;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "chantier_naval",
				  &niveau_chantier_naval);

  int niveau_taverne = 0;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "taverne",
				  &niveau_taverne);

  /* Calculer le temps nécessaire. */
  unsigned int temps_nécessaire;
  temps_nécessaire = récupérer_temps_nécessaire_objets_militaires
    (ressources_nécessaires, niveau_chantier_naval, niveau_taverne);

  /* Calculer la date à laquelle la construction sera terminée. */
  time_t date_fin_construction = time (NULL) + (time_t) temps_nécessaire;

  /* Retrancher les ressources. */
  ressources_nécessaires.bois *= -1;
  ressources_nécessaires.métal *= -1;
  ressources_nécessaires.poudre_à_canon *= -1;
  ressources_nécessaires.travailleurs *= -1;
  modifier_ressources (coordonnées, ressources_nécessaires);

  /* Appeler la fonction de la base de données qui enregistre
     une construction de navires. */
  ajouter_construction_navires (coordonnées, nom_navire,
				nombre_navires,
				date_fin_construction);

  répondre (infos_réseau, "fait");

  return succès;
}

int
construire_défenses (const struct informations_réseau infos_réseau,
		     struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char *pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  if (construction_en_cours ("défenses en construction",
                                      coordonnées))
    return succès;

  argument_courant = structure_trame->arguments->suivant;
  if (argument_courant == NULL)
    return succès;

  const char *nom_défense = argument_courant->valeur;
  /* Vérifier que le nom de la défense est correct. */
  if (!nom_défense_correct (nom_défense))
    return succès;

  /* Récupération du nombre de défenses à construire. */
  argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;
  const int nombre_défenses = atoi (argument_courant->valeur);
  if (nombre_défenses == 0)
    return succès;

  /* Calculer les ressources nécessaires. */
  struct ressources ressources_nécessaires;
  if (!récupérer_ressources_nécessaires_défenses (nom_défense, nombre_défenses,
                                                  &ressources_nécessaires))
    return succès;

  /* Récupérer les ressources de l’île. */
  struct ressources ressources_de_l_île;
  if (!récupérer_ressources_île (&ressources_de_l_île, coordonnées))
    return succès;

  /* Vérifier qu’il y a assez de ressources. */
  if (ressources_nécessaires.bois > ressources_de_l_île.bois
      || ressources_nécessaires.métal > ressources_de_l_île.métal
      || ressources_nécessaires.poudre_à_canon > ressources_de_l_île.poudre_à_canon
      || ressources_nécessaires.travailleurs > ressources_de_l_île.travailleurs)
    {
      /* Pas assez de ressources */
      return succès;
    }

  succès = 1;

  /* Récupérer le niveau du camp de travailleur. */
  int niveau_chantier_naval;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "chantier_naval",
				  &niveau_chantier_naval);

  int niveau_taverne;
  récupérer_valeur_objet_sur_île (coordonnées, "bâtiments",
				  "taverne",
				  &niveau_taverne);

  /* Calculer le temps nécessaire. */
  unsigned int temps_nécessaire;
  temps_nécessaire =
    récupérer_temps_nécessaire_objets_militaires (ressources_nécessaires,
						  niveau_chantier_naval,
                                                  niveau_taverne);

  /* Calculer la date à laquelle la construction sera terminée. */
  time_t date_fin_construction = time (NULL) + (time_t) temps_nécessaire;

  /* Retrancher les ressources. */
  ressources_nécessaires.bois *= -1;
  ressources_nécessaires.métal *= -1;
  ressources_nécessaires.poudre_à_canon *= -1;
  ressources_nécessaires.travailleurs *= -1;
  modifier_ressources (coordonnées, ressources_nécessaires);

  /* Appeler la fonction de la base de donnée qui enregistre
     une construction de bâtiment. */
  ajouter_construction_défenses (coordonnées, nom_défense,
				 nombre_défenses,
				 date_fin_construction);

  répondre (infos_réseau, "fait");

  return succès;
}

int
attaque (const struct informations_réseau infos_réseau,
	 struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  /* On cherche l’île de départ */
  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;
  unsigned numéro_île_départ = atoi (argument_courant->valeur);
  struct coordonnées_île île_départ;
  if (!trouver_coordonnées_île (structure_trame->entete->pseudo,
				numéro_île_départ,
				&île_départ))
    return succès;

  /* On cherche l’océan de l’île d’arrivée. */
  struct coordonnées_île île_arrivée;
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.océan = atoi (argument_courant->valeur);

  /* On cherche le secteur de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.secteur = atoi (argument_courant->valeur);

  /* On cherche l’emplacement de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.emplacement = atoi (argument_courant->valeur);

  /* On vérifie l’existence de l’île. */
  if (!vérifier_existence_île (île_arrivée))
    return succès;

  /* On vérifie que le joueur ne s’attaque pas lui même. */
  char *propriétaire = NULL;
  if (!récupérer_propriétaire_île (île_arrivée, &propriétaire))
    return succès;
  if (!strcmp (propriétaire, structure_trame->entete->pseudo))
    return succès;
  free (propriétaire);

  /* On récupère le nombre de navires. */
  int navires[NOMBRE_NAVIRES];
  for (unsigned i = 0; i < sizeof (navires) / sizeof *(navires); ++i)
    {
      argument_courant = argument_courant->suivant;
      if (argument_courant == NULL)
	return succès;
      navires[i] = atoi (argument_courant->valeur);
    }

  /* On enregistre les navires dans une structure. */
  struct navires flotte = {navires[0], navires[1], navires[2],
			   navires[3], navires[4], navires[5],
			   navires[6], navires[7], navires[8]};

  if (!soustraire_navires (île_départ, flotte))
    return succès;

  struct ressources ressources = {0};

  /* Inscrire l’expédition. */
  int vitesse = obtenir_vitesse_la_plus_longue (flotte);
  time_t heure_fin = récupérer_temps_expédition (île_départ, île_arrivée,
						 vitesse);
  bson_t *document;
  char *commanditaire = structure_trame->entete->pseudo;
  if (inscrire_expédition ("attaque", commanditaire, île_départ,
			   île_arrivée, ressources, heure_fin,
			   flotte, &document))
    {
      succès = 1;
      envoyer_document_dans_tube (*document, tube_expéditions);
      kill (identifiant_processus_expédition, SIGUSR1);
      bson_destroy (document);
      répondre (infos_réseau, "fait");
    }

  return succès;
}

int
transporter (const struct informations_réseau infos_réseau,
	     struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  /* On cherche l’île de départ */
  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;
  unsigned numéro_île_départ = atoi (argument_courant->valeur);
  struct coordonnées_île île_départ;
  if (!trouver_coordonnées_île (structure_trame->entete->pseudo,
				numéro_île_départ,
				&île_départ))
    return succès;

  /* On cherche l’océan de l’île d’arrivée. */
  struct coordonnées_île île_arrivée;
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.océan = atoi (argument_courant->valeur);

  /* On cherche le secteur de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.secteur = atoi (argument_courant->valeur);

  /* On cherche l’emplacement de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.emplacement = atoi (argument_courant->valeur);

  /* On vérifie l’existence de l’île. */
  if (!vérifier_existence_île (île_arrivée))
    return succès;

  /* On récupère le nombre de navires. */
  int navires[NOMBRE_NAVIRES];
  for (unsigned i = 0; i < sizeof (navires) / sizeof *(navires); ++i)
    {
      argument_courant = argument_courant->suivant;
      if (argument_courant == NULL)
	return succès;
      navires[i] = atoi (argument_courant->valeur);
    }

  /* On les soustrait à ceux de l’île de départ. */
  struct navires navires_à_ajouter = {-navires[0], -navires[1], -navires[2],
				      -navires[3], -navires[4], -navires[5],
				      -navires[6], -navires[7], -navires[8]};
  if (!modifier_nombre_navires (île_départ, navires_à_ajouter))
    return succès;

  /* On enregistre les navires dans une structure. */
  struct navires flotte = {navires[0], navires[1], navires[2],
			   navires[3], navires[4], navires[5],
			   navires[6], navires[7], navires[8]};

  /* On récupère maintenant les ressources à envoyer. */
  struct ressources ressources_envoyées;
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources_envoyées.bois = atoi (argument_courant->valeur);
  if (ressources_envoyées.bois < 0)
    return succès;

  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources_envoyées.métal = atoi (argument_courant->valeur);
  if (ressources_envoyées.métal < 0)
    return succès;

  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources_envoyées.poudre_à_canon = atoi (argument_courant->valeur);
  if (ressources_envoyées.poudre_à_canon < 0)
    return succès;

  /* Calculer la capacité de transport. */
  long capacité_transport = obtenir_capacité_flotte (flotte);

  /* Calculer la capacité nécessaire. */
  unsigned capacité_nécessaire = ressources_envoyées.bois;
  capacité_nécessaire += ressources_envoyées.métal;
  capacité_nécessaire += ressources_envoyées.poudre_à_canon;

  /* Vérifier qu’il y a assez de place pour les ressources. */
  if (capacité_nécessaire > capacité_transport)
    return succès;

  /* Inscrire l’expédition. */
  int vitesse = obtenir_vitesse_la_plus_longue (flotte);
  time_t heure_fin = récupérer_temps_expédition (île_départ, île_arrivée,
						 vitesse);
  bson_t *document;
  char *commanditaire = structure_trame->entete->pseudo;
  if (inscrire_expédition ("transport", commanditaire, île_départ,
			   île_arrivée, ressources_envoyées, heure_fin,
			   flotte, &document))
    {
      succès = 1;
      envoyer_document_dans_tube (*document, tube_expéditions);
      kill (identifiant_processus_expédition, SIGUSR1);
      bson_destroy (document);
      répondre (infos_réseau, "fait");
    }

  return succès;
}

int
colonisation (const struct informations_réseau infos_réseau,
              struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  int colonisation = 0;
  colonisation = récupérer_niveau_recherche (structure_trame->entete->pseudo,
                                             "colonisation");

  int nombre_îles = obtenir_nombre_îles (structure_trame->entete->pseudo);
  if (nombre_îles > (colonisation / 2 + 1))
    return succès;

  /* On cherche l’île de départ */
  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;
  unsigned numéro_île_départ = atoi (argument_courant->valeur);
  struct coordonnées_île île_départ;
  if (!trouver_coordonnées_île (structure_trame->entete->pseudo,
				numéro_île_départ,
				&île_départ))
    return succès;

  /* On cherche l’océan de l’île d’arrivée. */
  struct coordonnées_île île_arrivée;
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.océan = atoi (argument_courant->valeur);

  /* On cherche le secteur de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.secteur = atoi (argument_courant->valeur);

  /* On cherche l’emplacement de l’île d’arrivée. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  île_arrivée.emplacement = atoi (argument_courant->valeur);

  if (vérifier_existence_île (île_arrivée))
    return succès;

  unsigned nb_btlo = 0;
  unsigned nb_btlé = 0;

  /* On cherche le nombre de bateaux de transport lourd. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  nb_btlo = atoi (argument_courant->valeur);

  /* On cherche le nombre de bateaux de transport léger. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  nb_btlé = atoi (argument_courant->valeur);

  if (nb_btlo == 0)
    return succès;

  struct navires navires_de_l_île;
  récupérer_nombre_navires (île_départ, &navires_de_l_île);

  if (nb_btlo > navires_de_l_île.bateau_transport_lourd
      || nb_btlé > navires_de_l_île.bateau_transport_léger)
    return succès;

  struct navires flotte = {0};
  flotte.bateau_transport_léger = nb_btlé;
  flotte.bateau_transport_lourd = nb_btlo;

  struct ressources ressources = {0};
  /* On récupère le bois. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.bois = atoi (argument_courant->valeur);

  /* On récupère le métal. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.métal = atoi (argument_courant->valeur);

  /* On récupère la poudre à canon. */
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.poudre_à_canon = atoi (argument_courant->valeur);

  /* Calculer la capacité de transport. */
  long capacité_transport = obtenir_capacité_flotte (flotte);

  /* Calculer la capacité nécessaire. */
  unsigned capacité_nécessaire = ressources.bois;
  capacité_nécessaire += ressources.métal;
  capacité_nécessaire += ressources.poudre_à_canon;

  /* Vérifier qu’il y a assez de place pour les ressources. */
  if (capacité_nécessaire > capacité_transport)
    return succès;

  /* On soustrait les navires et les ressources. */
  if (!soustraire_navires (île_départ, flotte))
    {
      fprintf (stderr, "Impossible de soustraire les navires de l’île.\n");
      return succès;
    }
  soustraire_ressources (île_départ, ressources);

  /* Inscrire l’expédition. */
  int vitesse = obtenir_vitesse_la_plus_longue (flotte);
  time_t heure_fin = récupérer_temps_expédition (île_départ, île_arrivée,
						 vitesse);
  bson_t *document;
  char *commanditaire = structure_trame->entete->pseudo;
  if (inscrire_expédition ("colonisation", commanditaire, île_départ,
			   île_arrivée, ressources, heure_fin,
			   flotte, &document))
    {
      succès = 1;
      envoyer_document_dans_tube (*document, tube_expéditions);
      kill (identifiant_processus_expédition, SIGUSR1);
      bson_destroy (document);
      répondre (infos_réseau, "fait");
    }

  return succès;
}

int lancement_espionnage (const struct informations_réseau infos_réseau,
			  struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  char *pseudo = structure_trame->entete->pseudo;

  /* Trouver les coordonnées de l’île de départ. */
  struct coordonnées_île coordonnées_île_départ;
  struct coordonnées_île coordonnées_île_cible;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées_île_départ))
    return succès;

  /* Récupération du numéro d’océan de l’île cible. */
  structure_trame->arguments = structure_trame->arguments->suivant;
  coordonnées_île_cible.océan = atoi (structure_trame->arguments->valeur);
  if (coordonnées_île_cible.océan < 1)
    return succès;

  /* Récupération du numéro de secteur de l’île cible. */
  structure_trame->arguments = structure_trame->arguments->suivant;
  coordonnées_île_cible.secteur = atoi (structure_trame->arguments->valeur);
  if (coordonnées_île_cible.secteur < 1)
    return succès;

  /* Récupération de l’emplacement de l’île cible. */
  structure_trame->arguments = structure_trame->arguments->suivant;
  coordonnées_île_cible.emplacement = atoi (structure_trame->arguments->valeur);
  if (coordonnées_île_cible.emplacement < 1)
    return succès;

  /* On vérifie si l’île cible existe. */
  if (!vérifier_existence_île (coordonnées_île_cible))
    return succès;

  /* On extrait le nombre de bateaux espions devant être envoyé de la trame. */
  structure_trame->arguments = structure_trame->arguments->suivant;
  int nombre_bateaux_espions;
  nombre_bateaux_espions = atoi (structure_trame->arguments->valeur);

  /* On récupère le nombre de bateaux espions de l’île. */
  struct navires nombre_navires;
  if (!récupérer_nombre_navires (coordonnées_île_départ, &nombre_navires))
    return succès;

  /* On vérifie que le nombre de bateaux espions devant être envoyés */
  /* est inférieur ou égal au nombre de bateaux espions présents sur l’île. */
  if (nombre_navires.bateau_espion < nombre_bateaux_espions || nombre_bateaux_espions <= 0)
    return succès;

  struct navires flotte = {nombre_bateaux_espions, 0, 0, 0, 0, 0, 0, 0, 0};
  int vitesse = obtenir_vitesse_la_plus_longue (flotte);
  time_t heure_fin = récupérer_temps_expédition (coordonnées_île_départ,
						 coordonnées_île_cible,
						 vitesse);

  struct ressources ressources = {0, 0, 0, 0};
  bson_t *document;

  if (!inscrire_expédition ("espionnage", pseudo, coordonnées_île_départ,
			    coordonnées_île_cible, ressources, heure_fin,
			    flotte, &document))
    return succès;

  /* On soustrait le nombre de navires envoyés au nombre de navires de l’île. */
  modifier_valeur_objet_sur_île (coordonnées_île_départ, "navires",
				 "bateau_espion",
				 nombre_navires.bateau_espion
				 - nombre_bateaux_espions);

  envoyer_document_dans_tube (*document, tube_expéditions);
  kill (identifiant_processus_expédition, SIGUSR1);
  bson_destroy (document);

  if (!inscrire_expédition ("retour", pseudo, coordonnées_île_cible,
			    coordonnées_île_départ, ressources,
			    heure_fin + (heure_fin - time (NULL)),
			    flotte, &document))
    return succès;

  envoyer_document_dans_tube (*document, tube_expéditions);
  kill (identifiant_processus_expédition, SIGUSR1);
  bson_destroy (document);

  succès = 1;

  répondre (infos_réseau, "fait");

  return succès;
}

int stationnement (const struct informations_réseau infos_réseau,
                   struct trame* structure_trame)
{
  int succès = 0;

  /* Vérification des identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;
  char *pseudo = structure_trame->entete->pseudo;

  unsigned numéro_île_départ;
  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;
  numéro_île_départ = atoi (argument_courant->valeur);

  unsigned numéro_île_arrivée;
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  numéro_île_arrivée = atoi (argument_courant->valeur);

  struct coordonnées_île île_départ;
  if (!trouver_coordonnées_île (pseudo, numéro_île_départ, &île_départ))
    return succès;

  struct coordonnées_île île_arrivée;
  if (!trouver_coordonnées_île (pseudo, numéro_île_arrivée, &île_arrivée))
    return succès;

  /* On récupère le nombre de navires. */
  int navires[NOMBRE_NAVIRES];
  for (unsigned i = 0; i < sizeof (navires) / sizeof *(navires); ++i)
    {
      argument_courant = argument_courant->suivant;
      if (argument_courant == NULL)
	return succès;
      navires[i] = atoi (argument_courant->valeur);
    }

  struct ressources ressources = {0};
  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.bois = atoi (argument_courant->valeur);

  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.métal = atoi (argument_courant->valeur);

  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  ressources.poudre_à_canon = atoi (argument_courant->valeur);

  /* On soustrait les ressources. */
  if (soustraire_ressources (île_départ, ressources) < 1)
    return succès;

  /* On enregistre les navires dans une structure. */
  struct navires flotte = {navires[0], navires[1], navires[2],
			   navires[3], navires[4], navires[5],
			   navires[6], navires[7], navires[8]};

  if (!soustraire_navires (île_départ, flotte))
    {
      modifier_ressources (île_départ, ressources);
      return succès;
    }

  int vitesse = obtenir_vitesse_la_plus_longue (flotte);
  time_t heure_fin = récupérer_temps_expédition (île_départ, île_arrivée,
						 vitesse);

  bson_t *document;
  if (!inscrire_expédition ("stationnement", pseudo,
                            île_départ, île_arrivée, ressources, heure_fin,
			    flotte, &document))
    {
      modifier_nombre_navires (île_départ, flotte);
      modifier_ressources (île_départ, ressources);
      return succès;
    }

  envoyer_document_dans_tube (*document, tube_expéditions);
  kill (identifiant_processus_expédition, SIGUSR1);
  bson_destroy (document);

  succès = 1;
  répondre (infos_réseau, "fait");
  return succès;
}

int
lancer_recherche (const struct informations_réseau infos_réseau,
                  struct trame* structure_trame)
{
  int succès = 0;

  /* Vérification des identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char *pseudo = structure_trame->entete->pseudo;
  if (recherche_en_cours (pseudo))
    return succès;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Récupération du nom de la recherche et du numéro de l’île. */
  char *nom_recherche = argument_courant->valeur;
  if (!strcmp (nom_recherche, "formation_officiers"))
    nom_recherche = "formation d’officiers";

  argument_courant = argument_courant->suivant;
  if (argument_courant == NULL)
    return succès;
  unsigned numéro_île = atoi (argument_courant->valeur);

  struct coordonnées_île île;
  if (!trouver_coordonnées_île (pseudo, numéro_île, &île))
    return succès;

  if (!recherche_disponible (nom_recherche, île))
    return succès;

  unsigned niveau_recherche = récupérer_niveau_recherche (pseudo,
                                                          nom_recherche);
  if (niveau_recherche == -1)
    return succès;
  ++niveau_recherche;

  /* Récupération du coût de la recherche. */
  struct ressources coût_recherche;
  if (!récupérer_ressources_nécessaires_recherche (nom_recherche,
                                                   niveau_recherche,
                                                   &coût_recherche))
    return succès;

  /* A-t-il assez de ressources ? */
  if (soustraire_ressources (île, coût_recherche) < 1)
    return succès;
  else
    succès = 1;

  /* Récupération du niveau du laboratoire de recherche. */
  int niveau_labo = 0;
  récupérer_valeur_objet_sur_île (île, "bâtiments",
                                  "laboratoire_recherche", &niveau_labo);

  time_t date_fin = time (NULL);
  int temps_nécessaire = récupérer_temps_nécessaire_recherche (coût_recherche,
                                                               niveau_labo);
  date_fin += temps_nécessaire;
  ajouter_recherche (pseudo, nom_recherche, niveau_recherche, date_fin);
  répondre (infos_réseau, "fait");

  return succès;
}

int
consulter_ressources (const struct informations_réseau infos_réseau,
		      struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  /* Récupérer les ressources. */
  struct ressources ressources_de_l_île;
  récupérer_ressources_île (&ressources_de_l_île, coordonnées);

  /* Les mettre dans la chaine. */
  const int taille_reponse = snprintf (NULL, 0, "%d %d %d %d",
				       ressources_de_l_île.bois,
				       ressources_de_l_île.métal,
				       ressources_de_l_île.poudre_à_canon,
				       ressources_de_l_île.travailleurs) + 1;
  char reponse[taille_reponse];
  snprintf (reponse, taille_reponse, "%d %d %d %d", ressources_de_l_île.bois,
	    ressources_de_l_île.métal, ressources_de_l_île.poudre_à_canon,
	    ressources_de_l_île.travailleurs);

  /* Envoyer les ressources au client. */
  répondre (infos_réseau, reponse);

  return succès;
}

int
consulter_niveau_bâtiments (const struct informations_réseau infos_réseau,
			    struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  /* On récupère le niveau des bâtiments. */
  struct niveau_bâtiments niveaux;
  récupérer_niveau_bâtiments (coordonnées, &niveaux);

  /* Les mettre dans la chaine. */
  const int taille_reponse = snprintf (NULL, 0,
				       "%d %d %d %d %d %d %d %d %d %d %d",
  				       niveaux.forêt,
				       niveaux.mine_métal,
				       niveaux.fabrique_poudre_canon,
				       niveaux.hangar_bois,
				       niveaux.hangar_métal,
				       niveaux.poudrière,
				       niveaux.quartier_travailleurs,
				       niveaux.chantier_naval,
				       niveaux.laboratoire_recherche,
				       niveaux.camp_formation_travailleurs,
                                       niveaux.taverne) +1;
  char reponse[taille_reponse];
  snprintf (reponse, taille_reponse, "%d %d %d %d %d %d %d %d %d %d %d",
	    niveaux.forêt, niveaux.mine_métal, niveaux.fabrique_poudre_canon,
	    niveaux.hangar_bois, niveaux.hangar_métal,
	    niveaux.poudrière, niveaux.quartier_travailleurs,
	    niveaux.chantier_naval, niveaux.laboratoire_recherche,
	    niveaux.camp_formation_travailleurs, niveaux.taverne);

  /* Envoyer le niveau des bâtiments au client. */
  répondre (infos_réseau, reponse);

  return succès;
}

int
consulter_nombre_navires (const struct informations_réseau infos_réseau,
			  struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  /* On récupère le niveau des bâtiments. */
  struct navires nombre_navires;
  récupérer_nombre_navires (coordonnées, &nombre_navires);

  /* Les mettre dans la chaine. */
  const int taille_reponse = snprintf (NULL, 0,
				       "%d %d %d %d %d %d %d %d %d",
				       nombre_navires.bateau_espion,
				       nombre_navires.bateau_transport_léger,
				       nombre_navires.bateau_transport_lourd,
				       nombre_navires.canonnière,
				       nombre_navires.frégate,
				       nombre_navires.corvette,
				       nombre_navires.brick,
				       nombre_navires.galion,
				       nombre_navires.navire_de_ligne) + 1;
  char reponse[taille_reponse];
  snprintf (reponse, taille_reponse, "%d %d %d %d %d %d %d %d %d",
	    nombre_navires.bateau_espion,
	    nombre_navires.bateau_transport_léger,
	    nombre_navires.bateau_transport_lourd,
	    nombre_navires.canonnière,
	    nombre_navires.frégate,
	    nombre_navires.corvette,
	    nombre_navires.brick,
	    nombre_navires.galion,
	    nombre_navires.navire_de_ligne);

  /* Envoyer le niveau des bâtiments au client. */
  répondre (infos_réseau, reponse);

  return succès;
}

int
consulter_nombre_défenses (const struct informations_réseau infos_réseau,
			   struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  /* On récupère le niveau des bâtiments. */
  struct défenses nombre_défenses = {0};
  récupérer_nombre_défenses (coordonnées, &nombre_défenses);

  /* Les mettre dans la chaine. */
  const int taille_reponse = snprintf (NULL, 0,
				       "%d %d %d %d %d",
  				       nombre_défenses.murs,
				       nombre_défenses.canons,
				       nombre_défenses.mines,
                                       nombre_défenses.mortiers,
				       nombre_défenses.forts) +1;
  char reponse[taille_reponse];
  snprintf (reponse, taille_reponse, "%d %d %d %d %d",
	    nombre_défenses.murs,
	    nombre_défenses.canons,
	    nombre_défenses.mines,
            nombre_défenses.mortiers,
	    nombre_défenses.forts);

  /* Envoyer le niveau des bâtiments au client. */
  répondre (infos_réseau, reponse);

  return succès;
}

int
consulter_recherches (const struct informations_réseau infos_réseau,
                      struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifions les identifiants. */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  struct niveau_recherches niveaux = {0};
  if (!récupérer_niveau_recherches (structure_trame->entete->pseudo, &niveaux))
    return succès;

  succès = 1;
  unsigned taille = snprintf (NULL, 0, "%d %d %d %d %d %d",
                              niveaux.espionnage, niveaux.voilure,
                              niveaux.coque, niveaux.canons,
                              niveaux.formation_officiers,
                              niveaux.colonisation) + 1;
  char réponse[taille];
  snprintf (réponse, taille, "%d %d %d %d %d %d",
            niveaux.espionnage, niveaux.voilure,
            niveaux.coque, niveaux.canons,
            niveaux.formation_officiers,
            niveaux.colonisation);
  répondre (infos_réseau, réponse);

  return succès;
}

int
consulter_bâtiment_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  char *nom_bâtiment, *reponse;
  unsigned int taille_reponse = 1;
  time_t heure_de_fin = 0;

  /* Vérifier si une construction n’est pas déjà en cours sur cette île. */
  if (récupérer_construction_en_cours (coordonnées, &nom_bâtiment,
				       &heure_de_fin))
    {
      taille_reponse = snprintf (NULL, 0, "oui %s %ld", nom_bâtiment,
				 heure_de_fin) + 1;

      reponse = (char*) malloc (sizeof (char) * taille_reponse);
      snprintf (reponse, taille_reponse, "oui %s %ld", nom_bâtiment, heure_de_fin);

      répondre (infos_réseau, reponse);
      free (reponse);
      free (nom_bâtiment);
    }
  else
    {
      répondre (infos_réseau, "non");
    }

  return succès;
}

int
consulter_navires_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  char *nom_navires, *reponse;
  int nombre;
  unsigned int taille_reponse = 1;
  time_t heure_de_fin = 0;

  /* Vérifier si une construction n’est pas déjà en cours sur cette île. */
  if (récupérer_construction_navires_en_cours (coordonnées, &nom_navires,
                                               &nombre, &heure_de_fin))
    {
      taille_reponse = snprintf (NULL, 0, "oui %s %d %ld", nom_navires,
				 nombre, heure_de_fin) + 1;

      reponse = (char*) malloc (sizeof (char) * taille_reponse);
      snprintf (reponse, taille_reponse, "oui %s %d %ld",
                nom_navires, nombre, heure_de_fin);
      free (nom_navires);

      répondre (infos_réseau, reponse);
      free (reponse);
    }
  else
    {
      répondre (infos_réseau, "non");
    }

  return succès;
}

int
consulter_défenses_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame)
{
  int succès = 0;

  /* Vérifier les identifiants */
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char* pseudo = structure_trame->entete->pseudo;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return succès;

  /* Trouver les coordonnées de l’île. */
  struct coordonnées_île coordonnées;

  unsigned int numéro_île;
  numéro_île = atoi (structure_trame->arguments->valeur);
  if (!trouver_coordonnées_île (pseudo, numéro_île, &coordonnées))
    return succès;

  succès = 1;

  char *nom_défenses, *réponse;
  int nombre;
  unsigned int taille_réponse = 1;
  time_t heure_de_fin = 0;

  /* Vérifier si une construction n’est pas déjà en cours sur cette île. */
  if (récupérer_construction_défenses_en_cours (coordonnées, &nom_défenses,
                                                &nombre, &heure_de_fin))
    {
      taille_réponse = snprintf (NULL, 0, "oui %s %d %ld", nom_défenses,
				 nombre, heure_de_fin) + 1;

      réponse = (char*) malloc (sizeof (char) * taille_réponse);
      snprintf (réponse, taille_réponse, "oui %s %d %ld",
                nom_défenses, nombre, heure_de_fin);
      free (nom_défenses);

      répondre (infos_réseau, réponse);
      free (réponse);
    }
  else
    {
      répondre (infos_réseau, "non");
    }

  return succès;
}

int
consulter_informations_secteur (const struct informations_réseau infos_réseau,
				struct trame* structure_trame)
{
  if (!vérifier_identifiants (*structure_trame))
    return 0;

  struct argument *argument_courant = structure_trame->arguments;
  if (argument_courant == NULL)
    return 0;

  /* L’argument est censé contenir le numéro de l’océan */
  int numéro_océan = atoi (argument_courant->valeur);

  if (argument_courant->suivant == NULL)
    return 0;
  else
    argument_courant = argument_courant->suivant;

  int numéro_secteur = atoi (argument_courant->valeur);

  if (numéro_océan <= 0 || numéro_océan > OCÉAN_MAX)
    return 0;

  if (numéro_secteur <= 0 || numéro_secteur > SECTEUR_MAX)
    return 0;

  struct informations_secteur secteur;
  char *reponse;
  int taille_reponse = 0;

  if (récupérer_informations_secteur (numéro_océan, numéro_secteur, &secteur))
    {
      taille_reponse = snprintf (NULL, 0,
				 "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;"
				 "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
				 secteur.nom_île[0],
				 secteur.propriétaire_île[0],
				 secteur.nom_île[1],
				 secteur.propriétaire_île[1],
				 secteur.nom_île[2],
				 secteur.propriétaire_île[2],
				 secteur.nom_île[3],
				 secteur.propriétaire_île[3],
				 secteur.nom_île[4],
				 secteur.propriétaire_île[4],
				 secteur.nom_île[5],
				 secteur.propriétaire_île[5],
				 secteur.nom_île[6],
				 secteur.propriétaire_île[6],
				 secteur.nom_île[7],
				 secteur.propriétaire_île[7],
				 secteur.nom_île[8],
				 secteur.propriétaire_île[8],
				 secteur.nom_île[9],
				 secteur.propriétaire_île[9],
				 secteur.nom_île[10],
				 secteur.propriétaire_île[10],
				 secteur.nom_île[11],
				 secteur.propriétaire_île[11],
				 secteur.nom_île[12],
				 secteur.propriétaire_île[12],
				 secteur.nom_île[13],
				 secteur.propriétaire_île[13],
				 secteur.nom_île[14],
				 secteur.propriétaire_île[14]) + 1;

      reponse = (char*) malloc (sizeof (char) * taille_reponse);

      snprintf (reponse, taille_reponse,
		"%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;"
		"%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
		secteur.nom_île[0], secteur.propriétaire_île[0],
		secteur.nom_île[1], secteur.propriétaire_île[1],
		secteur.nom_île[2], secteur.propriétaire_île[2],
		secteur.nom_île[3], secteur.propriétaire_île[3],
		secteur.nom_île[4], secteur.propriétaire_île[4],
		secteur.nom_île[5], secteur.propriétaire_île[5],
		secteur.nom_île[6], secteur.propriétaire_île[6],
		secteur.nom_île[7], secteur.propriétaire_île[7],
		secteur.nom_île[8], secteur.propriétaire_île[8],
		secteur.nom_île[9], secteur.propriétaire_île[9],
		secteur.nom_île[10], secteur.propriétaire_île[10],
		secteur.nom_île[11], secteur.propriétaire_île[11],
		secteur.nom_île[12], secteur.propriétaire_île[12],
		secteur.nom_île[13], secteur.propriétaire_île[13],
		secteur.nom_île[14],
		secteur.propriétaire_île[14]);

      répondre (infos_réseau, reponse);
      free (reponse);
      libérer_informations_secteur (&secteur);
      return 1;
    }
  else
    return 0;
}

int
consultation_nombre_messages (const struct informations_réseau infos_réseau,
			      struct trame *structure_trame)
{
  int succès = 0;
  if (!vérifier_identifiants (*structure_trame))
    return succès;
  else
    succès = 1;

  int taille_réponse = 0;
  char *réponse = NULL;
  int nombre_messages = 0;

  nombre_messages = récupérer_nombre_messages (structure_trame->entete->pseudo);
  taille_réponse = snprintf (NULL, 0, "%d", nombre_messages)+1;
  réponse = (char*) malloc (sizeof (char) * taille_réponse);
  snprintf (réponse, taille_réponse, "%d", nombre_messages);
  répondre (infos_réseau, réponse);
  free (réponse);

  return succès;
}

int
consulter_message (const struct informations_réseau infos_réseau,
		   struct trame *structure_trame)
{
  int succès = 0;
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  if (structure_trame->arguments == NULL)
    return succès;

  unsigned numéro_message = atoi (structure_trame->arguments->valeur);

  /* Si le numéro de message vaut 0, il y a eu une erreur. */
  /* Le premier message est le message numéro 1. */
  if (numéro_message == 0)
    return succès;

  char *message;
  if (!récupérer_message (structure_trame->entete->pseudo,
			 numéro_message, &message))
    return succès;
  else
    succès = 1;

  répondre (infos_réseau, message);
  free (message);

  return succès;
}

int
consultation_de_mes_îles (const struct informations_réseau infos_réseau,
                         struct trame *structure_trame)
{
  int succès = 0;
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  char *message = obtenir_informations_îles_joueur
    (structure_trame->entete->pseudo);
  if (!message)
    return succès;

  succès = 1;
  répondre (infos_réseau, message);
  free (message);

  return succès;
}

int
consultation_recherche_en_cours (const struct informations_réseau infos_réseau,
                                 struct trame *structure_trame)
{
  int succès = 0;
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  const char *pseudo = structure_trame->entete->pseudo;

  time_t heure_fin;
  char *nom_recherche = NULL;
  int recherche_en_cours = obtenir_recherche_en_cours (pseudo,
                                                       &nom_recherche,
                                                       &heure_fin);
  succès = 1;
  if (!recherche_en_cours)
    {
      répondre (infos_réseau, "non");
    }
  else
    {
      unsigned long taille = snprintf (NULL, 0, "oui %s %ld", nom_recherche,
                                       heure_fin) + 1;
      char message[taille];
      snprintf (message, taille, "oui %s %ld", nom_recherche, heure_fin);
      free (nom_recherche);
      répondre (infos_réseau, message);
    }

  return succès;
}

int
consulter_expéditions (const struct informations_réseau infos_réseau,
                           struct trame* structure_trame)
{
  int succès = 0;
  if (!vérifier_identifiants (*structure_trame))
    return succès;

  char *message = NULL;
  obtenir_expéditions (structure_trame->entete->pseudo,
                       &message);
  succès = 1;

  if (!message)
    {
      message = "aucune";
      répondre (infos_réseau, message);
    }
  else
    {
      répondre (infos_réseau, message);
      free (message);
    }

  return succès;
}

int
nom1 (const struct informations_réseau infos_réseau,
      struct trame* structure_trame)
{
  return 0;
}
