/*
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016-2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants succèssifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

#include "technologie.h"

int
recherche_disponible (const char *recherche,
                      const struct coordonnées_île île)
{
  int niveau = 0;
  récupérer_valeur_objet_sur_île (île, "bâtiments",
                                  "laboratoire_recherche", &niveau);

  if (!strcmp (recherche, "formation d’officiers"))
    {
      /* Laboratoire de recherche au premier niveau. */
      if (niveau >= 1)
        return 1;
    }

  else if (!strcmp (recherche, "voilure"))
    {
      /* Laboratoire de recherche au second niveau. */
      if (niveau >= 2)
        return 1;
    }

  else if (!strcmp (recherche, "canons"))
    {
      /* Laboratoire de recherche au second niveau. */
      if (niveau >= 2)
        return 1;
    }

  else if (!strcmp (recherche, "coque"))
    {
      /* Laboratoire de recherche au second niveau. */
      if (niveau >= 2)
        return 1;
    }

  else if (!strcmp (recherche, "espionnage"))
    {
      /* Laboratoire de recherche au troisième niveau. */
      if (niveau >= 3)
        return 1;
    }

  else if (!strcmp (recherche, "colonisation"))
    {
      /* Laboratoire de recherche au cinquième niveau. */
      if (niveau >= 5)
        return 1;
    }

  return 0;
}
