/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions qui
permettront de manipuler la base de données.
*/

#pragma once

#include <stdlib.h>
#include <mongoc.h>
#include <bson.h>
#include <bcon.h>
#include <time.h>
#include <math.h>

#include "navires.h"
#include "défenses.h"

#define OCÉAN_MAX 100
#define SECTEUR_MAX 1000

struct ressources
{
  int bois, métal, poudre_à_canon, travailleurs;
};

struct coordonnées_île
{
  int océan, secteur, emplacement;
};

struct niveau_bâtiments
{
  int forêt;
  int mine_métal;
  int fabrique_poudre_canon;
  int hangar_bois;
  int hangar_métal;
  int poudrière;
  int quartier_travailleurs;
  int chantier_naval;
  int laboratoire_recherche;
  int camp_formation_travailleurs;
  int taverne;
};

struct niveau_recherches
{
  int espionnage;
  int voilure;
  int coque;
  int canons;
  int formation_officiers;
  int colonisation;
};

struct informations_secteur
{
  int numéro_océan, numéro_secteur;

  char *nom_île[15];
  char *propriétaire_île[15];
};

/**
 * Permet de supprimer un document dans une collection par son identifiant.
 */
int supprimer_document (const bson_oid_t identifiant,
			const char* nom_collection);

/**
 * Permet de créer un joueur dans la base de données.
 *
 * Cette fonction renvoie 1 si le joueur a bien été créé,
 * 0 sinon.
 */
int créer_un_joueur (const char* nom_du_joueur, const char* mot_de_passe);

/**
 * Permet de modifier le nombre de ressources.
 *
 * ressources_à_ajouter est la quantité de ressources à ajouter,
 * si une quantité de ressources est négative, elle sera soustraite aux ressources de l’île.
 *
 * Renvoie 0 si l’île n’existe pas, -1 s’il n’y a pas assez de ressources sur
 * l’île ou 1 si tout c’est bien passé.
 */
int modifier_ressources (const struct coordonnées_île coordonnées,
			 const struct ressources ressources_à_ajouter);

/**
 * Permet de modifier le nombre de ressources.
 *
 * ressources_à_soustraire est la quantité de ressources à soustraire.
 *
 * Renvoie 0 si l’île n’existe pas, -1 s’il n’y a pas assez de ressources sur
 * l’île ou 1 si tout c’est bien passé.
 */
int soustraire_ressources (const struct coordonnées_île coordonnées,
			   const struct ressources ressources_à_soustraire);

/**
 * Permet de mettre à jour les ressources d’une île.
 */
int mettre_à_jour_ressources (const struct coordonnées_île coordonnées);

/**
 * Permet d’ajouter ou d’enlever des navires sur une île.
 *
 * navires_à_ajouter est le nombre de navires à ajouter,
 * si le nombre d’un navire est négatif, il sera soustrait aux navires de l’île.
 */
int modifier_nombre_navires (const struct coordonnées_île coordonnées,
			     const struct navires navires_à_ajouter);

/**
 * Permet d’enlever des navires sur une île.
 *
 * navires_à_soustraire est le nombre de navires à soustraire.
 */
int soustraire_navires (const struct coordonnées_île coordonnées,
			const struct navires navires_à_soustraire);

/**
 * Permet d’ajouter ou d’enlever des défenses sur une île.
 *
 * défenses_à_ajouter est le nombre de navires à ajouter,
 * si le nombre d’un navire est négatif, il sera soustrait aux navires de l’île.
 */
int ajouter_défenses (const struct coordonnées_île coordonnées,
		      const struct défenses défenses_à_ajouter);

/**
 * Permet d’enlever des défenses sur une île.
 *
 * navires_à_soustraire est le nombre de défenses à soustraire.
 */
int soustraire_défenses (const struct coordonnées_île coordonnées,
			 const struct défenses défenses_à_soustraire);

/**
 * Permet de vérifier le mot de passe du joueur.
 *
 * Cette fonction renvoie 1 si le mot de passe est correcte,
 * 0 sinon.
 */
int vérifier_mot_de_passe (const char* nom_du_joueur, const char* mot_de_passe);

/**
 * Permet de vérifier si un joueur existe.
 *
 * Renvoie 1 si le joueur existe, 0 sinon.
 */
int vérifier_existence_joueur (const char* nom_du_joueur);

/**
 * Vérifie si une île existe.
 *
 * Renvoie 1 si l’île existe, 0 sinon.
 */
int vérifier_existence_île (const struct coordonnées_île coordonnées);

/**
 * Permet de connaître le nombre de ressources sur une île.
 *
 * Renvoie 1 si tout s’est bien passé, 0 sinon.
 */
int récupérer_ressources_île (struct ressources *ressources_de_l_île,
			      const struct coordonnées_île coordonnées);

/**
 * Permet de connaître la date de dernière mise à jour de l’île.
 *
 * Renvoie 1 si tout s’est bien passé, 0 si l’île n’existe pas.
 */
int récupérer_date_dernière_màj_île (time_t* date,
				     const struct coordonnées_île coordonnées);

/**
 * Enregistre dans la chaîne destination le nom du propriétaire
 * de l’île aux coordonnées indiquées. La chaîne devra être libérée.
 */
int récupérer_propriétaire_île (const struct coordonnées_île coordonnées,
				char **destination);

/**
 * Permet d’obtenir la valeur d’un objet sur une île.
 *
 * @param coordonnées contient les coordonnées de l’île.
 * @param type_objet est le type de l’objet, par exemple « bâtiments ».
 * @param nom_objet est le nom de l’objet.
 * @param valeur contiendra la valeur de l’objet.
 *
 * @retourne 1 si tout s’est bien passé, 0 s’il y a eu échec.
 */
int récupérer_valeur_objet_sur_île (const struct coordonnées_île coordonnées,
				    const char* type_objet,
				    const char* nom_objet,
				    int* valeur);

int modifier_valeur_objet_sur_île (const struct coordonnées_île coordonnées,
				   const char* type_objet,
				   const char* nom_objet,
				   const int valeur);

/**
 * Permet de récupérer le niveau d’une recherche.
 *
 * Renvoie -1 s’il y a eu une erreur sinon renvoie le niveau de la recherche.
 */
int récupérer_niveau_recherche (const char *pseudo, const char* nom_recherche);

/**
 * Permet de modifier le niveau d’une recherche.
 *
 * Retourne 0 s’il y a eu une erreur, ou 1 si la modification a été effectuée.
 */
int modifier_niveau_recherche (const char *pseudo, const char* nom_recherche,
                               unsigned niveau);

/**
 * Permet de trouver les coordonnées de la n-ième île d’un joueur.
 */
int trouver_coordonnées_île (const char* pseudo,
			     const unsigned int numéro_île,
			     struct coordonnées_île* coordonnées);

/**
 * Permet de récupérer le niveau des bâtiments d’une île.
 */
int récupérer_niveau_bâtiments (const struct coordonnées_île coordonnées,
				struct niveau_bâtiments* niveaux);

/**
 * Permet de récupérer le niveau des recherches d’un joueur.
 *
 * @param pseudo est le pseudo du joueur.
 * @param niveaux contiendra le niveau des recherches.
 *
 * @retourne 1 si tout s’est bien passé, 0 s’il y a échec.
 */
int récupérer_niveau_recherches (const char* pseudo,
				 struct niveau_recherches *niveaux);

/**
 * Permet de récupérer le nombre de défenses présentes sur une île.
 */
int récupérer_nombre_défenses (const struct coordonnées_île coordonnées,
			       struct défenses* nombre_défenses);

/**
 * Permet de récupérer le nombre de navires présents sur une île.
 */
int récupérer_nombre_navires (const struct coordonnées_île coordonnées,
			      struct navires* nombre_navires);

/**
 * Permet de savoir si une construction est en cours sur une île.
 *
 * Renvoie 0 s’il n’y a pas de construction en cours, 1 dans le cas contraire.
 */
int construction_en_cours (const char *nom_collection,
                           const struct coordonnées_île coordonnées);
/**
 * Permet de savoir si une recherche est en cours.
 *
 * Retourne 0 s’il n’y a pas de recherche en cours, 1 s’il y en a une.
 */
int
recherche_en_cours (const char *pseudo);

/**
 * Récupérer le nom du bâtiment en construction et l’heure à laquelle la
 * construction sera terminée.
 *
 * Renvoie 0 s’il n’y a pas de construction en cours,
 *  1 si tout s’est bien passé.
 */
int récupérer_construction_en_cours (const struct coordonnées_île coordonnées,
				     char **nom_bâtiment, time_t* heure_de_fin);

/**
 * Permet d’obtenir le nom de la recherche en cours et son heure de fin.
 *
 * Renvoie 0 s’il n’y a pas de recherche en cours, 1 si tout s’est bien passé.
 */
int
obtenir_recherche_en_cours (const char *pseudo, char **nom_recherche,
                            time_t *heure_de_fin);

/**
 * Récupérer le nom des navires en construction et l’heure à laquelle la
 * construction sera terminée.
 * La chaîne devra être libérée.
 *
 * Renvoie 0 s’il n’y a pas de construction en cours,
 *  1 si tout s’est bien passé.
 */
int
récupérer_construction_navires_en_cours
(const struct coordonnées_île coordonnées,
 char **nom_navires, int *nombre, time_t* heure_de_fin);

/**
 * Récupérer le nom des défenses en construction et l’heure à laquelle la
 * construction sera terminée.
 * La chaîne devra être libérée.
 *
 * Renvoie 0 s’il n’y a pas de construction en cours,
 *  1 si tout s’est bien passé.
 */
int
récupérer_construction_défenses_en_cours
(const struct coordonnées_île coordonnées,
 char **nom_défenses, int *nombre, time_t* heure_de_fin);

/**
 * Ajoute une construction dans la table des constructions de bâtiments.
 */
int ajouter_construction_bâtiment (const struct coordonnées_île coordonnées,
				   const char* nom_bâtiment,
				   const int niveau_bâtiment,
				   time_t date_fin_construction);
/**
 * Ajoute une construction dans la table des constructions de navires.
 */
int
ajouter_construction_navires (const struct coordonnées_île coordonnées,
			      const char* nom_navire,
			      const int nombre_navires,
			      time_t date_fin_construction);

/**
 * Ajoute une construction dans la table des constructions de défenses.
 */
int
ajouter_construction_défenses (const struct coordonnées_île coordonnées,
			      const char* nom_défense,
			      const int nombre_défense,
			      time_t date_fin_construction);
/**
 * Ajoute une recherche à la liste des recherches en cours.
 */
int
ajouter_recherche (const char *pseudo,
                   const char* nom_recherche,
                   const int niveau_recherche,
                   time_t date_fin);

void mettre_a_jour_liste_bâtiments ();

void mettre_a_jour_liste_navires ();

void mettre_à_jour_liste_défenses ();

void mettre_à_jour_liste_recherches ();

/**
 * Permet d’obtenir le nom et le propriétaire de chaque île dans un secteur.
 */
int récupérer_informations_secteur (const int numéro_océan,
				    const int numéro_secteur,
				    struct informations_secteur *secteur);

/**
 * Libérer une structure secteur.
 */
void libérer_informations_secteur (struct informations_secteur *secteur);

int inscrire_expédition (const char *type_mission,
			 const char *nom_commanditaire,
			 const struct coordonnées_île île_départ,
			 const struct coordonnées_île île_arrivée,
			 const struct ressources ressources,
			 time_t heure_fin, const struct navires flotte,
			 bson_t **document);
/**
 * Permet d’inscrire un rapport d’espionnage dans la collection de messages.
 */
int inscrire_rapport_espionnage (const char *destinataire,
				 const char *pseudo_cible,
				 const struct coordonnées_île île_cible,
				 const struct ressources ressources,
				 const struct navires flotte,
				 const struct défenses défenses,
				 const struct niveau_bâtiments bâtiments,
				 const struct niveau_recherches recherches);

/**
 * Permet d’inscrire un message de retour dans les messages.
 */
int inscrire_retour (const char *destinataire,
		     const struct coordonnées_île île_retour,
		     const struct coordonnées_île île_départ,
		     const struct ressources ressources);

/**
 * Permet d’inscrire un rapport de transport reçu dans les messages.
 */
int inscrire_rapport_transport_reçu (const struct coordonnées_île île_départ,
				     const struct coordonnées_île île_arrivée,
				     const struct ressources ressources_reçues);

/**
 * Permet d’inscrire un rapport de transport effectué dans les messages.
 */
int inscrire_rapport_transport_effectué
(const struct coordonnées_île île_départ,
 const struct coordonnées_île île_arrivée,
 const struct ressources ressources_envoyées);

/**
 * Permet d’inscrire un rapport de stationnement effectué dans les messages.
 */
int inscrire_rapport_stationnement_effectué
(const struct coordonnées_île île_départ,
 const struct coordonnées_île île_arrivée,
 const struct ressources ressources_envoyées,
 const struct navires flotte_envoyée);

/**
 * Permet d’inscrire un rapport d’attaque dans la collection de messages.
 */
int inscrire_rapport_attaque (const char *destinataire,
			      const char *pseudo_cible,
			      const struct coordonnées_île île_cible,
			      const char *issue_du_combat,
			      const struct ressources ressources_récupérées,
			      const struct navires flotte_après_combat,
			      const struct navires navires_perdus,
			      const struct navires navires_détruits,
			      const struct défenses défenses_détruites);

/**
 * Permet d’inscrire un rapport de défense dans la collection de messages.
 */
int inscrire_rapport_défense (const char *destinataire,
			      const char *pseudo_attaquant,
			      const struct coordonnées_île île_attaquant,
			      const struct coordonnées_île île_cible,
			      const char *issue_du_combat,
			      const struct navires flotte_attaquante,
			      const struct navires navires_détruits,
			      const struct ressources ressources_perdues,
			      const struct navires navires_perdus,
			      const struct défenses défenses_perdues);

int
inscrire_rapport_colonisation_infructueuse (const struct coordonnées_île île,
					    const char *destinataire);

int
inscrire_rapport_colonisation_réussie (const struct coordonnées_île île,
					   const char *destinataire);

int ajouter_île (const struct coordonnées_île île, const char *colon,
		 const struct ressources ressources,
		 const struct navires flotte);

int récupérer_nombre_messages (const char *destinataire);

/**
 * Permet de récupérer le n-ième message.
 *
 * destinataire est le destinataire du message.
 * numéro est le numéro du message que l’on veut obtenir.
 * message servira a enregistrer le document enregistrant le message,
 * il faudra libérer la mémoire avec « free » quand il ne sera plus utilisé.
 */
int récupérer_message (const char *destinataire, unsigned numéro,
		       char **message);

/**
 * Permet de supprimer les anciens messages.
 */
void supprimer_anciens_messages ();

/**
 * Permet d’obtenir un document JSON contenant le nombre d’îles du joueur
 * ainsi que leur nom et leurs coordonnées.
 */
char*
obtenir_informations_îles_joueur (const char *pseudo);

/**
 * Permet d’obtenir le nom d’une île depuis ses coordonnées.
 * La chaîne renvoyée devra être libérée.
 */
char*
obtenir_nom_île (const struct coordonnées_île coordonnées);

/**
 * Permet de modifier le nombre d’îles d’un joueur.
 */
int
modifier_nombre_îles (const char *pseudo, unsigned nombre_d_îles);

/**
 * Permet d’obtenir le nombre d’îles d’une joueur.
 */
int
obtenir_nombre_îles (const char *pseudo);
