/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions relatives aux navires.
*/

#include <string.h>
#include <stdlib.h>

#pragma once

#define NOMBRE_NAVIRES 9

#define ATTAQUE_BATEAU_ESPION 0
#define ATTAQUE_BATEAU_TLÉ 0
#define ATTAQUE_BATEAU_TLO 0
#define ATTAQUE_CANONNIÈRE 2000
#define ATTAQUE_CORVETTE 8000
#define ATTAQUE_FRÉGATE 12000
#define ATTAQUE_BRICK 35000
#define ATTAQUE_GALION 100000
#define ATTAQUE_NDL 750000

#define TYPE_BATEAU_ESPION 1
#define TYPE_BATEAU_TLÉ 2
#define TYPE_BATEAU_TLO 3
#define TYPE_CANONNIÈRE 4
#define TYPE_CORVETTE 5
#define TYPE_FRÉGATE 6
#define TYPE_BRICK 7
#define TYPE_GALION 8
#define TYPE_NDL 9

#define CAPACITÉ_BATEAU_ESPION 10
#define CAPACITÉ_BATEAU_TLÉ 5000
#define CAPACITÉ_BATEAU_TLO 25000
#define CAPACITÉ_CANONNIÈRE 1000
#define CAPACITÉ_CORVETTE 2000
#define CAPACITÉ_FRÉGATE 3000
#define CAPACITÉ_BRICK 4000
#define CAPACITÉ_GALION 10000
#define CAPACITÉ_NDL 500000

/* La vitesse est en millième de fois la vitesse normale. */
#define VITESSE_BATEAU_ESPION 100000
#define VITESSE_BATEAU_TLÉ 2000
#define VITESSE_BATEAU_TLO 4000
#define VITESSE_CANONNIÈRE 4000
#define VITESSE_CORVETTE 2000
#define VITESSE_FRÉGATE 1000
#define VITESSE_BRICK 1000
#define VITESSE_GALION 250
#define VITESSE_NDL 250
#define VITESSE_MAX VITESSE_BATEAU_ESPION

struct navires
{
  int bateau_espion;
  int bateau_transport_léger;
  int bateau_transport_lourd;
  int canonnière;
  int corvette;
  int frégate;
  int brick;
  int galion;
  int navire_de_ligne;
};

/**
 * Permet de connaître le nombre total de navires dans la structure.
 */
int nombre_total_navires (const struct navires navires);

/**
 * Permet d’ajouter deux structures navire entre elles.
 *
 * destination est la structure qui sera modifiée.
 *
 * source est la structure contenant les navires à ajouter à
 * la structure destination.
 *
 * Retourne 1 si tout c’est bien passé, 0 dans le cas contraire.
 */
int ajouter_structures_navires (struct navires *destination,
				const struct navires source);

/**
 * Permet de soustraire deux structures navire entre elles.
 *
 * destination est la structure qui sera modifiée.
 *
 * source est la structure contenant les navires à soustraire à
 * la structure destination.
 *
 * Retourne 1 si tout c’est bien passé, 0 dans le cas contraire.
 */
int soustraire_structures_navires (struct navires *destination,
				   const struct navires source);

/**
 * Permet de savoir si le nom donné en paramètre est un nom correcte de navire.
 *
 * Retourne 0 si le nom est incorrecte, 1 s’il est correcte.
 */
int nom_navire_correct (const char* nom_navire);

/**
 * Permet d’obtenir la capacité de transport total d’une flotte.
 */
long obtenir_capacité_flotte (const struct navires flotte);

/**
 * Permet d’obtenir la vitesse la plus longue.
 */
int obtenir_vitesse_la_plus_longue (const struct navires flotte);

/**
 * Permet d’afficher une structure navires.
 */
void afficher_navires (const struct navires flotte);
