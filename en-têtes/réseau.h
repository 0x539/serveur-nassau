/*
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016-2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions qui
permettront de dialoguer en réseau.
*/

#pragma once

#include <string.h>		/* strdup, strsep */
#include <stdlib.h>		/* malloc */
#include <stdio.h>		/* fprintf */
#include <sys/socket.h>		/* sockaddr_storage */
#include <unistd.h>		/* close */
#include <fcntl.h>		/* fcntl */
#include <netdb.h>		/* addrinfo */
#include <signal.h>

#include "bd.h"
#include "ressources.h"
#include "expéditions.h"
#include "navires.h"

struct entete_trame
{
  char *pseudo, *mot_de_passe;
};

struct argument
{
  char *valeur;
  struct argument *suivant;
};

struct trame
{
  struct entete_trame *entete;
  char* commande;
  struct argument *arguments;
};

struct informations_réseau
{
  int interface_connexion;
  struct sockaddr_storage adresse_pair;
  socklen_t longueur_adresse_pair;
};

/* Variables globales */
int tube_expéditions;
pid_t identifiant_processus_expédition;

/**
 * Permet de trouver quelle est la commande envoyée.
 *
 * Renvoie 0 si la commande est invalide,
 *
 * 1 si c’est une construction de bâtiment,
 * 2 si c’est une construction de navires,
 * 3 si c’est une construction de défenses,
 *
 * 4 si c’est une attaque,
 * 5 si c’est un transport,
 * 6 si c’est une colonisation,
 * 7 si c’est un espionnage,
 * 8 si c’est un stationnement,
 * 9 si c’est une recherche,
 *
 * 10 si c’est l’abandon d’une ile,
 *
 * 11 si c’est une consultation des ressources,
 * 12 si c’est une consultation du niveau des bâtiments,
 * 13 si c’est une consultation du nombre de navires,
 * 14 si c’est une consultation du nombre de défenses,
 * 15 si c’est une consultation du niveaux des recherches,
 * 16 si c’est une consultation des constructions de bâtiments.
 * 17 si c’est une consultation des constructions de navires.
 * 18 si c’est une consultation des constructions de défenses.
 * 19 si c’est une consultation des expéditions.
 * 20 si c’est une consultation des informations sur les iles d’un secteur,
 * 21 si c’est une demande du nombre de messages,
 * 22 si c’est une consultation du n-ième message,
 * 23 si c’est une consultation des informations fondamentales d’îles,
 * 24 si c’est une consultation des recherches en cours.
 *
 * 25 si c’est l’annulation d’une construction de bâtiment,
 * 26 si c’est l’annulation d’une construction de navires,
 * 27 si c’est l’annulation d’une construction de défenses,
 * 28 si c’est l’annulation d’une expédition,
 * 29 si c’est l’annulation d’une recherche.
 */
int décoder_commande (const char* commande);

/**
 * Stocke la trame donnée en paramètre dans une structure trame.
 *
 * Renvoie 0 si la trame est invalide, 1 si tout s’est bien passé.
 */
int mettre_trame_dans_structure (const char* chaine_trame,
				 struct trame** structure_trame,
				 size_t taille_message);

/**
 * Libère une structure trame.
 */
void liberer_trame (struct trame *la_trame);

/**
 * Vérifie les identifiants reçus.
 *
 * Renvoie 0 si les identifiants sont incorrects, 1 sinon.
 */
int verifier_identifiants (const struct trame structure_trame);

int récupérer_ressources_nécessaires (const char* nom_objet,
				      int niveau,
				      struct ressources* ressources_nécessaires);

int nom_bâtiment_correct (const char* nom_bâtiment);
int nom_défense_correct (const char* nom_défense);

int récupérer_temps_nécessaire (const struct ressources ressources_nécessaires,
				const int niveau_camp_travailleurs);

/**
 * Récupérer l’interface de connexion.
 */
int récupérer_interface_connexion (unsigned short port);

/**
 * Lire l’interface de connexion et mettre ce que l’on a lu dans un tampon.
 *
 * Renvoie le nombre d’octets lu.
 */
int lire_interface_connexion (struct informations_réseau* infos_réseau,
			      char* tampon, size_t taille_tampon);

/**
 * Répondre à une commande.
 *
 * Le message doit se terminer par un octet de valeur entière 0.
 */
void répondre (const struct informations_réseau infos_réseau, const char* message);
/**
 * Lance un serveur sur le port spécifié et lit en boucle ce que l’on reçoit.
 */
void attendre_commande (unsigned short port);

/**
 * Traite le message reçu.
 */
void traiter_message (const struct informations_réseau infos_réseau,
		      void* message, size_t taille_message);

int
nom1 (const struct informations_réseau infos_réseau,
      struct trame* structure_trame);

int construire_bâtiment (const struct informations_réseau infos_réseau,
			 struct trame* structure_trame);

int construire_navires (const struct informations_réseau infos_réseau,
			struct trame* structure_trame);

int construire_défenses (const struct informations_réseau infos_réseau,
			 struct trame* structure_trame);

int attaque (const struct informations_réseau infos_réseau,
	     struct trame* structure_trame);

int transporter (const struct informations_réseau infos_réseau,
		 struct trame* structure_trame);

int colonisation (const struct informations_réseau infos_réseau,
                  struct trame* structure_trame);

int lancement_espionnage (const struct informations_réseau infos_réseau,
			  struct trame* structure_trame);

int stationnement (const struct informations_réseau infos_réseau,
                   struct trame* structure_trame);

int lancer_recherche (const struct informations_réseau infos_réseau,
                      struct trame* structure_trame);

/**
 * Permet d’obtenir le nombre de ressources d’une île.
 */
int consulter_ressources (const struct informations_réseau infos_réseau,
			  struct trame* structure_trame);

/**
 * Permet d’obtenir le niveau des bâtiments d’une île.
 */
int consulter_niveau_bâtiments (const struct informations_réseau infos_réseau,
				struct trame* structure_trame);

/**
 * Permet d’obtenir le nombre de navires sur une île.
 */
int consulter_nombre_navires (const struct informations_réseau infos_réseau,
			      struct trame* structure_trame);

/**
 * Permet d’obtenir le nombre de défenses sur une île.
 */
int consulter_nombre_défenses (const struct informations_réseau infos_réseau,
			       struct trame* structure_trame);

/**
 * Permet d’obtenir le niveau des recherches.
 */
int consulter_recherches (const struct informations_réseau infos_réseau,
                          struct trame* structure_trame);

int
consulter_bâtiment_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame);

int
consulter_navires_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame);

int
consulter_défenses_construction (const struct informations_réseau infos_réseau,
				 struct trame* structure_trame);

int
consulter_informations_secteur (const struct informations_réseau infos_réseau,
				struct trame* structure_trame);

int
consultation_nombre_messages (const struct informations_réseau infos_réseau,
			      struct trame *structure_trame);

int
consultation_de_mes_îles (const struct informations_réseau infos_réseau,
                          struct trame *structure_trame);

int
consultation_recherche_en_cours (const struct informations_réseau infos_réseau,
                                 struct trame *structure_trame);

int
consulter_message (const struct informations_réseau infos_réseau,
		   struct trame *structure_trame);

int
consulter_expéditions (const struct informations_réseau infos_réseau,
                       struct trame* structure_trame);
