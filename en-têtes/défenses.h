/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions relatives aux défenses.
*/

#include <string.h>
#include <stdlib.h>

#pragma once

#define NOMBRE_DÉFENSES 5

#define ATTAQUE_MUR 0
#define ATTAQUE_MINE 8500
#define ATTAQUE_CANON 7500
#define ATTAQUE_MORTIER 10000
#define ATTAQUE_FORT 350000

#define TYPE_MUR 1
#define TYPE_MINE 2
#define TYPE_CANON 3
#define TYPE_MORTIER 4
#define TYPE_FORT 5

struct défenses
{
  int murs;
  int mines;
  int canons;
  int mortiers;
  int forts;
};

/**
 * Permet d’ajouter deux structures défense entre elles.
 *
 * destination est la structure qui sera modifiée.
 *
 * source est la structure contenant les défenses à ajouter à
 * la structure destination.
 *
 * Retourne 1 si tout c’est bien passé, 0 dans le cas contraire.
 */
int ajouter_structures_défenses (struct défenses *destination,
				 const struct défenses source);

/**
 * Permet de soustraire deux structures défense entre elles.
 *
 * destination est la structure qui sera modifiée.
 *
 * source est la structure contenant les défenses à soustraire à
 * la structure destination.
 *
 * Retourne 1 si tout c’est bien passé, 0 dans le cas contraire.
 */
int soustraire_structures_défenses (struct défenses *destination,
				    const struct défenses source);
