/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l’accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n’est
offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l’auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l’attention de l’utilisateur est attirée sur les risques
associés au chargement,  à l’utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l’utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
logiciel à leurs besoins dans des conditions permettant d’assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions
relatives aux expéditions.
*/

#pragma once

#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>

#include "bd.h"
#include "navires.h"
#include "défenses.h"
#include "réseau.h"		/* tube_expéditions */

/* Structure. */
struct expédition
{
  char *type;
  char *commanditaire;
  bson_oid_t identifiant;

  struct coordonnées_île île_départ;
  struct coordonnées_île île_arrivée;

  struct navires flotte;
  struct ressources ressources;

  time_t heure_de_fin;

  struct expédition *suivant;
};

struct objet_militaire
{
  unsigned structure;		/* L’état de la structure. */
  unsigned attaque;		/* Le nombre de points d’attaque. */

  struct navires feu_rapide_navires; /* Feu rapide contre les navires. */
  struct défenses feu_rapide_défenses; /* Feu rapide contre les défenses. */

  unsigned type;

  struct objet_militaire *précédent;
  struct objet_militaire *suivant;
};

/* Variable globale. */
struct expédition *expéditions;

/**
 * Cette fonction crée le processus se chargeant de toutes les expéditions.
 */
void créer_processus_expéditions ();

/**
 * Attendre les expéditions.
 */
void attendre_expéditions ();

/**
 * Lit la base de donnée des expéditions et remplit la liste des expéditions.
 */
void remplir_liste_expéditions ();

/**
 * Ajoute le document dans la liste des expéditions.
 */
void mettre_document_dans_liste_expéditions (const bson_t *document);

/**
 * Exécute la première expédition de la liste.
 */
void exécuter_expédition ();

/**
 * Exécute l’attaque.
 */
void exécuter_attaque ();

/**
 * Exécute le transport.
 */
void exécuter_transport ();

/**
 * Exécute la colonisation.
 */
void exécuter_colonisation ();

/**
 * Exécute l’espionnage.
 */
void exécuter_espionnage ();

/**
 * Exécute le stationnement.
 */
void exécuter_stationnement ();

/**
 * Exécute le retour.
 */
void exécuter_retour ();

/**
 * Libérer une expédition.
 */
void libérer_expédition (struct expédition *expédition);

/**
 * Lit les données dans le tube.
 */
void lire_données_tube_expéditions ();

/**
 * Permet d’envoyer un document bson dans un tube.
 */
void envoyer_document_dans_tube (const bson_t document, int tube);

/**
 * Permet de récupérer le temps que prendra une expédition.
 */
time_t récupérer_temps_expédition (const struct coordonnées_île île_départ,
				   const struct coordonnées_île île_arrivée,
				   int vitesse);

/**
 * Affiche toute les expéditions en mémoire.
 */
void afficher_expéditions ();

/**
 * Supprime la première expédition de la base de données
 * et de la liste d’expéditions.
 */
void supprimer_expédition_courante ();

/**
 * Permet de remplir un tableau d’objets militaires avec les caractéristiques
 * des navires et des défenses donnés en paramètres.
 *
 * Dans cette fonction il y a les constantes des points de structures
 * des navires et des défenses ainsi que les taux de feu rapides.
 */
int remplir_objets_militaires (struct objet_militaire **objets,
			       const struct navires *navires,
			       const struct défenses *défenses);

/**
 * Permet de libérer une liste d’objets militaires.
 */
void libérer_objets_militaires (struct objet_militaire *objets);

/**
 * Permet d’obtenir la probabilité en pourcentage de tirer à nouveau.
 *
 * Retourne un nombre entre 0 et 100.
 */
int
obtenir_probabilité_tir_à_nouveau (const unsigned type_objet,
				   const struct navires feu_rapide,
				   const struct défenses feu_rapide_défenses);

/**
 * Permet d’obtenir à partir d’une liste d’objets militaires
 * la liste des navires et des défenses dans leur structure respective.
 */
void
convertir_objets_militaires (struct objet_militaire *objets_militaires,
			     struct navires *flotte,
			     struct défenses *défenses);

/**
 * Cette fonction est appelée lorsque le combat est nul.
 * Elle exécute toutes les actions à faire lorsque le combat est nul.
 */
void
combat_nul (struct objet_militaire *attaquant_après_combat,
	    struct objet_militaire *défenseur_après_combat,
	    const struct navires flotte_défenseur_avant_combat,
	    const struct défenses défenses_avant_combat);

/**
 * Cette fonction est appelée lorsque le défenseur a gagné.
 * Elle exécute toutes les actions à faire lorsque le défenseur est victorieux.
 */
void
victoire_défenseur (struct objet_militaire *défenseur_après_combat,
		    const struct navires flotte_défenseur_avant_combat,
		    const struct défenses défenses_avant_combat);

/**
 * Cette fonction est appelée lorsque l’attaquant a gagné.
 * Elle exécute toutes les actions à faire lorsque l’attaquant est victorieux.
 */
void
victoire_attaquant (struct objet_militaire *attaquant_après_combat,
		    struct objet_militaire *défenseur_après_combat,
		    const struct navires flotte_défenseur_avant_combat,
		    const struct défenses défenses_avant_combat);

/**
 * Permet de connaître le montant des ressources
 * qui vont être prises par l’attaquant.
 */
struct ressources
calculer_montant_ressources_à_prendre (long capacité_flotte,
				       const struct ressources ressources_île);

void
afficher_objets_militaires (struct objet_militaire *objets);

/**
 * Permet d’obtenir toute les expéditions dont « clef » vaut « valeur ».
 *
 * Il faudra libérer la variable expéditions.
 *
 * La fonction renvoie « 1 » si tout c’est bien passée.
 * Sinon elle renvoie « 0 ».
 */

int
obtenir_expéditions_générique (const char *clef,
                               const char *valeur,
                               char **expéditions);

/**
 * Permet d’obtenir toutes les expéditions contre le joueur et ordonnées
 * par le joueur donné en paramètre.
 *
 * Il faudra libérer la variable expéditions.
 *
 * La fonction renvoie « 1 » si tout c’est bien passée.
 * Sinon elle renvoie « 0 ».
 */
int
obtenir_expéditions (const char *joueur,
                     char **expéditions);
