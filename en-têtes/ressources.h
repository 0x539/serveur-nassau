/*
Auteur : Corentin Bocquillon <corentin@nybble.fr>
Auteur : Baptiste Bocquillon <pyth0n11@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les déclarations des fonctions qui
relatives aux ressources.
*/

#pragma once

#include "bd.h" 		/* structure ressources */
#include "navires.h"
#include "défenses.h"

/* Navires */

#define COÛT_BOIS_BATEAU_ESPION 0
#define COÛT_MÉTAL_BATEAU_ESPION 1000
#define COÛT_PÀC_BATEAU_ESPION 0

#define COÛT_BOIS_BATEAU_TLÉ 2000
#define COÛT_MÉTAL_BATEAU_TLÉ 2000
#define COÛT_PÀC_BATEAU_TLÉ 0

#define COÛT_BOIS_BATEAU_TLO 6000
#define COÛT_MÉTAL_BATEAU_TLO 6000
#define COÛT_PÀC_BATEAU_TLO 0

#define COÛT_BOIS_CANONNIÈRE 3000
#define COÛT_MÉTAL_CANONNIÈRE 1000
#define COÛT_PÀC_CANONNIÈRE 0

#define COÛT_BOIS_CORVETTE 6000
#define COÛT_MÉTAL_CORVETTE 4000
#define COÛT_PÀC_CORVETTE 0

#define COÛT_BOIS_FRÉGATE 20000
#define COÛT_MÉTAL_FRÉGATE 7000
#define COÛT_PÀC_FRÉGATE 2000

#define COÛT_BOIS_BRICK 45000
#define COÛT_MÉTAL_BRICK 15000
#define COÛT_PÀC_BRICK 0

#define COÛT_BOIS_GALION 50000
#define COÛT_MÉTAL_GALION 25000
#define COÛT_PÀC_GALION 15000

#define COÛT_BOIS_NDL 5000000
#define COÛT_MÉTAL_NDL 4000000
#define COÛT_PÀC_NDL 1000000

/* Défenses */

#define COÛT_BOIS_MUR 7500
#define COÛT_MÉTAL_MUR 5000
#define COÛT_PÀC_MUR 0

#define COÛT_BOIS_MINE 5000
#define COÛT_MÉTAL_MINE 2000
#define COÛT_PÀC_MINE 1000

#define COÛT_BOIS_CANON 10000
#define COÛT_MÉTAL_CANON 7500
#define COÛT_PÀC_CANON 3000

#define COÛT_BOIS_MORTIER 20000
#define COÛT_MÉTAL_MORTIER 15000
#define COÛT_PÀC_MORTIER 6000

#define COÛT_BOIS_FORT 50000
#define COÛT_MÉTAL_FORT 50000
#define COÛT_PÀC_FORT 30000

/* Recherches */

#define COÛT_BOIS_ESPIONNAGE 200
#define COÛT_MÉTAL_ESPIONNAGE 1000
#define COÛT_PÀC_ESPIONNAGE 200

#define COÛT_BOIS_CANONS 800
#define COÛT_MÉTAL_CANONS 200
#define COÛT_PÀC_CANONS 0

#define COÛT_BOIS_VOILURE 2000
#define COÛT_MÉTAL_VOILURE 4000
#define COÛT_PÀC_VOILURE 600

#define COÛT_BOIS_COQUE 1000
#define COÛT_MÉTAL_COQUE 100
#define COÛT_PÀC_COQUE 0

#define COÛT_BOIS_COLONISATION 4000
#define COÛT_MÉTAL_COLONISATION 8000
#define COÛT_PÀC_COLONISATION 4000

#define COÛT_BOIS_FORMATION_OFFICIERS 0
#define COÛT_MÉTAL_FORMATION_OFFICIERS 400
#define COÛT_PÀC_FORMATION_OFFICIERS 600

int
récupérer_ressources_nécessaires_bâtiment
(const char* nom_objet,
 const int niveau,
 struct ressources* ressources_nécessaires);

int
récupérer_ressources_nécessaires_navire
(const char* nom_navire,
 const int nombre,
 struct ressources* ressources_nécessaires);

int
récupérer_ressources_nécessaires_défenses
(const char* nom_défense,
 const int nombre,
 struct ressources* ressources_nécessaires);

int
récupérer_ressources_nécessaires_recherche
(const char* nom_objet,
 int niveau,
 struct ressources* ressources_nécessaires);

int
récupérer_temps_nécessaire_bâtiment
(const struct ressources ressources_nécessaires,
 const int niveau_camp_travailleurs,
 const int niveau_taverne);

int
récupérer_temps_nécessaire_objets_militaires
(const struct ressources ressources_nécessaires,
 const int niveau_chantier_naval,
 const int niveau_taverne);

int
récupérer_temps_nécessaire_recherche
(const struct ressources ressources_nécessaires,
 const unsigned niveau_laboratoire_recherche);
